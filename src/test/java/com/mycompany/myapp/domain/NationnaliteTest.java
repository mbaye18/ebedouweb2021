package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NationnaliteTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Nationnalite.class);
        Nationnalite nationnalite1 = new Nationnalite();
        nationnalite1.setId(1L);
        Nationnalite nationnalite2 = new Nationnalite();
        nationnalite2.setId(nationnalite1.getId());
        assertThat(nationnalite1).isEqualTo(nationnalite2);
        nationnalite2.setId(2L);
        assertThat(nationnalite1).isNotEqualTo(nationnalite2);
        nationnalite1.setId(null);
        assertThat(nationnalite1).isNotEqualTo(nationnalite2);
    }
}
