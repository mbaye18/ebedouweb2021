package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class InfoplusTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Infoplus.class);
        Infoplus infoplus1 = new Infoplus();
        infoplus1.setId(1L);
        Infoplus infoplus2 = new Infoplus();
        infoplus2.setId(infoplus1.getId());
        assertThat(infoplus1).isEqualTo(infoplus2);
        infoplus2.setId(2L);
        assertThat(infoplus1).isNotEqualTo(infoplus2);
        infoplus1.setId(null);
        assertThat(infoplus1).isNotEqualTo(infoplus2);
    }
}
