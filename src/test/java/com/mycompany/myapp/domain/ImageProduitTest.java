package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ImageProduitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageProduit.class);
        ImageProduit imageProduit1 = new ImageProduit();
        imageProduit1.setId(1L);
        ImageProduit imageProduit2 = new ImageProduit();
        imageProduit2.setId(imageProduit1.getId());
        assertThat(imageProduit1).isEqualTo(imageProduit2);
        imageProduit2.setId(2L);
        assertThat(imageProduit1).isNotEqualTo(imageProduit2);
        imageProduit1.setId(null);
        assertThat(imageProduit1).isNotEqualTo(imageProduit2);
    }
}
