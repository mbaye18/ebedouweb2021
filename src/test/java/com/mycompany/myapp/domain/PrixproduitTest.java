package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrixproduitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Prixproduit.class);
        Prixproduit prixproduit1 = new Prixproduit();
        prixproduit1.setId(1L);
        Prixproduit prixproduit2 = new Prixproduit();
        prixproduit2.setId(prixproduit1.getId());
        assertThat(prixproduit1).isEqualTo(prixproduit2);
        prixproduit2.setId(2L);
        assertThat(prixproduit1).isNotEqualTo(prixproduit2);
        prixproduit1.setId(null);
        assertThat(prixproduit1).isNotEqualTo(prixproduit2);
    }
}
