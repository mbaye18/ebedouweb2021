package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ListeCooperativeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListeCooperative.class);
        ListeCooperative listeCooperative1 = new ListeCooperative();
        listeCooperative1.setId(1L);
        ListeCooperative listeCooperative2 = new ListeCooperative();
        listeCooperative2.setId(listeCooperative1.getId());
        assertThat(listeCooperative1).isEqualTo(listeCooperative2);
        listeCooperative2.setId(2L);
        assertThat(listeCooperative1).isNotEqualTo(listeCooperative2);
        listeCooperative1.setId(null);
        assertThat(listeCooperative1).isNotEqualTo(listeCooperative2);
    }
}
