package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class QuantiteProduitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuantiteProduit.class);
        QuantiteProduit quantiteProduit1 = new QuantiteProduit();
        quantiteProduit1.setId(1L);
        QuantiteProduit quantiteProduit2 = new QuantiteProduit();
        quantiteProduit2.setId(quantiteProduit1.getId());
        assertThat(quantiteProduit1).isEqualTo(quantiteProduit2);
        quantiteProduit2.setId(2L);
        assertThat(quantiteProduit1).isNotEqualTo(quantiteProduit2);
        quantiteProduit1.setId(null);
        assertThat(quantiteProduit1).isNotEqualTo(quantiteProduit2);
    }
}
