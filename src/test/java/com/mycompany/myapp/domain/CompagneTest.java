package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CompagneTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Compagne.class);
        Compagne compagne1 = new Compagne();
        compagne1.setId(1L);
        Compagne compagne2 = new Compagne();
        compagne2.setId(compagne1.getId());
        assertThat(compagne1).isEqualTo(compagne2);
        compagne2.setId(2L);
        assertThat(compagne1).isNotEqualTo(compagne2);
        compagne1.setId(null);
        assertThat(compagne1).isNotEqualTo(compagne2);
    }
}
