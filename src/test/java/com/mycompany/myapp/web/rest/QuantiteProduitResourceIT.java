package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Prixproduit;
import com.mycompany.myapp.domain.QuantiteProduit;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.QuantiteProduitRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link QuantiteProduitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class QuantiteProduitResourceIT {

    private static final Float DEFAULT_QUANTITE = 1F;
    private static final Float UPDATED_QUANTITE = 2F;

    private static final Instant DEFAULT_DATE_AJOUT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_AJOUT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Float DEFAULT_QUANTITE_RESTANT = 1F;
    private static final Float UPDATED_QUANTITE_RESTANT = 2F;

    private static final String ENTITY_API_URL = "/api/quantite-produits";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private QuantiteProduitRepository quantiteProduitRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuantiteProduitMockMvc;

    private QuantiteProduit quantiteProduit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuantiteProduit createEntity(EntityManager em) {
        QuantiteProduit quantiteProduit = new QuantiteProduit()
            .quantite(DEFAULT_QUANTITE)
            .dateAjout(DEFAULT_DATE_AJOUT)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .description(DEFAULT_DESCRIPTION)
            .quantiteRestant(DEFAULT_QUANTITE_RESTANT);
        // Add required entity
        Prixproduit prixproduit;
        if (TestUtil.findAll(em, Prixproduit.class).isEmpty()) {
            prixproduit = PrixproduitResourceIT.createEntity(em);
            em.persist(prixproduit);
            em.flush();
        } else {
            prixproduit = TestUtil.findAll(em, Prixproduit.class).get(0);
        }
        quantiteProduit.setPrixproduit(prixproduit);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        quantiteProduit.setUser(user);
        return quantiteProduit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuantiteProduit createUpdatedEntity(EntityManager em) {
        QuantiteProduit quantiteProduit = new QuantiteProduit()
            .quantite(UPDATED_QUANTITE)
            .dateAjout(UPDATED_DATE_AJOUT)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .description(UPDATED_DESCRIPTION)
            .quantiteRestant(UPDATED_QUANTITE_RESTANT);
        // Add required entity
        Prixproduit prixproduit;
        if (TestUtil.findAll(em, Prixproduit.class).isEmpty()) {
            prixproduit = PrixproduitResourceIT.createUpdatedEntity(em);
            em.persist(prixproduit);
            em.flush();
        } else {
            prixproduit = TestUtil.findAll(em, Prixproduit.class).get(0);
        }
        quantiteProduit.setPrixproduit(prixproduit);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        quantiteProduit.setUser(user);
        return quantiteProduit;
    }

    @BeforeEach
    public void initTest() {
        quantiteProduit = createEntity(em);
    }

    @Test
    @Transactional
    void createQuantiteProduit() throws Exception {
        int databaseSizeBeforeCreate = quantiteProduitRepository.findAll().size();
        // Create the QuantiteProduit
        restQuantiteProduitMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isCreated());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeCreate + 1);
        QuantiteProduit testQuantiteProduit = quantiteProduitList.get(quantiteProduitList.size() - 1);
        assertThat(testQuantiteProduit.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testQuantiteProduit.getDateAjout()).isEqualTo(DEFAULT_DATE_AJOUT);
        assertThat(testQuantiteProduit.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testQuantiteProduit.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testQuantiteProduit.getQuantiteRestant()).isEqualTo(DEFAULT_QUANTITE_RESTANT);
    }

    @Test
    @Transactional
    void createQuantiteProduitWithExistingId() throws Exception {
        // Create the QuantiteProduit with an existing ID
        quantiteProduit.setId(1L);

        int databaseSizeBeforeCreate = quantiteProduitRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuantiteProduitMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkQuantiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = quantiteProduitRepository.findAll().size();
        // set the field null
        quantiteProduit.setQuantite(null);

        // Create the QuantiteProduit, which fails.

        restQuantiteProduitMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateAjoutIsRequired() throws Exception {
        int databaseSizeBeforeTest = quantiteProduitRepository.findAll().size();
        // set the field null
        quantiteProduit.setDateAjout(null);

        // Create the QuantiteProduit, which fails.

        restQuantiteProduitMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllQuantiteProduits() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        // Get all the quantiteProduitList
        restQuantiteProduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quantiteProduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE.doubleValue())))
            .andExpect(jsonPath("$.[*].dateAjout").value(hasItem(DEFAULT_DATE_AJOUT.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].quantiteRestant").value(hasItem(DEFAULT_QUANTITE_RESTANT.doubleValue())));
    }

    @Test
    @Transactional
    void getQuantiteProduit() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        // Get the quantiteProduit
        restQuantiteProduitMockMvc
            .perform(get(ENTITY_API_URL_ID, quantiteProduit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(quantiteProduit.getId().intValue()))
            .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE.doubleValue()))
            .andExpect(jsonPath("$.dateAjout").value(DEFAULT_DATE_AJOUT.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.quantiteRestant").value(DEFAULT_QUANTITE_RESTANT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingQuantiteProduit() throws Exception {
        // Get the quantiteProduit
        restQuantiteProduitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewQuantiteProduit() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();

        // Update the quantiteProduit
        QuantiteProduit updatedQuantiteProduit = quantiteProduitRepository.findById(quantiteProduit.getId()).get();
        // Disconnect from session so that the updates on updatedQuantiteProduit are not directly saved in db
        em.detach(updatedQuantiteProduit);
        updatedQuantiteProduit
            .quantite(UPDATED_QUANTITE)
            .dateAjout(UPDATED_DATE_AJOUT)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .description(UPDATED_DESCRIPTION)
            .quantiteRestant(UPDATED_QUANTITE_RESTANT);

        restQuantiteProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedQuantiteProduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedQuantiteProduit))
            )
            .andExpect(status().isOk());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
        QuantiteProduit testQuantiteProduit = quantiteProduitList.get(quantiteProduitList.size() - 1);
        assertThat(testQuantiteProduit.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testQuantiteProduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testQuantiteProduit.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testQuantiteProduit.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testQuantiteProduit.getQuantiteRestant()).isEqualTo(UPDATED_QUANTITE_RESTANT);
    }

    @Test
    @Transactional
    void putNonExistingQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, quantiteProduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateQuantiteProduitWithPatch() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();

        // Update the quantiteProduit using partial update
        QuantiteProduit partialUpdatedQuantiteProduit = new QuantiteProduit();
        partialUpdatedQuantiteProduit.setId(quantiteProduit.getId());

        partialUpdatedQuantiteProduit.dateAjout(UPDATED_DATE_AJOUT).dateModification(UPDATED_DATE_MODIFICATION);

        restQuantiteProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuantiteProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuantiteProduit))
            )
            .andExpect(status().isOk());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
        QuantiteProduit testQuantiteProduit = quantiteProduitList.get(quantiteProduitList.size() - 1);
        assertThat(testQuantiteProduit.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testQuantiteProduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testQuantiteProduit.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testQuantiteProduit.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testQuantiteProduit.getQuantiteRestant()).isEqualTo(DEFAULT_QUANTITE_RESTANT);
    }

    @Test
    @Transactional
    void fullUpdateQuantiteProduitWithPatch() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();

        // Update the quantiteProduit using partial update
        QuantiteProduit partialUpdatedQuantiteProduit = new QuantiteProduit();
        partialUpdatedQuantiteProduit.setId(quantiteProduit.getId());

        partialUpdatedQuantiteProduit
            .quantite(UPDATED_QUANTITE)
            .dateAjout(UPDATED_DATE_AJOUT)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .description(UPDATED_DESCRIPTION)
            .quantiteRestant(UPDATED_QUANTITE_RESTANT);

        restQuantiteProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuantiteProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuantiteProduit))
            )
            .andExpect(status().isOk());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
        QuantiteProduit testQuantiteProduit = quantiteProduitList.get(quantiteProduitList.size() - 1);
        assertThat(testQuantiteProduit.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testQuantiteProduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testQuantiteProduit.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testQuantiteProduit.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testQuantiteProduit.getQuantiteRestant()).isEqualTo(UPDATED_QUANTITE_RESTANT);
    }

    @Test
    @Transactional
    void patchNonExistingQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, quantiteProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamQuantiteProduit() throws Exception {
        int databaseSizeBeforeUpdate = quantiteProduitRepository.findAll().size();
        quantiteProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuantiteProduitMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(quantiteProduit))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the QuantiteProduit in the database
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteQuantiteProduit() throws Exception {
        // Initialize the database
        quantiteProduitRepository.saveAndFlush(quantiteProduit);

        int databaseSizeBeforeDelete = quantiteProduitRepository.findAll().size();

        // Delete the quantiteProduit
        restQuantiteProduitMockMvc
            .perform(delete(ENTITY_API_URL_ID, quantiteProduit.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<QuantiteProduit> quantiteProduitList = quantiteProduitRepository.findAll();
        assertThat(quantiteProduitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
