package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Compagne;
import com.mycompany.myapp.repository.CompagneRepository;
import com.mycompany.myapp.service.criteria.CompagneCriteria;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CompagneResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CompagneResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CREATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATION = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_CREATION = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_ETAT = false;
    private static final Boolean UPDATED_ETAT = true;

    private static final String ENTITY_API_URL = "/api/compagnes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CompagneRepository compagneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompagneMockMvc;

    private Compagne compagne;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Compagne createEntity(EntityManager em) {
        Compagne compagne = new Compagne().libelle(DEFAULT_LIBELLE).dateCreation(DEFAULT_DATE_CREATION).etat(DEFAULT_ETAT);
        return compagne;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Compagne createUpdatedEntity(EntityManager em) {
        Compagne compagne = new Compagne().libelle(UPDATED_LIBELLE).dateCreation(UPDATED_DATE_CREATION).etat(UPDATED_ETAT);
        return compagne;
    }

    @BeforeEach
    public void initTest() {
        compagne = createEntity(em);
    }

    @Test
    @Transactional
    void createCompagne() throws Exception {
        int databaseSizeBeforeCreate = compagneRepository.findAll().size();
        // Create the Compagne
        restCompagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isCreated());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeCreate + 1);
        Compagne testCompagne = compagneList.get(compagneList.size() - 1);
        assertThat(testCompagne.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testCompagne.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testCompagne.getEtat()).isEqualTo(DEFAULT_ETAT);
    }

    @Test
    @Transactional
    void createCompagneWithExistingId() throws Exception {
        // Create the Compagne with an existing ID
        compagne.setId(1L);

        int databaseSizeBeforeCreate = compagneRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isBadRequest());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = compagneRepository.findAll().size();
        // set the field null
        compagne.setLibelle(null);

        // Create the Compagne, which fails.

        restCompagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isBadRequest());

        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateCreationIsRequired() throws Exception {
        int databaseSizeBeforeTest = compagneRepository.findAll().size();
        // set the field null
        compagne.setDateCreation(null);

        // Create the Compagne, which fails.

        restCompagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isBadRequest());

        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEtatIsRequired() throws Exception {
        int databaseSizeBeforeTest = compagneRepository.findAll().size();
        // set the field null
        compagne.setEtat(null);

        // Create the Compagne, which fails.

        restCompagneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isBadRequest());

        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCompagnes() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compagne.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }

    @Test
    @Transactional
    void getCompagne() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get the compagne
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL_ID, compagne.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compagne.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.booleanValue()));
    }

    @Test
    @Transactional
    void getCompagnesByIdFiltering() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        Long id = compagne.getId();

        defaultCompagneShouldBeFound("id.equals=" + id);
        defaultCompagneShouldNotBeFound("id.notEquals=" + id);

        defaultCompagneShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompagneShouldNotBeFound("id.greaterThan=" + id);

        defaultCompagneShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompagneShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleIsEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle equals to DEFAULT_LIBELLE
        defaultCompagneShouldBeFound("libelle.equals=" + DEFAULT_LIBELLE);

        // Get all the compagneList where libelle equals to UPDATED_LIBELLE
        defaultCompagneShouldNotBeFound("libelle.equals=" + UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle not equals to DEFAULT_LIBELLE
        defaultCompagneShouldNotBeFound("libelle.notEquals=" + DEFAULT_LIBELLE);

        // Get all the compagneList where libelle not equals to UPDATED_LIBELLE
        defaultCompagneShouldBeFound("libelle.notEquals=" + UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleIsInShouldWork() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle in DEFAULT_LIBELLE or UPDATED_LIBELLE
        defaultCompagneShouldBeFound("libelle.in=" + DEFAULT_LIBELLE + "," + UPDATED_LIBELLE);

        // Get all the compagneList where libelle equals to UPDATED_LIBELLE
        defaultCompagneShouldNotBeFound("libelle.in=" + UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleIsNullOrNotNull() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle is not null
        defaultCompagneShouldBeFound("libelle.specified=true");

        // Get all the compagneList where libelle is null
        defaultCompagneShouldNotBeFound("libelle.specified=false");
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleContainsSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle contains DEFAULT_LIBELLE
        defaultCompagneShouldBeFound("libelle.contains=" + DEFAULT_LIBELLE);

        // Get all the compagneList where libelle contains UPDATED_LIBELLE
        defaultCompagneShouldNotBeFound("libelle.contains=" + UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void getAllCompagnesByLibelleNotContainsSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where libelle does not contain DEFAULT_LIBELLE
        defaultCompagneShouldNotBeFound("libelle.doesNotContain=" + DEFAULT_LIBELLE);

        // Get all the compagneList where libelle does not contain UPDATED_LIBELLE
        defaultCompagneShouldBeFound("libelle.doesNotContain=" + UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation equals to UPDATED_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the compagneList where dateCreation equals to UPDATED_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation is not null
        defaultCompagneShouldBeFound("dateCreation.specified=true");

        // Get all the compagneList where dateCreation is null
        defaultCompagneShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation is greater than or equal to DEFAULT_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.greaterThanOrEqual=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation is greater than or equal to UPDATED_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.greaterThanOrEqual=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation is less than or equal to DEFAULT_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.lessThanOrEqual=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation is less than or equal to SMALLER_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.lessThanOrEqual=" + SMALLER_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsLessThanSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation is less than DEFAULT_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.lessThan=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation is less than UPDATED_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.lessThan=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByDateCreationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where dateCreation is greater than DEFAULT_DATE_CREATION
        defaultCompagneShouldNotBeFound("dateCreation.greaterThan=" + DEFAULT_DATE_CREATION);

        // Get all the compagneList where dateCreation is greater than SMALLER_DATE_CREATION
        defaultCompagneShouldBeFound("dateCreation.greaterThan=" + SMALLER_DATE_CREATION);
    }

    @Test
    @Transactional
    void getAllCompagnesByEtatIsEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where etat equals to DEFAULT_ETAT
        defaultCompagneShouldBeFound("etat.equals=" + DEFAULT_ETAT);

        // Get all the compagneList where etat equals to UPDATED_ETAT
        defaultCompagneShouldNotBeFound("etat.equals=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    void getAllCompagnesByEtatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where etat not equals to DEFAULT_ETAT
        defaultCompagneShouldNotBeFound("etat.notEquals=" + DEFAULT_ETAT);

        // Get all the compagneList where etat not equals to UPDATED_ETAT
        defaultCompagneShouldBeFound("etat.notEquals=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    void getAllCompagnesByEtatIsInShouldWork() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where etat in DEFAULT_ETAT or UPDATED_ETAT
        defaultCompagneShouldBeFound("etat.in=" + DEFAULT_ETAT + "," + UPDATED_ETAT);

        // Get all the compagneList where etat equals to UPDATED_ETAT
        defaultCompagneShouldNotBeFound("etat.in=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    void getAllCompagnesByEtatIsNullOrNotNull() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        // Get all the compagneList where etat is not null
        defaultCompagneShouldBeFound("etat.specified=true");

        // Get all the compagneList where etat is null
        defaultCompagneShouldNotBeFound("etat.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompagneShouldBeFound(String filter) throws Exception {
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compagne.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));

        // Check, that the count call also returns 1
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompagneShouldNotBeFound(String filter) throws Exception {
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompagneMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCompagne() throws Exception {
        // Get the compagne
        restCompagneMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCompagne() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();

        // Update the compagne
        Compagne updatedCompagne = compagneRepository.findById(compagne.getId()).get();
        // Disconnect from session so that the updates on updatedCompagne are not directly saved in db
        em.detach(updatedCompagne);
        updatedCompagne.libelle(UPDATED_LIBELLE).dateCreation(UPDATED_DATE_CREATION).etat(UPDATED_ETAT);

        restCompagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCompagne.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCompagne))
            )
            .andExpect(status().isOk());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
        Compagne testCompagne = compagneList.get(compagneList.size() - 1);
        assertThat(testCompagne.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testCompagne.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testCompagne.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    void putNonExistingCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, compagne.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(compagne))
            )
            .andExpect(status().isBadRequest());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(compagne))
            )
            .andExpect(status().isBadRequest());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCompagneWithPatch() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();

        // Update the compagne using partial update
        Compagne partialUpdatedCompagne = new Compagne();
        partialUpdatedCompagne.setId(compagne.getId());

        partialUpdatedCompagne.etat(UPDATED_ETAT);

        restCompagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompagne.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompagne))
            )
            .andExpect(status().isOk());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
        Compagne testCompagne = compagneList.get(compagneList.size() - 1);
        assertThat(testCompagne.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testCompagne.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testCompagne.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    void fullUpdateCompagneWithPatch() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();

        // Update the compagne using partial update
        Compagne partialUpdatedCompagne = new Compagne();
        partialUpdatedCompagne.setId(compagne.getId());

        partialUpdatedCompagne.libelle(UPDATED_LIBELLE).dateCreation(UPDATED_DATE_CREATION).etat(UPDATED_ETAT);

        restCompagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompagne.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompagne))
            )
            .andExpect(status().isOk());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
        Compagne testCompagne = compagneList.get(compagneList.size() - 1);
        assertThat(testCompagne.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testCompagne.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testCompagne.getEtat()).isEqualTo(UPDATED_ETAT);
    }

    @Test
    @Transactional
    void patchNonExistingCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, compagne.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(compagne))
            )
            .andExpect(status().isBadRequest());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(compagne))
            )
            .andExpect(status().isBadRequest());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCompagne() throws Exception {
        int databaseSizeBeforeUpdate = compagneRepository.findAll().size();
        compagne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompagneMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(compagne)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Compagne in the database
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCompagne() throws Exception {
        // Initialize the database
        compagneRepository.saveAndFlush(compagne);

        int databaseSizeBeforeDelete = compagneRepository.findAll().size();

        // Delete the compagne
        restCompagneMockMvc
            .perform(delete(ENTITY_API_URL_ID, compagne.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Compagne> compagneList = compagneRepository.findAll();
        assertThat(compagneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
