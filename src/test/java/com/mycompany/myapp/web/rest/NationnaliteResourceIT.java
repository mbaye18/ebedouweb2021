package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Nationnalite;
import com.mycompany.myapp.repository.NationnaliteRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NationnaliteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class NationnaliteResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/nationnalites";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NationnaliteRepository nationnaliteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNationnaliteMockMvc;

    private Nationnalite nationnalite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Nationnalite createEntity(EntityManager em) {
        Nationnalite nationnalite = new Nationnalite().libelle(DEFAULT_LIBELLE);
        return nationnalite;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Nationnalite createUpdatedEntity(EntityManager em) {
        Nationnalite nationnalite = new Nationnalite().libelle(UPDATED_LIBELLE);
        return nationnalite;
    }

    @BeforeEach
    public void initTest() {
        nationnalite = createEntity(em);
    }

    @Test
    @Transactional
    void createNationnalite() throws Exception {
        int databaseSizeBeforeCreate = nationnaliteRepository.findAll().size();
        // Create the Nationnalite
        restNationnaliteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nationnalite)))
            .andExpect(status().isCreated());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeCreate + 1);
        Nationnalite testNationnalite = nationnaliteList.get(nationnaliteList.size() - 1);
        assertThat(testNationnalite.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    void createNationnaliteWithExistingId() throws Exception {
        // Create the Nationnalite with an existing ID
        nationnalite.setId(1L);

        int databaseSizeBeforeCreate = nationnaliteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNationnaliteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nationnalite)))
            .andExpect(status().isBadRequest());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = nationnaliteRepository.findAll().size();
        // set the field null
        nationnalite.setLibelle(null);

        // Create the Nationnalite, which fails.

        restNationnaliteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nationnalite)))
            .andExpect(status().isBadRequest());

        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllNationnalites() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        // Get all the nationnaliteList
        restNationnaliteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nationnalite.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }

    @Test
    @Transactional
    void getNationnalite() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        // Get the nationnalite
        restNationnaliteMockMvc
            .perform(get(ENTITY_API_URL_ID, nationnalite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nationnalite.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }

    @Test
    @Transactional
    void getNonExistingNationnalite() throws Exception {
        // Get the nationnalite
        restNationnaliteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewNationnalite() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();

        // Update the nationnalite
        Nationnalite updatedNationnalite = nationnaliteRepository.findById(nationnalite.getId()).get();
        // Disconnect from session so that the updates on updatedNationnalite are not directly saved in db
        em.detach(updatedNationnalite);
        updatedNationnalite.libelle(UPDATED_LIBELLE);

        restNationnaliteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedNationnalite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedNationnalite))
            )
            .andExpect(status().isOk());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
        Nationnalite testNationnalite = nationnaliteList.get(nationnaliteList.size() - 1);
        assertThat(testNationnalite.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void putNonExistingNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, nationnalite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nationnalite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nationnalite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nationnalite)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNationnaliteWithPatch() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();

        // Update the nationnalite using partial update
        Nationnalite partialUpdatedNationnalite = new Nationnalite();
        partialUpdatedNationnalite.setId(nationnalite.getId());

        partialUpdatedNationnalite.libelle(UPDATED_LIBELLE);

        restNationnaliteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNationnalite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNationnalite))
            )
            .andExpect(status().isOk());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
        Nationnalite testNationnalite = nationnaliteList.get(nationnaliteList.size() - 1);
        assertThat(testNationnalite.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void fullUpdateNationnaliteWithPatch() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();

        // Update the nationnalite using partial update
        Nationnalite partialUpdatedNationnalite = new Nationnalite();
        partialUpdatedNationnalite.setId(nationnalite.getId());

        partialUpdatedNationnalite.libelle(UPDATED_LIBELLE);

        restNationnaliteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNationnalite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNationnalite))
            )
            .andExpect(status().isOk());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
        Nationnalite testNationnalite = nationnaliteList.get(nationnaliteList.size() - 1);
        assertThat(testNationnalite.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void patchNonExistingNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, nationnalite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nationnalite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nationnalite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNationnalite() throws Exception {
        int databaseSizeBeforeUpdate = nationnaliteRepository.findAll().size();
        nationnalite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNationnaliteMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(nationnalite))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Nationnalite in the database
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNationnalite() throws Exception {
        // Initialize the database
        nationnaliteRepository.saveAndFlush(nationnalite);

        int databaseSizeBeforeDelete = nationnaliteRepository.findAll().size();

        // Delete the nationnalite
        restNationnaliteMockMvc
            .perform(delete(ENTITY_API_URL_ID, nationnalite.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Nationnalite> nationnaliteList = nationnaliteRepository.findAll();
        assertThat(nationnaliteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
