package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.ImageProduit;
import com.mycompany.myapp.repository.ImageProduitRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ImageProduitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ImageProduitResourceIT {

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/image-produits";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ImageProduitRepository imageProduitRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImageProduitMockMvc;

    private ImageProduit imageProduit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageProduit createEntity(EntityManager em) {
        ImageProduit imageProduit = new ImageProduit().image(DEFAULT_IMAGE).imageContentType(DEFAULT_IMAGE_CONTENT_TYPE).date(DEFAULT_DATE);
        return imageProduit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageProduit createUpdatedEntity(EntityManager em) {
        ImageProduit imageProduit = new ImageProduit().image(UPDATED_IMAGE).imageContentType(UPDATED_IMAGE_CONTENT_TYPE).date(UPDATED_DATE);
        return imageProduit;
    }

    @BeforeEach
    public void initTest() {
        imageProduit = createEntity(em);
    }

    @Test
    @Transactional
    void createImageProduit() throws Exception {
        int databaseSizeBeforeCreate = imageProduitRepository.findAll().size();
        // Create the ImageProduit
        restImageProduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imageProduit)))
            .andExpect(status().isCreated());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeCreate + 1);
        ImageProduit testImageProduit = imageProduitList.get(imageProduitList.size() - 1);
        assertThat(testImageProduit.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testImageProduit.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testImageProduit.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    void createImageProduitWithExistingId() throws Exception {
        // Create the ImageProduit with an existing ID
        imageProduit.setId(1L);

        int databaseSizeBeforeCreate = imageProduitRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageProduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imageProduit)))
            .andExpect(status().isBadRequest());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageProduitRepository.findAll().size();
        // set the field null
        imageProduit.setDate(null);

        // Create the ImageProduit, which fails.

        restImageProduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imageProduit)))
            .andExpect(status().isBadRequest());

        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllImageProduits() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        // Get all the imageProduitList
        restImageProduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageProduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }

    @Test
    @Transactional
    void getImageProduit() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        // Get the imageProduit
        restImageProduitMockMvc
            .perform(get(ENTITY_API_URL_ID, imageProduit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(imageProduit.getId().intValue()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingImageProduit() throws Exception {
        // Get the imageProduit
        restImageProduitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewImageProduit() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();

        // Update the imageProduit
        ImageProduit updatedImageProduit = imageProduitRepository.findById(imageProduit.getId()).get();
        // Disconnect from session so that the updates on updatedImageProduit are not directly saved in db
        em.detach(updatedImageProduit);
        updatedImageProduit.image(UPDATED_IMAGE).imageContentType(UPDATED_IMAGE_CONTENT_TYPE).date(UPDATED_DATE);

        restImageProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedImageProduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedImageProduit))
            )
            .andExpect(status().isOk());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
        ImageProduit testImageProduit = imageProduitList.get(imageProduitList.size() - 1);
        assertThat(testImageProduit.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testImageProduit.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testImageProduit.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, imageProduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imageProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imageProduit)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateImageProduitWithPatch() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();

        // Update the imageProduit using partial update
        ImageProduit partialUpdatedImageProduit = new ImageProduit();
        partialUpdatedImageProduit.setId(imageProduit.getId());

        partialUpdatedImageProduit.image(UPDATED_IMAGE).imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restImageProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImageProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImageProduit))
            )
            .andExpect(status().isOk());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
        ImageProduit testImageProduit = imageProduitList.get(imageProduitList.size() - 1);
        assertThat(testImageProduit.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testImageProduit.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testImageProduit.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    void fullUpdateImageProduitWithPatch() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();

        // Update the imageProduit using partial update
        ImageProduit partialUpdatedImageProduit = new ImageProduit();
        partialUpdatedImageProduit.setId(imageProduit.getId());

        partialUpdatedImageProduit.image(UPDATED_IMAGE).imageContentType(UPDATED_IMAGE_CONTENT_TYPE).date(UPDATED_DATE);

        restImageProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImageProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImageProduit))
            )
            .andExpect(status().isOk());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
        ImageProduit testImageProduit = imageProduitList.get(imageProduitList.size() - 1);
        assertThat(testImageProduit.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testImageProduit.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testImageProduit.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, imageProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imageProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imageProduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamImageProduit() throws Exception {
        int databaseSizeBeforeUpdate = imageProduitRepository.findAll().size();
        imageProduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImageProduitMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(imageProduit))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ImageProduit in the database
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteImageProduit() throws Exception {
        // Initialize the database
        imageProduitRepository.saveAndFlush(imageProduit);

        int databaseSizeBeforeDelete = imageProduitRepository.findAll().size();

        // Delete the imageProduit
        restImageProduitMockMvc
            .perform(delete(ENTITY_API_URL_ID, imageProduit.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImageProduit> imageProduitList = imageProduitRepository.findAll();
        assertThat(imageProduitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
