package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Compagne;
import com.mycompany.myapp.domain.Prixproduit;
import com.mycompany.myapp.domain.Produit;
import com.mycompany.myapp.repository.PrixproduitRepository;
import com.mycompany.myapp.service.criteria.PrixproduitCriteria;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PrixproduitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PrixproduitResourceIT {

    private static final Double DEFAULT_PRIX = 1D;
    private static final Double UPDATED_PRIX = 2D;
    private static final Double SMALLER_PRIX = 1D - 1D;

    private static final Instant DEFAULT_DATE_AJOUT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_AJOUT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/prixproduits";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PrixproduitRepository prixproduitRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPrixproduitMockMvc;

    private Prixproduit prixproduit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prixproduit createEntity(EntityManager em) {
        Prixproduit prixproduit = new Prixproduit()
            .prix(DEFAULT_PRIX)
            .dateAjout(DEFAULT_DATE_AJOUT)
            .dateModification(DEFAULT_DATE_MODIFICATION);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createEntity(em);
            em.persist(produit);
            em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        prixproduit.setProduit(produit);
        // Add required entity
        Compagne compagne;
        if (TestUtil.findAll(em, Compagne.class).isEmpty()) {
            compagne = CompagneResourceIT.createEntity(em);
            em.persist(compagne);
            em.flush();
        } else {
            compagne = TestUtil.findAll(em, Compagne.class).get(0);
        }
        prixproduit.setCompagne(compagne);
        return prixproduit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prixproduit createUpdatedEntity(EntityManager em) {
        Prixproduit prixproduit = new Prixproduit()
            .prix(UPDATED_PRIX)
            .dateAjout(UPDATED_DATE_AJOUT)
            .dateModification(UPDATED_DATE_MODIFICATION);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createUpdatedEntity(em);
            em.persist(produit);
            em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        prixproduit.setProduit(produit);
        // Add required entity
        Compagne compagne;
        if (TestUtil.findAll(em, Compagne.class).isEmpty()) {
            compagne = CompagneResourceIT.createUpdatedEntity(em);
            em.persist(compagne);
            em.flush();
        } else {
            compagne = TestUtil.findAll(em, Compagne.class).get(0);
        }
        prixproduit.setCompagne(compagne);
        return prixproduit;
    }

    @BeforeEach
    public void initTest() {
        prixproduit = createEntity(em);
    }

    @Test
    @Transactional
    void createPrixproduit() throws Exception {
        int databaseSizeBeforeCreate = prixproduitRepository.findAll().size();
        // Create the Prixproduit
        restPrixproduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isCreated());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeCreate + 1);
        Prixproduit testPrixproduit = prixproduitList.get(prixproduitList.size() - 1);
        assertThat(testPrixproduit.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testPrixproduit.getDateAjout()).isEqualTo(DEFAULT_DATE_AJOUT);
        assertThat(testPrixproduit.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void createPrixproduitWithExistingId() throws Exception {
        // Create the Prixproduit with an existing ID
        prixproduit.setId(1L);

        int databaseSizeBeforeCreate = prixproduitRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrixproduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isBadRequest());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPrixIsRequired() throws Exception {
        int databaseSizeBeforeTest = prixproduitRepository.findAll().size();
        // set the field null
        prixproduit.setPrix(null);

        // Create the Prixproduit, which fails.

        restPrixproduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isBadRequest());

        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateAjoutIsRequired() throws Exception {
        int databaseSizeBeforeTest = prixproduitRepository.findAll().size();
        // set the field null
        prixproduit.setDateAjout(null);

        // Create the Prixproduit, which fails.

        restPrixproduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isBadRequest());

        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateModificationIsRequired() throws Exception {
        int databaseSizeBeforeTest = prixproduitRepository.findAll().size();
        // set the field null
        prixproduit.setDateModification(null);

        // Create the Prixproduit, which fails.

        restPrixproduitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isBadRequest());

        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPrixproduits() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prixproduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX.doubleValue())))
            .andExpect(jsonPath("$.[*].dateAjout").value(hasItem(DEFAULT_DATE_AJOUT.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())));
    }

    @Test
    @Transactional
    void getPrixproduit() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get the prixproduit
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL_ID, prixproduit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(prixproduit.getId().intValue()))
            .andExpect(jsonPath("$.prix").value(DEFAULT_PRIX.doubleValue()))
            .andExpect(jsonPath("$.dateAjout").value(DEFAULT_DATE_AJOUT.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()));
    }

    @Test
    @Transactional
    void getPrixproduitsByIdFiltering() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        Long id = prixproduit.getId();

        defaultPrixproduitShouldBeFound("id.equals=" + id);
        defaultPrixproduitShouldNotBeFound("id.notEquals=" + id);

        defaultPrixproduitShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPrixproduitShouldNotBeFound("id.greaterThan=" + id);

        defaultPrixproduitShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPrixproduitShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix equals to DEFAULT_PRIX
        defaultPrixproduitShouldBeFound("prix.equals=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix equals to UPDATED_PRIX
        defaultPrixproduitShouldNotBeFound("prix.equals=" + UPDATED_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsNotEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix not equals to DEFAULT_PRIX
        defaultPrixproduitShouldNotBeFound("prix.notEquals=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix not equals to UPDATED_PRIX
        defaultPrixproduitShouldBeFound("prix.notEquals=" + UPDATED_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsInShouldWork() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix in DEFAULT_PRIX or UPDATED_PRIX
        defaultPrixproduitShouldBeFound("prix.in=" + DEFAULT_PRIX + "," + UPDATED_PRIX);

        // Get all the prixproduitList where prix equals to UPDATED_PRIX
        defaultPrixproduitShouldNotBeFound("prix.in=" + UPDATED_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsNullOrNotNull() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix is not null
        defaultPrixproduitShouldBeFound("prix.specified=true");

        // Get all the prixproduitList where prix is null
        defaultPrixproduitShouldNotBeFound("prix.specified=false");
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix is greater than or equal to DEFAULT_PRIX
        defaultPrixproduitShouldBeFound("prix.greaterThanOrEqual=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix is greater than or equal to UPDATED_PRIX
        defaultPrixproduitShouldNotBeFound("prix.greaterThanOrEqual=" + UPDATED_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix is less than or equal to DEFAULT_PRIX
        defaultPrixproduitShouldBeFound("prix.lessThanOrEqual=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix is less than or equal to SMALLER_PRIX
        defaultPrixproduitShouldNotBeFound("prix.lessThanOrEqual=" + SMALLER_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsLessThanSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix is less than DEFAULT_PRIX
        defaultPrixproduitShouldNotBeFound("prix.lessThan=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix is less than UPDATED_PRIX
        defaultPrixproduitShouldBeFound("prix.lessThan=" + UPDATED_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByPrixIsGreaterThanSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where prix is greater than DEFAULT_PRIX
        defaultPrixproduitShouldNotBeFound("prix.greaterThan=" + DEFAULT_PRIX);

        // Get all the prixproduitList where prix is greater than SMALLER_PRIX
        defaultPrixproduitShouldBeFound("prix.greaterThan=" + SMALLER_PRIX);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateAjoutIsEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateAjout equals to DEFAULT_DATE_AJOUT
        defaultPrixproduitShouldBeFound("dateAjout.equals=" + DEFAULT_DATE_AJOUT);

        // Get all the prixproduitList where dateAjout equals to UPDATED_DATE_AJOUT
        defaultPrixproduitShouldNotBeFound("dateAjout.equals=" + UPDATED_DATE_AJOUT);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateAjoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateAjout not equals to DEFAULT_DATE_AJOUT
        defaultPrixproduitShouldNotBeFound("dateAjout.notEquals=" + DEFAULT_DATE_AJOUT);

        // Get all the prixproduitList where dateAjout not equals to UPDATED_DATE_AJOUT
        defaultPrixproduitShouldBeFound("dateAjout.notEquals=" + UPDATED_DATE_AJOUT);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateAjoutIsInShouldWork() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateAjout in DEFAULT_DATE_AJOUT or UPDATED_DATE_AJOUT
        defaultPrixproduitShouldBeFound("dateAjout.in=" + DEFAULT_DATE_AJOUT + "," + UPDATED_DATE_AJOUT);

        // Get all the prixproduitList where dateAjout equals to UPDATED_DATE_AJOUT
        defaultPrixproduitShouldNotBeFound("dateAjout.in=" + UPDATED_DATE_AJOUT);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateAjoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateAjout is not null
        defaultPrixproduitShouldBeFound("dateAjout.specified=true");

        // Get all the prixproduitList where dateAjout is null
        defaultPrixproduitShouldNotBeFound("dateAjout.specified=false");
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultPrixproduitShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the prixproduitList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultPrixproduitShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultPrixproduitShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the prixproduitList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultPrixproduitShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultPrixproduitShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the prixproduitList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultPrixproduitShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void getAllPrixproduitsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        // Get all the prixproduitList where dateModification is not null
        defaultPrixproduitShouldBeFound("dateModification.specified=true");

        // Get all the prixproduitList where dateModification is null
        defaultPrixproduitShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    void getAllPrixproduitsByProduitIsEqualToSomething() throws Exception {
        // Get already existing entity
        Produit produit = prixproduit.getProduit();
        prixproduitRepository.saveAndFlush(prixproduit);
        Long produitId = produit.getId();

        // Get all the prixproduitList where produit equals to produitId
        defaultPrixproduitShouldBeFound("produitId.equals=" + produitId);

        // Get all the prixproduitList where produit equals to (produitId + 1)
        defaultPrixproduitShouldNotBeFound("produitId.equals=" + (produitId + 1));
    }

    @Test
    @Transactional
    void getAllPrixproduitsByCompagneIsEqualToSomething() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);
        Compagne compagne;
        if (TestUtil.findAll(em, Compagne.class).isEmpty()) {
            compagne = CompagneResourceIT.createEntity(em);
            em.persist(compagne);
            em.flush();
        } else {
            compagne = TestUtil.findAll(em, Compagne.class).get(0);
        }
        em.persist(compagne);
        em.flush();
        prixproduit.setCompagne(compagne);
        prixproduitRepository.saveAndFlush(prixproduit);
        Long compagneId = compagne.getId();

        // Get all the prixproduitList where compagne equals to compagneId
        defaultPrixproduitShouldBeFound("compagneId.equals=" + compagneId);

        // Get all the prixproduitList where compagne equals to (compagneId + 1)
        defaultPrixproduitShouldNotBeFound("compagneId.equals=" + (compagneId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPrixproduitShouldBeFound(String filter) throws Exception {
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prixproduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX.doubleValue())))
            .andExpect(jsonPath("$.[*].dateAjout").value(hasItem(DEFAULT_DATE_AJOUT.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())));

        // Check, that the count call also returns 1
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPrixproduitShouldNotBeFound(String filter) throws Exception {
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPrixproduitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPrixproduit() throws Exception {
        // Get the prixproduit
        restPrixproduitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPrixproduit() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();

        // Update the prixproduit
        Prixproduit updatedPrixproduit = prixproduitRepository.findById(prixproduit.getId()).get();
        // Disconnect from session so that the updates on updatedPrixproduit are not directly saved in db
        em.detach(updatedPrixproduit);
        updatedPrixproduit.prix(UPDATED_PRIX).dateAjout(UPDATED_DATE_AJOUT).dateModification(UPDATED_DATE_MODIFICATION);

        restPrixproduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPrixproduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPrixproduit))
            )
            .andExpect(status().isOk());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
        Prixproduit testPrixproduit = prixproduitList.get(prixproduitList.size() - 1);
        assertThat(testPrixproduit.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testPrixproduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testPrixproduit.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void putNonExistingPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prixproduit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prixproduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prixproduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(prixproduit)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePrixproduitWithPatch() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();

        // Update the prixproduit using partial update
        Prixproduit partialUpdatedPrixproduit = new Prixproduit();
        partialUpdatedPrixproduit.setId(prixproduit.getId());

        partialUpdatedPrixproduit.prix(UPDATED_PRIX).dateAjout(UPDATED_DATE_AJOUT);

        restPrixproduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrixproduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrixproduit))
            )
            .andExpect(status().isOk());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
        Prixproduit testPrixproduit = prixproduitList.get(prixproduitList.size() - 1);
        assertThat(testPrixproduit.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testPrixproduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testPrixproduit.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void fullUpdatePrixproduitWithPatch() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();

        // Update the prixproduit using partial update
        Prixproduit partialUpdatedPrixproduit = new Prixproduit();
        partialUpdatedPrixproduit.setId(prixproduit.getId());

        partialUpdatedPrixproduit.prix(UPDATED_PRIX).dateAjout(UPDATED_DATE_AJOUT).dateModification(UPDATED_DATE_MODIFICATION);

        restPrixproduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrixproduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrixproduit))
            )
            .andExpect(status().isOk());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
        Prixproduit testPrixproduit = prixproduitList.get(prixproduitList.size() - 1);
        assertThat(testPrixproduit.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testPrixproduit.getDateAjout()).isEqualTo(UPDATED_DATE_AJOUT);
        assertThat(testPrixproduit.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    void patchNonExistingPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, prixproduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prixproduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prixproduit))
            )
            .andExpect(status().isBadRequest());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPrixproduit() throws Exception {
        int databaseSizeBeforeUpdate = prixproduitRepository.findAll().size();
        prixproduit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrixproduitMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(prixproduit))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Prixproduit in the database
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePrixproduit() throws Exception {
        // Initialize the database
        prixproduitRepository.saveAndFlush(prixproduit);

        int databaseSizeBeforeDelete = prixproduitRepository.findAll().size();

        // Delete the prixproduit
        restPrixproduitMockMvc
            .perform(delete(ENTITY_API_URL_ID, prixproduit.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Prixproduit> prixproduitList = prixproduitRepository.findAll();
        assertThat(prixproduitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
