package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Cooperative;
import com.mycompany.myapp.domain.Infoplus;
import com.mycompany.myapp.domain.ListeCooperative;
import com.mycompany.myapp.repository.ListeCooperativeRepository;
import com.mycompany.myapp.service.criteria.ListeCooperativeCriteria;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ListeCooperativeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ListeCooperativeResourceIT {

    private static final String ENTITY_API_URL = "/api/liste-cooperatives";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ListeCooperativeRepository listeCooperativeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restListeCooperativeMockMvc;

    private ListeCooperative listeCooperative;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListeCooperative createEntity(EntityManager em) {
        ListeCooperative listeCooperative = new ListeCooperative();
        // Add required entity
        Infoplus infoplus;
        if (TestUtil.findAll(em, Infoplus.class).isEmpty()) {
            infoplus = InfoplusResourceIT.createEntity(em);
            em.persist(infoplus);
            em.flush();
        } else {
            infoplus = TestUtil.findAll(em, Infoplus.class).get(0);
        }
        listeCooperative.setMembre(infoplus);
        // Add required entity
        Cooperative cooperative;
        if (TestUtil.findAll(em, Cooperative.class).isEmpty()) {
            cooperative = CooperativeResourceIT.createEntity(em);
            em.persist(cooperative);
            em.flush();
        } else {
            cooperative = TestUtil.findAll(em, Cooperative.class).get(0);
        }
        listeCooperative.setCooperative(cooperative);
        return listeCooperative;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListeCooperative createUpdatedEntity(EntityManager em) {
        ListeCooperative listeCooperative = new ListeCooperative();
        // Add required entity
        Infoplus infoplus;
        if (TestUtil.findAll(em, Infoplus.class).isEmpty()) {
            infoplus = InfoplusResourceIT.createUpdatedEntity(em);
            em.persist(infoplus);
            em.flush();
        } else {
            infoplus = TestUtil.findAll(em, Infoplus.class).get(0);
        }
        listeCooperative.setMembre(infoplus);
        // Add required entity
        Cooperative cooperative;
        if (TestUtil.findAll(em, Cooperative.class).isEmpty()) {
            cooperative = CooperativeResourceIT.createUpdatedEntity(em);
            em.persist(cooperative);
            em.flush();
        } else {
            cooperative = TestUtil.findAll(em, Cooperative.class).get(0);
        }
        listeCooperative.setCooperative(cooperative);
        return listeCooperative;
    }

    @BeforeEach
    public void initTest() {
        listeCooperative = createEntity(em);
    }

    @Test
    @Transactional
    void createListeCooperative() throws Exception {
        int databaseSizeBeforeCreate = listeCooperativeRepository.findAll().size();
        // Create the ListeCooperative
        restListeCooperativeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isCreated());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeCreate + 1);
        ListeCooperative testListeCooperative = listeCooperativeList.get(listeCooperativeList.size() - 1);
    }

    @Test
    @Transactional
    void createListeCooperativeWithExistingId() throws Exception {
        // Create the ListeCooperative with an existing ID
        listeCooperative.setId(1L);

        int databaseSizeBeforeCreate = listeCooperativeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restListeCooperativeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllListeCooperatives() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        // Get all the listeCooperativeList
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listeCooperative.getId().intValue())));
    }

    @Test
    @Transactional
    void getListeCooperative() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        // Get the listeCooperative
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL_ID, listeCooperative.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(listeCooperative.getId().intValue()));
    }

    @Test
    @Transactional
    void getListeCooperativesByIdFiltering() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        Long id = listeCooperative.getId();

        defaultListeCooperativeShouldBeFound("id.equals=" + id);
        defaultListeCooperativeShouldNotBeFound("id.notEquals=" + id);

        defaultListeCooperativeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultListeCooperativeShouldNotBeFound("id.greaterThan=" + id);

        defaultListeCooperativeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultListeCooperativeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllListeCooperativesByMembreIsEqualToSomething() throws Exception {
        // Get already existing entity
        Infoplus membre = listeCooperative.getMembre();
        listeCooperativeRepository.saveAndFlush(listeCooperative);
        Long membreId = membre.getId();

        // Get all the listeCooperativeList where membre equals to membreId
        defaultListeCooperativeShouldBeFound("membreId.equals=" + membreId);

        // Get all the listeCooperativeList where membre equals to (membreId + 1)
        defaultListeCooperativeShouldNotBeFound("membreId.equals=" + (membreId + 1));
    }

    @Test
    @Transactional
    void getAllListeCooperativesByCooperativeIsEqualToSomething() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);
        Cooperative cooperative;
        if (TestUtil.findAll(em, Cooperative.class).isEmpty()) {
            cooperative = CooperativeResourceIT.createEntity(em);
            em.persist(cooperative);
            em.flush();
        } else {
            cooperative = TestUtil.findAll(em, Cooperative.class).get(0);
        }
        em.persist(cooperative);
        em.flush();
        listeCooperative.setCooperative(cooperative);
        listeCooperativeRepository.saveAndFlush(listeCooperative);
        Long cooperativeId = cooperative.getId();

        // Get all the listeCooperativeList where cooperative equals to cooperativeId
        defaultListeCooperativeShouldBeFound("cooperativeId.equals=" + cooperativeId);

        // Get all the listeCooperativeList where cooperative equals to (cooperativeId + 1)
        defaultListeCooperativeShouldNotBeFound("cooperativeId.equals=" + (cooperativeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultListeCooperativeShouldBeFound(String filter) throws Exception {
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listeCooperative.getId().intValue())));

        // Check, that the count call also returns 1
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultListeCooperativeShouldNotBeFound(String filter) throws Exception {
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restListeCooperativeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingListeCooperative() throws Exception {
        // Get the listeCooperative
        restListeCooperativeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewListeCooperative() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();

        // Update the listeCooperative
        ListeCooperative updatedListeCooperative = listeCooperativeRepository.findById(listeCooperative.getId()).get();
        // Disconnect from session so that the updates on updatedListeCooperative are not directly saved in db
        em.detach(updatedListeCooperative);

        restListeCooperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedListeCooperative.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedListeCooperative))
            )
            .andExpect(status().isOk());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
        ListeCooperative testListeCooperative = listeCooperativeList.get(listeCooperativeList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, listeCooperative.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateListeCooperativeWithPatch() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();

        // Update the listeCooperative using partial update
        ListeCooperative partialUpdatedListeCooperative = new ListeCooperative();
        partialUpdatedListeCooperative.setId(listeCooperative.getId());

        restListeCooperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListeCooperative.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListeCooperative))
            )
            .andExpect(status().isOk());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
        ListeCooperative testListeCooperative = listeCooperativeList.get(listeCooperativeList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateListeCooperativeWithPatch() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();

        // Update the listeCooperative using partial update
        ListeCooperative partialUpdatedListeCooperative = new ListeCooperative();
        partialUpdatedListeCooperative.setId(listeCooperative.getId());

        restListeCooperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListeCooperative.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListeCooperative))
            )
            .andExpect(status().isOk());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
        ListeCooperative testListeCooperative = listeCooperativeList.get(listeCooperativeList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, listeCooperative.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isBadRequest());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamListeCooperative() throws Exception {
        int databaseSizeBeforeUpdate = listeCooperativeRepository.findAll().size();
        listeCooperative.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListeCooperativeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listeCooperative))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ListeCooperative in the database
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteListeCooperative() throws Exception {
        // Initialize the database
        listeCooperativeRepository.saveAndFlush(listeCooperative);

        int databaseSizeBeforeDelete = listeCooperativeRepository.findAll().size();

        // Delete the listeCooperative
        restListeCooperativeMockMvc
            .perform(delete(ENTITY_API_URL_ID, listeCooperative.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ListeCooperative> listeCooperativeList = listeCooperativeRepository.findAll();
        assertThat(listeCooperativeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
