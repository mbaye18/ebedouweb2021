package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Infoplus;
import com.mycompany.myapp.domain.Nationnalite;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.InfoplusRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link InfoplusResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InfoplusResourceIT {

    private static final String DEFAULT_NUMERO_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_TELEPHONE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PIECE_IDENTITE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PIECE_IDENTITE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PIECE_IDENTITE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PIECE_IDENTITE_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_COPIE_AGREMENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_COPIE_AGREMENT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_COPIE_AGREMENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_COPIE_AGREMENT_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_DATE_NAISSANCE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LIEU_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_NAISSANCE = "BBBBBBBBBB";

    private static final String DEFAULT_GENRE = "AAAAAAAAAA";
    private static final String UPDATED_GENRE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/infopluses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private InfoplusRepository infoplusRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInfoplusMockMvc;

    private Infoplus infoplus;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Infoplus createEntity(EntityManager em) {
        Infoplus infoplus = new Infoplus()
            .numeroTelephone(DEFAULT_NUMERO_TELEPHONE)
            .pieceIdentite(DEFAULT_PIECE_IDENTITE)
            .pieceIdentiteContentType(DEFAULT_PIECE_IDENTITE_CONTENT_TYPE)
            .copieAgrement(DEFAULT_COPIE_AGREMENT)
            .copieAgrementContentType(DEFAULT_COPIE_AGREMENT_CONTENT_TYPE)
            .dateNaissance(DEFAULT_DATE_NAISSANCE)
            .lieuNaissance(DEFAULT_LIEU_NAISSANCE)
            .genre(DEFAULT_GENRE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        infoplus.setUser(user);
        // Add required entity
        Nationnalite nationnalite;
        if (TestUtil.findAll(em, Nationnalite.class).isEmpty()) {
            nationnalite = NationnaliteResourceIT.createEntity(em);
            em.persist(nationnalite);
            em.flush();
        } else {
            nationnalite = TestUtil.findAll(em, Nationnalite.class).get(0);
        }
        infoplus.setNationnalite(nationnalite);
        return infoplus;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Infoplus createUpdatedEntity(EntityManager em) {
        Infoplus infoplus = new Infoplus()
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .pieceIdentite(UPDATED_PIECE_IDENTITE)
            .pieceIdentiteContentType(UPDATED_PIECE_IDENTITE_CONTENT_TYPE)
            .copieAgrement(UPDATED_COPIE_AGREMENT)
            .copieAgrementContentType(UPDATED_COPIE_AGREMENT_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .lieuNaissance(UPDATED_LIEU_NAISSANCE)
            .genre(UPDATED_GENRE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        infoplus.setUser(user);
        // Add required entity
        Nationnalite nationnalite;
        if (TestUtil.findAll(em, Nationnalite.class).isEmpty()) {
            nationnalite = NationnaliteResourceIT.createUpdatedEntity(em);
            em.persist(nationnalite);
            em.flush();
        } else {
            nationnalite = TestUtil.findAll(em, Nationnalite.class).get(0);
        }
        infoplus.setNationnalite(nationnalite);
        return infoplus;
    }

    @BeforeEach
    public void initTest() {
        infoplus = createEntity(em);
    }

    @Test
    @Transactional
    void createInfoplus() throws Exception {
        int databaseSizeBeforeCreate = infoplusRepository.findAll().size();
        // Create the Infoplus
        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isCreated());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeCreate + 1);
        Infoplus testInfoplus = infoplusList.get(infoplusList.size() - 1);
        assertThat(testInfoplus.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testInfoplus.getPieceIdentite()).isEqualTo(DEFAULT_PIECE_IDENTITE);
        assertThat(testInfoplus.getPieceIdentiteContentType()).isEqualTo(DEFAULT_PIECE_IDENTITE_CONTENT_TYPE);
        assertThat(testInfoplus.getCopieAgrement()).isEqualTo(DEFAULT_COPIE_AGREMENT);
        assertThat(testInfoplus.getCopieAgrementContentType()).isEqualTo(DEFAULT_COPIE_AGREMENT_CONTENT_TYPE);
        assertThat(testInfoplus.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);
        assertThat(testInfoplus.getLieuNaissance()).isEqualTo(DEFAULT_LIEU_NAISSANCE);
        assertThat(testInfoplus.getGenre()).isEqualTo(DEFAULT_GENRE);
    }

    @Test
    @Transactional
    void createInfoplusWithExistingId() throws Exception {
        // Create the Infoplus with an existing ID
        infoplus.setId(1L);

        int databaseSizeBeforeCreate = infoplusRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isBadRequest());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNumeroTelephoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = infoplusRepository.findAll().size();
        // set the field null
        infoplus.setNumeroTelephone(null);

        // Create the Infoplus, which fails.

        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isBadRequest());

        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateNaissanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = infoplusRepository.findAll().size();
        // set the field null
        infoplus.setDateNaissance(null);

        // Create the Infoplus, which fails.

        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isBadRequest());

        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLieuNaissanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = infoplusRepository.findAll().size();
        // set the field null
        infoplus.setLieuNaissance(null);

        // Create the Infoplus, which fails.

        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isBadRequest());

        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGenreIsRequired() throws Exception {
        int databaseSizeBeforeTest = infoplusRepository.findAll().size();
        // set the field null
        infoplus.setGenre(null);

        // Create the Infoplus, which fails.

        restInfoplusMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isBadRequest());

        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllInfopluses() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        // Get all the infoplusList
        restInfoplusMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(infoplus.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroTelephone").value(hasItem(DEFAULT_NUMERO_TELEPHONE)))
            .andExpect(jsonPath("$.[*].pieceIdentiteContentType").value(hasItem(DEFAULT_PIECE_IDENTITE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].pieceIdentite").value(hasItem(Base64Utils.encodeToString(DEFAULT_PIECE_IDENTITE))))
            .andExpect(jsonPath("$.[*].copieAgrementContentType").value(hasItem(DEFAULT_COPIE_AGREMENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].copieAgrement").value(hasItem(Base64Utils.encodeToString(DEFAULT_COPIE_AGREMENT))))
            .andExpect(jsonPath("$.[*].dateNaissance").value(hasItem(DEFAULT_DATE_NAISSANCE.toString())))
            .andExpect(jsonPath("$.[*].lieuNaissance").value(hasItem(DEFAULT_LIEU_NAISSANCE)))
            .andExpect(jsonPath("$.[*].genre").value(hasItem(DEFAULT_GENRE)));
    }

    @Test
    @Transactional
    void getInfoplus() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        // Get the infoplus
        restInfoplusMockMvc
            .perform(get(ENTITY_API_URL_ID, infoplus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(infoplus.getId().intValue()))
            .andExpect(jsonPath("$.numeroTelephone").value(DEFAULT_NUMERO_TELEPHONE))
            .andExpect(jsonPath("$.pieceIdentiteContentType").value(DEFAULT_PIECE_IDENTITE_CONTENT_TYPE))
            .andExpect(jsonPath("$.pieceIdentite").value(Base64Utils.encodeToString(DEFAULT_PIECE_IDENTITE)))
            .andExpect(jsonPath("$.copieAgrementContentType").value(DEFAULT_COPIE_AGREMENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.copieAgrement").value(Base64Utils.encodeToString(DEFAULT_COPIE_AGREMENT)))
            .andExpect(jsonPath("$.dateNaissance").value(DEFAULT_DATE_NAISSANCE.toString()))
            .andExpect(jsonPath("$.lieuNaissance").value(DEFAULT_LIEU_NAISSANCE))
            .andExpect(jsonPath("$.genre").value(DEFAULT_GENRE));
    }

    @Test
    @Transactional
    void getNonExistingInfoplus() throws Exception {
        // Get the infoplus
        restInfoplusMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewInfoplus() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();

        // Update the infoplus
        Infoplus updatedInfoplus = infoplusRepository.findById(infoplus.getId()).get();
        // Disconnect from session so that the updates on updatedInfoplus are not directly saved in db
        em.detach(updatedInfoplus);
        updatedInfoplus
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .pieceIdentite(UPDATED_PIECE_IDENTITE)
            .pieceIdentiteContentType(UPDATED_PIECE_IDENTITE_CONTENT_TYPE)
            .copieAgrement(UPDATED_COPIE_AGREMENT)
            .copieAgrementContentType(UPDATED_COPIE_AGREMENT_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .lieuNaissance(UPDATED_LIEU_NAISSANCE)
            .genre(UPDATED_GENRE);

        restInfoplusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedInfoplus.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedInfoplus))
            )
            .andExpect(status().isOk());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
        Infoplus testInfoplus = infoplusList.get(infoplusList.size() - 1);
        assertThat(testInfoplus.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testInfoplus.getPieceIdentite()).isEqualTo(UPDATED_PIECE_IDENTITE);
        assertThat(testInfoplus.getPieceIdentiteContentType()).isEqualTo(UPDATED_PIECE_IDENTITE_CONTENT_TYPE);
        assertThat(testInfoplus.getCopieAgrement()).isEqualTo(UPDATED_COPIE_AGREMENT);
        assertThat(testInfoplus.getCopieAgrementContentType()).isEqualTo(UPDATED_COPIE_AGREMENT_CONTENT_TYPE);
        assertThat(testInfoplus.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);
        assertThat(testInfoplus.getLieuNaissance()).isEqualTo(UPDATED_LIEU_NAISSANCE);
        assertThat(testInfoplus.getGenre()).isEqualTo(UPDATED_GENRE);
    }

    @Test
    @Transactional
    void putNonExistingInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, infoplus.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(infoplus))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(infoplus))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInfoplusWithPatch() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();

        // Update the infoplus using partial update
        Infoplus partialUpdatedInfoplus = new Infoplus();
        partialUpdatedInfoplus.setId(infoplus.getId());

        restInfoplusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInfoplus.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInfoplus))
            )
            .andExpect(status().isOk());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
        Infoplus testInfoplus = infoplusList.get(infoplusList.size() - 1);
        assertThat(testInfoplus.getNumeroTelephone()).isEqualTo(DEFAULT_NUMERO_TELEPHONE);
        assertThat(testInfoplus.getPieceIdentite()).isEqualTo(DEFAULT_PIECE_IDENTITE);
        assertThat(testInfoplus.getPieceIdentiteContentType()).isEqualTo(DEFAULT_PIECE_IDENTITE_CONTENT_TYPE);
        assertThat(testInfoplus.getCopieAgrement()).isEqualTo(DEFAULT_COPIE_AGREMENT);
        assertThat(testInfoplus.getCopieAgrementContentType()).isEqualTo(DEFAULT_COPIE_AGREMENT_CONTENT_TYPE);
        assertThat(testInfoplus.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);
        assertThat(testInfoplus.getLieuNaissance()).isEqualTo(DEFAULT_LIEU_NAISSANCE);
        assertThat(testInfoplus.getGenre()).isEqualTo(DEFAULT_GENRE);
    }

    @Test
    @Transactional
    void fullUpdateInfoplusWithPatch() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();

        // Update the infoplus using partial update
        Infoplus partialUpdatedInfoplus = new Infoplus();
        partialUpdatedInfoplus.setId(infoplus.getId());

        partialUpdatedInfoplus
            .numeroTelephone(UPDATED_NUMERO_TELEPHONE)
            .pieceIdentite(UPDATED_PIECE_IDENTITE)
            .pieceIdentiteContentType(UPDATED_PIECE_IDENTITE_CONTENT_TYPE)
            .copieAgrement(UPDATED_COPIE_AGREMENT)
            .copieAgrementContentType(UPDATED_COPIE_AGREMENT_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .lieuNaissance(UPDATED_LIEU_NAISSANCE)
            .genre(UPDATED_GENRE);

        restInfoplusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInfoplus.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInfoplus))
            )
            .andExpect(status().isOk());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
        Infoplus testInfoplus = infoplusList.get(infoplusList.size() - 1);
        assertThat(testInfoplus.getNumeroTelephone()).isEqualTo(UPDATED_NUMERO_TELEPHONE);
        assertThat(testInfoplus.getPieceIdentite()).isEqualTo(UPDATED_PIECE_IDENTITE);
        assertThat(testInfoplus.getPieceIdentiteContentType()).isEqualTo(UPDATED_PIECE_IDENTITE_CONTENT_TYPE);
        assertThat(testInfoplus.getCopieAgrement()).isEqualTo(UPDATED_COPIE_AGREMENT);
        assertThat(testInfoplus.getCopieAgrementContentType()).isEqualTo(UPDATED_COPIE_AGREMENT_CONTENT_TYPE);
        assertThat(testInfoplus.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);
        assertThat(testInfoplus.getLieuNaissance()).isEqualTo(UPDATED_LIEU_NAISSANCE);
        assertThat(testInfoplus.getGenre()).isEqualTo(UPDATED_GENRE);
    }

    @Test
    @Transactional
    void patchNonExistingInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, infoplus.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(infoplus))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(infoplus))
            )
            .andExpect(status().isBadRequest());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInfoplus() throws Exception {
        int databaseSizeBeforeUpdate = infoplusRepository.findAll().size();
        infoplus.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInfoplusMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(infoplus)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Infoplus in the database
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInfoplus() throws Exception {
        // Initialize the database
        infoplusRepository.saveAndFlush(infoplus);

        int databaseSizeBeforeDelete = infoplusRepository.findAll().size();

        // Delete the infoplus
        restInfoplusMockMvc
            .perform(delete(ENTITY_API_URL_ID, infoplus.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Infoplus> infoplusList = infoplusRepository.findAll();
        assertThat(infoplusList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
