package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Prixproduit.
 */
@Entity
@Table(name = "prixproduit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Prixproduit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "prix", nullable = false)
    private Double prix;

    @NotNull
    @Column(name = "date_ajout", nullable = false)
    private Instant dateAjout;

    @NotNull
    @Column(name = "date_modification", nullable = false)
    private Instant dateModification;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Produit produit;

    @ManyToOne(optional = false)
    @NotNull
    private Compagne compagne;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Prixproduit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrix() {
        return this.prix;
    }

    public Prixproduit prix(Double prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Instant getDateAjout() {
        return this.dateAjout;
    }

    public Prixproduit dateAjout(Instant dateAjout) {
        this.setDateAjout(dateAjout);
        return this;
    }

    public void setDateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Instant getDateModification() {
        return this.dateModification;
    }

    public Prixproduit dateModification(Instant dateModification) {
        this.setDateModification(dateModification);
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Produit getProduit() {
        return this.produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Prixproduit produit(Produit produit) {
        this.setProduit(produit);
        return this;
    }

    public Compagne getCompagne() {
        return this.compagne;
    }

    public void setCompagne(Compagne compagne) {
        this.compagne = compagne;
    }

    public Prixproduit compagne(Compagne compagne) {
        this.setCompagne(compagne);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prixproduit)) {
            return false;
        }
        return id != null && id.equals(((Prixproduit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Prixproduit{" +
            "id=" + getId() +
            ", prix=" + getPrix() +
            ", dateAjout='" + getDateAjout() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            "}";
    }
}
