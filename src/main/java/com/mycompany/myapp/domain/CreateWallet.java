package com.mycompany.myapp.domain;

public class CreateWallet {
    private String admin_pass;
    private String name;
    private String email;
    private String password;
    public String getAdmin_pass() {
        return admin_pass;
    }
    public void setAdmin_pass(String admin_pass) {
        this.admin_pass = admin_pass;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
