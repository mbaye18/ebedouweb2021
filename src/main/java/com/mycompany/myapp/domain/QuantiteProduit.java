package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 * A QuantiteProduit.
 */
@Entity
@Table(name = "quantite_produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class QuantiteProduit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "quantite", nullable = false)
    private Float quantite;

    @NotNull
    @Column(name = "date_ajout", nullable = false)
    private Instant dateAjout;

    @Column(name = "date_modification")
    private Instant dateModification;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description")
    private String description;

    @Column(name = "quantite_restant")
    private Float quantiteRestant;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "produit", "compagne" }, allowSetters = true)
    private Prixproduit prixproduit;

    @ManyToOne(optional = false)
    @NotNull
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public QuantiteProduit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getQuantite() {
        return this.quantite;
    }

    public QuantiteProduit quantite(Float quantite) {
        this.setQuantite(quantite);
        return this;
    }

    public void setQuantite(Float quantite) {
        this.quantite = quantite;
    }

    public Instant getDateAjout() {
        return this.dateAjout;
    }

    public QuantiteProduit dateAjout(Instant dateAjout) {
        this.setDateAjout(dateAjout);
        return this;
    }

    public void setDateAjout(Instant dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Instant getDateModification() {
        return this.dateModification;
    }

    public QuantiteProduit dateModification(Instant dateModification) {
        this.setDateModification(dateModification);
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public String getDescription() {
        return this.description;
    }

    public QuantiteProduit description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getQuantiteRestant() {
        return this.quantiteRestant;
    }

    public QuantiteProduit quantiteRestant(Float quantiteRestant) {
        this.setQuantiteRestant(quantiteRestant);
        return this;
    }

    public void setQuantiteRestant(Float quantiteRestant) {
        this.quantiteRestant = quantiteRestant;
    }

    public Prixproduit getPrixproduit() {
        return this.prixproduit;
    }

    public void setPrixproduit(Prixproduit prixproduit) {
        this.prixproduit = prixproduit;
    }

    public QuantiteProduit prixproduit(Prixproduit prixproduit) {
        this.setPrixproduit(prixproduit);
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public QuantiteProduit user(User user) {
        this.setUser(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuantiteProduit)) {
            return false;
        }
        return id != null && id.equals(((QuantiteProduit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuantiteProduit{" +
            "id=" + getId() +
            ", quantite=" + getQuantite() +
            ", dateAjout='" + getDateAjout() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", description='" + getDescription() + "'" +
            ", quantiteRestant=" + getQuantiteRestant() +
            "}";
    }
}
