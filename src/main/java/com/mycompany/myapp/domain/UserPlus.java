package com.mycompany.myapp.domain;

import java.time.LocalDate;
import java.util.Set;

public class UserPlus {
   
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String numeroTelephone;
    private LocalDate dateNaissance;
    private String lieuNaissance;
    private String genre;
    private Nationnalite nationnalite;
    private ZoneExploitation zoneExploitation;
    private byte[] pieceIdentite;
    private String pieceIdentiteContentType;
    private Set<String> authorities;
    public String getLogin() {
        return login;
    }
    public byte[] getPieceIdentite() {
        return this.pieceIdentite;
    }
    public String getPieceIdentiteContentType() {
        return this.pieceIdentiteContentType;
    }


    public void setPieceIdentiteContentType(String pieceIdentiteContentType) {
        this.pieceIdentiteContentType = pieceIdentiteContentType;
    }
    public void setPieceIdentite(byte[] pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }
    // Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login =login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
       this.email = email;
    }
    
    public String getNumeroTelephone() {
        return this.numeroTelephone;
    }
    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public LocalDate getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return this.lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Nationnalite getNationnalite() {
        return this.nationnalite;
    }

    public void setNationnalite(Nationnalite nationnalite) {
        this.nationnalite = nationnalite;
    }

    public ZoneExploitation getZoneExploitation() {
        return this.zoneExploitation;
    }
    public void setZoneExploitation(ZoneExploitation zoneExploitation) {
        this.zoneExploitation = zoneExploitation;
    }
    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }
    
    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }
}
