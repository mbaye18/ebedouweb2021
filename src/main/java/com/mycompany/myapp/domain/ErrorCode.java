package com.mycompany.myapp.domain;

import java.util.HashMap;

public class ErrorCode {
	
	public static HashMap<String, String> smsError = new HashMap<String, String>();
	public static String getErrStr(String key){
		
	smsError.put("000",	"OK");	
	smsError.put("100",	"Requête POST absente");
	smsError.put("101",	"Mauvais identifiants");
	smsError.put("102",	"Votre SMS dépasse les 160 caractères");
	smsError.put("103",	"Il n'y a aucun destinataire à votre message");	
	smsError.put("104",	"Vous n'avez pas assez de crédit (vérifiez le nombre de vos contacts, ainsi que le nombre de SMS nécessaires à l'envoi de votre texte)");
	smsError.put("105",	"Vous n'avez pas assez de crédit, mais votre dernière commande est en attente de validation");
	smsError.put("106",	"Vous avez mal renseigné le Sender. 3 à 11 caractères, choisis parmi 0 à 9, a à z, A à Z. Ni accent, ni espace, ni ponctuation");
	smsError.put("107",	"Le texte de votre message est vide");
	smsError.put("108",	"Vous n'avez pas renseigné votre login");
	smsError.put("109",	"Vous n'avez pas renseigné votre mot de passe");
	smsError.put("110",	"Vous n'avez pas renseigné la liste de destinataires");
	smsError.put("111",	"Vous n'avez pas choisi de moyen de saisie de vos destinataires");
	smsError.put("112",	"Vous n'avez pas défini la qualité de votre message");
	smsError.put("113",	"Votre compte n'est pas validé. Connectez-vous sur Octopush et rendez-vous à la section \"Interface Utilisateur\"");
	smsError.put("114",	"Vous faites l'objet d'une enquête pour utilisation frauduleuse de nos services");
	smsError.put("115",	"Le nombre de destinataire est différent du nombre de l'un des paramètres que vous leur avez associés");
	smsError.put("116",	"L'option de publipostage ne fonctionne qu'en utilisant une liste de contact");
	smsError.put("117",	"Votre liste de destinataires ne contient aucun bon numéro. Avez-vous formaté vos numéros en les préfixant du format international ? Contactez-nous en cas de difficulté");
	smsError.put("118",	"Vous devez cocher l'une des deux cases pour indiquer si vous ne souhaitez pas envoyer de SMS de test ou si vous l'avez reçu et validé");
	smsError.put("119",	"Vous ne pouvez pas envoyer de SMS de plus de 160 caractères pour ce type de SMS");
	smsError.put("120",	"Un SMS avec le même request_id a déjà été envoyé");
	smsError.put("121",	"En sms Premium, la mention \"STOP au XXXXX\" (à recopier TELLE QUELLE, sans les guillemets, et avec les 5 X) est obligatoire et doit figurer dans votre texte (respecter les majuscules)");
	smsError.put("122",	"En SMS Standard, la mention \"no PUB rep STOP\" (à recopier telle quelle) est obligatoire et doit figurer dans votre texte (respecter les majuscules)");
	smsError.put("150",	"Aucun pays n'a été trouvé pour ce pays");
	smsError.put("151",	"Le pays destinataire ne fait pas parti des pays desservis par Octopush");
	smsError.put("152",	"Vous ne pouvez pas envoyer de SMS Standard vers ce pays. Choisissez le SMS Premium");
	return smsError.get(key);
	}
}
