package com.mycompany.myapp.domain;

import java.util.ArrayList;
import java.util.Arrays;
 
public class ConfigFile {
	
private static String DOMAIN = "http://www.octopush-dm.com";
private static String PORT = "80";
private static String PATH_SMS = "/api/sms";
private static String PATH_BALANCE = "/api/balance";
private static final String QUALITE_STANDARD = "XXX";
private static final String QUALITE_PRO = "FR";
private static final Integer INSTANTANE = 1;
private static final Integer DIFFERE = 2;
private static final String SIMULATION = "simu";
private static final String REEL = "real";

private static String _user_login = "jfbenie@gmail.com";
private static String _api_key = "gG0dZwGwuOjAz10JDdDguQoEBz9Ju2d7";
private static String _sms_text = "Bienvenue sur E-bedou  votre compte ";

private static ArrayList<String> _sms_recipients = new ArrayList<>();
private static ArrayList<String> _recipients_first_names = new ArrayList<>(Arrays.asList("fn1", "fn2", "fn3"));
private static ArrayList<String> _recipients_last_names = new ArrayList<>(Arrays.asList("ln1", "ln2", "ln3"));
private static ArrayList<String> _sms_fields_1 = new ArrayList<>(Arrays.asList("1_field1", "1_field2", "1_field3"));
private static ArrayList<String> _sms_fields_2 = new ArrayList<>(Arrays.asList("2_field1", "2_field2", "2_field3"));
private static ArrayList<String> _sms_fields_3 = new ArrayList<>(Arrays.asList("3_field1", "3_field2", "3_field3"));

private static Integer _sms_mode = INSTANTANE;	// INSTANTANE or change to DIFFERE for delay sms-mode sending

private static String _sms_type = QUALITE_PRO;	// QUALITE_STANDARD or change to QUALITE_PRO

//Change to correct value of DAY_OF_MONTH, MONTH, HOUR_OF_DAY etc'
private static int _sms_d = 16;	//	DAY_OF_MONTH
private static int _sms_m = 12;	//	MONTH
private static int _sms_h = 19;	//	HOUR_OF_DAY;
private static int _sms_i = 54;	//	MINUTE;
private static int _sms_y = 2013;	//	YEAR;

private static String _sms_sender = "E-bedou";
private static String _request_mode = REEL;
private static String _sms_ticket = "********";






















































































































public static String getDOMAIN() {
	return DOMAIN;
}
public static void setDOMAIN(String dOMAIN) {
	DOMAIN = dOMAIN;
}
public static String getPORT() {
	return PORT;
}
public static void setPORT(String pORT) {
	PORT = pORT;
}
public static String getPATH_SMS() {
	return PATH_SMS;
}
public static void setPATH_SMS(String pATH_SMS) {
	PATH_SMS = pATH_SMS;
}
public static String getPATH_BALANCE() {
	return PATH_BALANCE;
}
public static void setPATH_BALANCE(String pATH_BALANCE) {
	PATH_BALANCE = pATH_BALANCE;
}
public static String get_user_login() {
	return _user_login;
}
public static void set_user_login(String _user_login) {
	ConfigFile._user_login = _user_login;
}
public static String get_api_key() {
	return _api_key;
}
public static void set_api_key(String _api_key) {
	ConfigFile._api_key = _api_key;
}
public static String get_sms_text() {
	return _sms_text;
}
public static void set_sms_text(String _sms_text) {
	ConfigFile._sms_text = _sms_text;
}
public static ArrayList<String> get_sms_recipients() {
	return _sms_recipients;
}
public static void set_sms_recipients(ArrayList<String> _sms_recipients) {
	ConfigFile._sms_recipients = _sms_recipients;
}
public static ArrayList<String> get_recipients_first_names() {
	return _recipients_first_names;
}
public static void set_recipients_first_names(
		ArrayList<String> _recipients_first_names) {
	ConfigFile._recipients_first_names = _recipients_first_names;
}
public static ArrayList<String> get_recipients_last_names() {
	return _recipients_last_names;
}
public static void set_recipients_last_names(
		ArrayList<String> _recipients_last_names) {
	ConfigFile._recipients_last_names = _recipients_last_names;
}
public static ArrayList<String> get_sms_fields_1() {
	return _sms_fields_1;
}
public static void set_sms_fields_1(ArrayList<String> _sms_fields_1) {
	ConfigFile._sms_fields_1 = _sms_fields_1;
}
public static ArrayList<String> get_sms_fields_2() {
	return _sms_fields_2;
}
public static void set_sms_fields_2(ArrayList<String> _sms_fields_2) {
	ConfigFile._sms_fields_2 = _sms_fields_2;
}
public static ArrayList<String> get_sms_fields_3() {
	return _sms_fields_3;
}
public static void set_sms_fields_3(ArrayList<String> _sms_fields_3) {
	ConfigFile._sms_fields_3 = _sms_fields_3;
}
public static Integer get_sms_mode() {
	return _sms_mode;
}
public static void set_sms_mode(Integer _sms_mode) {
	ConfigFile._sms_mode = _sms_mode;
}
public static String get_sms_type() {
	return _sms_type;
}
public static void set_sms_type(String _sms_type) {
	ConfigFile._sms_type = _sms_type;
}
public static int get_sms_d() {
	return _sms_d;
}
public static void set_sms_d(int _sms_d) {
	ConfigFile._sms_d = _sms_d;
}
public static int get_sms_m() {
	return _sms_m;
}
public static void set_sms_m(int _sms_m) {
	ConfigFile._sms_m = _sms_m;
}
public static int get_sms_h() {
	return _sms_h;
}
public static void set_sms_h(int _sms_h) {
	ConfigFile._sms_h = _sms_h;
}
public static int get_sms_i() {
	return _sms_i;
}
public static void set_sms_i(int _sms_i) {
	ConfigFile._sms_i = _sms_i;
}
public static int get_sms_y() {
	return _sms_y;
}
public static void set_sms_y(int _sms_y) {
	ConfigFile._sms_y = _sms_y;
}
public static String get_sms_sender() {
	return _sms_sender;
}
public static void set_sms_sender(String _sms_sender) {
	ConfigFile._sms_sender = _sms_sender;
}
public static String get_request_mode() {
	return _request_mode;
}
public static void set_request_mode(String _request_mode) {
	ConfigFile._request_mode = _request_mode;
}
public static String get_sms_ticket() {
	return _sms_ticket;
}
public static void set_sms_ticket(String _sms_ticket) {
	ConfigFile._sms_ticket = _sms_ticket;
}
public static String getQualiteStandard() {
	return QUALITE_STANDARD;
}
public static String getQualitePro() {
	return QUALITE_PRO;
}
public static Integer getInstantane() {
	return INSTANTANE;
}
public static Integer getDiffere() {
	return DIFFERE;
}
public static String getSimulation() {
	return SIMULATION;
}
public static String getReel() {
	return REEL;
}



}


