package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Infoplus.
 */
@Entity
@Table(name = "infoplus")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Infoplus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "numero_telephone", nullable = false)
    private String numeroTelephone;

    @Lob
    @Column(name = "piece_identite")
    private byte[] pieceIdentite;

    @Column(name = "piece_identite_content_type")
    private String pieceIdentiteContentType;

    @Lob
    @Column(name = "copie_agrement")
    private byte[] copieAgrement;

    @Column(name = "copie_agrement_content_type")
    private String copieAgrementContentType;

    @NotNull
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @NotNull
    @Column(name = "lieu_naissance", nullable = false)
    private String lieuNaissance;

    @NotNull
    @Column(name = "genre", nullable = false)
    private String genre;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    @ManyToOne(optional = false)
    @NotNull
    private Nationnalite nationnalite;

    @ManyToOne
    @JsonIgnoreProperties(value = { "pays" }, allowSetters = true)
    private ZoneExploitation zoneExploitation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Infoplus id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroTelephone() {
        return this.numeroTelephone;
    }

    public Infoplus numeroTelephone(String numeroTelephone) {
        this.setNumeroTelephone(numeroTelephone);
        return this;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public byte[] getPieceIdentite() {
        return this.pieceIdentite;
    }

    public Infoplus pieceIdentite(byte[] pieceIdentite) {
        this.setPieceIdentite(pieceIdentite);
        return this;
    }

    public void setPieceIdentite(byte[] pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }

    public String getPieceIdentiteContentType() {
        return this.pieceIdentiteContentType;
    }

    public Infoplus pieceIdentiteContentType(String pieceIdentiteContentType) {
        this.pieceIdentiteContentType = pieceIdentiteContentType;
        return this;
    }

    public void setPieceIdentiteContentType(String pieceIdentiteContentType) {
        this.pieceIdentiteContentType = pieceIdentiteContentType;
    }

    public byte[] getCopieAgrement() {
        return this.copieAgrement;
    }

    public Infoplus copieAgrement(byte[] copieAgrement) {
        this.setCopieAgrement(copieAgrement);
        return this;
    }

    public void setCopieAgrement(byte[] copieAgrement) {
        this.copieAgrement = copieAgrement;
    }

    public String getCopieAgrementContentType() {
        return this.copieAgrementContentType;
    }

    public Infoplus copieAgrementContentType(String copieAgrementContentType) {
        this.copieAgrementContentType = copieAgrementContentType;
        return this;
    }

    public void setCopieAgrementContentType(String copieAgrementContentType) {
        this.copieAgrementContentType = copieAgrementContentType;
    }

    public LocalDate getDateNaissance() {
        return this.dateNaissance;
    }

    public Infoplus dateNaissance(LocalDate dateNaissance) {
        this.setDateNaissance(dateNaissance);
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return this.lieuNaissance;
    }

    public Infoplus lieuNaissance(String lieuNaissance) {
        this.setLieuNaissance(lieuNaissance);
        return this;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getGenre() {
        return this.genre;
    }

    public Infoplus genre(String genre) {
        this.setGenre(genre);
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Infoplus user(User user) {
        this.setUser(user);
        return this;
    }

    public Nationnalite getNationnalite() {
        return this.nationnalite;
    }

    public void setNationnalite(Nationnalite nationnalite) {
        this.nationnalite = nationnalite;
    }

    public Infoplus nationnalite(Nationnalite nationnalite) {
        this.setNationnalite(nationnalite);
        return this;
    }

    public ZoneExploitation getZoneExploitation() {
        return this.zoneExploitation;
    }

    public void setZoneExploitation(ZoneExploitation zoneExploitation) {
        this.zoneExploitation = zoneExploitation;
    }

    public Infoplus zoneExploitation(ZoneExploitation zoneExploitation) {
        this.setZoneExploitation(zoneExploitation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Infoplus)) {
            return false;
        }
        return id != null && id.equals(((Infoplus) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Infoplus{" +
            "id=" + getId() +
            ", numeroTelephone='" + getNumeroTelephone() + "'" +
            ", pieceIdentite='" + getPieceIdentite() + "'" +
            ", pieceIdentiteContentType='" + getPieceIdentiteContentType() + "'" +
            ", copieAgrement='" + getCopieAgrement() + "'" +
            ", copieAgrementContentType='" + getCopieAgrementContentType() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", genre='" + getGenre() + "'" +
            "}";
    }
}
