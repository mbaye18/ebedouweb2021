package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.CreateWallet;
import com.mycompany.myapp.domain.Infoplus;
import com.mycompany.myapp.repository.InfoplusRepository;
import com.mycompany.myapp.service.InfoplusService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Infoplus}.
 */
@RestController
@RequestMapping("/api")
public class InfoplusResource {

    private final Logger log = LoggerFactory.getLogger(InfoplusResource.class);

    private static final String ENTITY_NAME = "infoplus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InfoplusService infoplusService;

    private final InfoplusRepository infoplusRepository;

    public InfoplusResource(InfoplusService infoplusService, InfoplusRepository infoplusRepository) {
        this.infoplusService = infoplusService;
        this.infoplusRepository = infoplusRepository;
    }

    /**
     * {@code POST  /infopluses} : Create a new infoplus.
     *
     * @param infoplus the infoplus to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new infoplus, or with status {@code 400 (Bad Request)} if the infoplus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/infopluses")
    public ResponseEntity<Infoplus> createInfoplus(@Valid @RequestBody Infoplus infoplus) throws URISyntaxException {
        log.debug("REST request to save Infoplus : {}", infoplus);
        if (infoplus.getId() != null) {
            throw new BadRequestAlertException("A new infoplus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Infoplus result = infoplusService.save(infoplus);
        return ResponseEntity
            .created(new URI("/api/infopluses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /infopluses/:id} : Updates an existing infoplus.
     *
     * @param id the id of the infoplus to save.
     * @param infoplus the infoplus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated infoplus,
     * or with status {@code 400 (Bad Request)} if the infoplus is not valid,
     * or with status {@code 500 (Internal Server Error)} if the infoplus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/infopluses/{id}")
    public ResponseEntity<Infoplus> updateInfoplus(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Infoplus infoplus
    ) throws URISyntaxException {
        log.debug("REST request to update Infoplus : {}, {}", id, infoplus);
        if (infoplus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, infoplus.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!infoplusRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Infoplus result = infoplusService.save(infoplus);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, infoplus.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /infopluses/:id} : Partial updates given fields of an existing infoplus, field will ignore if it is null
     *
     * @param id the id of the infoplus to save.
     * @param infoplus the infoplus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated infoplus,
     * or with status {@code 400 (Bad Request)} if the infoplus is not valid,
     * or with status {@code 404 (Not Found)} if the infoplus is not found,
     * or with status {@code 500 (Internal Server Error)} if the infoplus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/infopluses/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Infoplus> partialUpdateInfoplus(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Infoplus infoplus
    ) throws URISyntaxException {
        log.debug("REST request to partial update Infoplus partially : {}, {}", id, infoplus);
        if (infoplus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, infoplus.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!infoplusRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Infoplus> result = infoplusService.partialUpdate(infoplus);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, infoplus.getId().toString())
        );
    }

    /**
     * {@code GET  /infopluses} : get all the infopluses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of infopluses in body.
     */
    @GetMapping("/infopluses")
    public ResponseEntity<List<Infoplus>> getAllInfopluses(Pageable pageable) {
        log.debug("REST request to get a page of Infopluses");
        Page<Infoplus> page = infoplusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /infopluses/:id} : get the "id" infoplus.
     *
     * @param id the id of the infoplus to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the infoplus, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/infopluses/{id}")
    public ResponseEntity<Infoplus> getInfoplus(@PathVariable String id) {
        log.debug("REST request to get Infoplus : {}", id);
        Optional<Infoplus> infoplus = infoplusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(infoplus);
    }

    /**
     * {@code DELETE  /infopluses/:id} : delete the "id" infoplus.
     *
     * @param id the id of the infoplus to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/infopluses/{id}")
    public ResponseEntity<Void> deleteInfoplus(@PathVariable Long id) {
        log.debug("REST request to delete Infoplus : {}", id);
        infoplusService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
