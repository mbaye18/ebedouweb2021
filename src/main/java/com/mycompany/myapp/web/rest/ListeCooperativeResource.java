package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ListeCooperative;
import com.mycompany.myapp.repository.ListeCooperativeRepository;
import com.mycompany.myapp.service.ListeCooperativeQueryService;
import com.mycompany.myapp.service.ListeCooperativeService;
import com.mycompany.myapp.service.criteria.ListeCooperativeCriteria;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ListeCooperative}.
 */
@RestController
@RequestMapping("/api")
public class ListeCooperativeResource {

    private final Logger log = LoggerFactory.getLogger(ListeCooperativeResource.class);

    private static final String ENTITY_NAME = "listeCooperative";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListeCooperativeService listeCooperativeService;

    private final ListeCooperativeRepository listeCooperativeRepository;

    private final ListeCooperativeQueryService listeCooperativeQueryService;

    public ListeCooperativeResource(
        ListeCooperativeService listeCooperativeService,
        ListeCooperativeRepository listeCooperativeRepository,
        ListeCooperativeQueryService listeCooperativeQueryService
    ) {
        this.listeCooperativeService = listeCooperativeService;
        this.listeCooperativeRepository = listeCooperativeRepository;
        this.listeCooperativeQueryService = listeCooperativeQueryService;
    }

    /**
     * {@code POST  /liste-cooperatives} : Create a new listeCooperative.
     *
     * @param listeCooperative the listeCooperative to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listeCooperative, or with status {@code 400 (Bad Request)} if the listeCooperative has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/liste-cooperatives")
    public ResponseEntity<ListeCooperative> createListeCooperative(@Valid @RequestBody ListeCooperative listeCooperative)
        throws URISyntaxException {
        log.debug("REST request to save ListeCooperative : {}", listeCooperative);
        if (listeCooperative.getId() != null) {
            throw new BadRequestAlertException("A new listeCooperative cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListeCooperative result = listeCooperativeService.save(listeCooperative);
        return ResponseEntity
            .created(new URI("/api/liste-cooperatives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /liste-cooperatives/:id} : Updates an existing listeCooperative.
     *
     * @param id the id of the listeCooperative to save.
     * @param listeCooperative the listeCooperative to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listeCooperative,
     * or with status {@code 400 (Bad Request)} if the listeCooperative is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listeCooperative couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/liste-cooperatives/{id}")
    public ResponseEntity<ListeCooperative> updateListeCooperative(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ListeCooperative listeCooperative
    ) throws URISyntaxException {
        log.debug("REST request to update ListeCooperative : {}, {}", id, listeCooperative);
        if (listeCooperative.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listeCooperative.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listeCooperativeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ListeCooperative result = listeCooperativeService.save(listeCooperative);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listeCooperative.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /liste-cooperatives/:id} : Partial updates given fields of an existing listeCooperative, field will ignore if it is null
     *
     * @param id the id of the listeCooperative to save.
     * @param listeCooperative the listeCooperative to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listeCooperative,
     * or with status {@code 400 (Bad Request)} if the listeCooperative is not valid,
     * or with status {@code 404 (Not Found)} if the listeCooperative is not found,
     * or with status {@code 500 (Internal Server Error)} if the listeCooperative couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/liste-cooperatives/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ListeCooperative> partialUpdateListeCooperative(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ListeCooperative listeCooperative
    ) throws URISyntaxException {
        log.debug("REST request to partial update ListeCooperative partially : {}, {}", id, listeCooperative);
        if (listeCooperative.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listeCooperative.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listeCooperativeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ListeCooperative> result = listeCooperativeService.partialUpdate(listeCooperative);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listeCooperative.getId().toString())
        );
    }

    /**
     * {@code GET  /liste-cooperatives} : get all the listeCooperatives.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listeCooperatives in body.
     */
    @GetMapping("/liste-cooperatives")
    public ResponseEntity<List<ListeCooperative>> getAllListeCooperatives(ListeCooperativeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ListeCooperatives by criteria: {}", criteria);
        Page<ListeCooperative> page = listeCooperativeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /liste-cooperatives/count} : count all the listeCooperatives.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/liste-cooperatives/count")
    public ResponseEntity<Long> countListeCooperatives(ListeCooperativeCriteria criteria) {
        log.debug("REST request to count ListeCooperatives by criteria: {}", criteria);
        return ResponseEntity.ok().body(listeCooperativeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /liste-cooperatives/:id} : get the "id" listeCooperative.
     *
     * @param id the id of the listeCooperative to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listeCooperative, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/liste-cooperatives/{id}")
    public ResponseEntity<ListeCooperative> getListeCooperative(@PathVariable Long id) {
        log.debug("REST request to get ListeCooperative : {}", id);
        Optional<ListeCooperative> listeCooperative = listeCooperativeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(listeCooperative);
    }

    /**
     * {@code DELETE  /liste-cooperatives/:id} : delete the "id" listeCooperative.
     *
     * @param id the id of the listeCooperative to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/liste-cooperatives/{id}")
    public ResponseEntity<Void> deleteListeCooperative(@PathVariable Long id) {
        log.debug("REST request to delete ListeCooperative : {}", id);
        listeCooperativeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
