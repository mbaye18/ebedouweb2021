package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Nationnalite;
import com.mycompany.myapp.repository.NationnaliteRepository;
import com.mycompany.myapp.service.NationnaliteService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Nationnalite}.
 */
@RestController
@RequestMapping("/api")
public class NationnaliteResource {

    private final Logger log = LoggerFactory.getLogger(NationnaliteResource.class);

    private static final String ENTITY_NAME = "nationnalite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NationnaliteService nationnaliteService;

    private final NationnaliteRepository nationnaliteRepository;

    public NationnaliteResource(NationnaliteService nationnaliteService, NationnaliteRepository nationnaliteRepository) {
        this.nationnaliteService = nationnaliteService;
        this.nationnaliteRepository = nationnaliteRepository;
    }

    /**
     * {@code POST  /nationnalites} : Create a new nationnalite.
     *
     * @param nationnalite the nationnalite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nationnalite, or with status {@code 400 (Bad Request)} if the nationnalite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nationnalites")
    public ResponseEntity<Nationnalite> createNationnalite(@Valid @RequestBody Nationnalite nationnalite) throws URISyntaxException {
        log.debug("REST request to save Nationnalite : {}", nationnalite);
        if (nationnalite.getId() != null) {
            throw new BadRequestAlertException("A new nationnalite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Nationnalite result = nationnaliteService.save(nationnalite);
        return ResponseEntity
            .created(new URI("/api/nationnalites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nationnalites/:id} : Updates an existing nationnalite.
     *
     * @param id the id of the nationnalite to save.
     * @param nationnalite the nationnalite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nationnalite,
     * or with status {@code 400 (Bad Request)} if the nationnalite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nationnalite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nationnalites/{id}")
    public ResponseEntity<Nationnalite> updateNationnalite(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Nationnalite nationnalite
    ) throws URISyntaxException {
        log.debug("REST request to update Nationnalite : {}, {}", id, nationnalite);
        if (nationnalite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nationnalite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nationnaliteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Nationnalite result = nationnaliteService.save(nationnalite);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nationnalite.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /nationnalites/:id} : Partial updates given fields of an existing nationnalite, field will ignore if it is null
     *
     * @param id the id of the nationnalite to save.
     * @param nationnalite the nationnalite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nationnalite,
     * or with status {@code 400 (Bad Request)} if the nationnalite is not valid,
     * or with status {@code 404 (Not Found)} if the nationnalite is not found,
     * or with status {@code 500 (Internal Server Error)} if the nationnalite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nationnalites/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Nationnalite> partialUpdateNationnalite(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Nationnalite nationnalite
    ) throws URISyntaxException {
        log.debug("REST request to partial update Nationnalite partially : {}, {}", id, nationnalite);
        if (nationnalite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, nationnalite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!nationnaliteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Nationnalite> result = nationnaliteService.partialUpdate(nationnalite);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nationnalite.getId().toString())
        );
    }

    /**
     * {@code GET  /nationnalites} : get all the nationnalites.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nationnalites in body.
     */
    @GetMapping("/nationnalites")
    public ResponseEntity<List<Nationnalite>> getAllNationnalites(Pageable pageable) {
        log.debug("REST request to get a page of Nationnalites");
        Page<Nationnalite> page = nationnaliteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nationnalites/:id} : get the "id" nationnalite.
     *
     * @param id the id of the nationnalite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nationnalite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nationnalites/{id}")
    public ResponseEntity<Nationnalite> getNationnalite(@PathVariable Long id) {
        log.debug("REST request to get Nationnalite : {}", id);
        Optional<Nationnalite> nationnalite = nationnaliteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nationnalite);
    }

    /**
     * {@code DELETE  /nationnalites/:id} : delete the "id" nationnalite.
     *
     * @param id the id of the nationnalite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nationnalites/{id}")
    public ResponseEntity<Void> deleteNationnalite(@PathVariable Long id) {
        log.debug("REST request to delete Nationnalite : {}", id);
        nationnaliteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
