package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Prixproduit;
import com.mycompany.myapp.repository.PrixproduitRepository;
import com.mycompany.myapp.service.PrixproduitQueryService;
import com.mycompany.myapp.service.PrixproduitService;
import com.mycompany.myapp.service.criteria.PrixproduitCriteria;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Prixproduit}.
 */
@RestController
@RequestMapping("/api")
public class PrixproduitResource {

    private final Logger log = LoggerFactory.getLogger(PrixproduitResource.class);

    private static final String ENTITY_NAME = "prixproduit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PrixproduitService prixproduitService;

    private final PrixproduitRepository prixproduitRepository;

    private final PrixproduitQueryService prixproduitQueryService;

    public PrixproduitResource(
        PrixproduitService prixproduitService,
        PrixproduitRepository prixproduitRepository,
        PrixproduitQueryService prixproduitQueryService
    ) {
        this.prixproduitService = prixproduitService;
        this.prixproduitRepository = prixproduitRepository;
        this.prixproduitQueryService = prixproduitQueryService;
    }

    /**
     * {@code POST  /prixproduits} : Create a new prixproduit.
     *
     * @param prixproduit the prixproduit to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new prixproduit, or with status {@code 400 (Bad Request)} if the prixproduit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/prixproduits")
    public ResponseEntity<Prixproduit> createPrixproduit(@Valid @RequestBody Prixproduit prixproduit) throws URISyntaxException {
        log.debug("REST request to save Prixproduit : {}", prixproduit);
        if (prixproduit.getId() != null) {
            throw new BadRequestAlertException("A new prixproduit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Prixproduit result = prixproduitService.save(prixproduit);
        return ResponseEntity
            .created(new URI("/api/prixproduits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /prixproduits/:id} : Updates an existing prixproduit.
     *
     * @param id the id of the prixproduit to save.
     * @param prixproduit the prixproduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prixproduit,
     * or with status {@code 400 (Bad Request)} if the prixproduit is not valid,
     * or with status {@code 500 (Internal Server Error)} if the prixproduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/prixproduits/{id}")
    public ResponseEntity<Prixproduit> updatePrixproduit(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Prixproduit prixproduit
    ) throws URISyntaxException {
        log.debug("REST request to update Prixproduit : {}, {}", id, prixproduit);
        if (prixproduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prixproduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prixproduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Prixproduit result = prixproduitService.save(prixproduit);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prixproduit.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /prixproduits/:id} : Partial updates given fields of an existing prixproduit, field will ignore if it is null
     *
     * @param id the id of the prixproduit to save.
     * @param prixproduit the prixproduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prixproduit,
     * or with status {@code 400 (Bad Request)} if the prixproduit is not valid,
     * or with status {@code 404 (Not Found)} if the prixproduit is not found,
     * or with status {@code 500 (Internal Server Error)} if the prixproduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/prixproduits/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Prixproduit> partialUpdatePrixproduit(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Prixproduit prixproduit
    ) throws URISyntaxException {
        log.debug("REST request to partial update Prixproduit partially : {}, {}", id, prixproduit);
        if (prixproduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prixproduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prixproduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Prixproduit> result = prixproduitService.partialUpdate(prixproduit);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prixproduit.getId().toString())
        );
    }

    /**
     * {@code GET  /prixproduits} : get all the prixproduits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of prixproduits in body.
     */
    @GetMapping("/prixproduits")
    public ResponseEntity<List<Prixproduit>> getAllPrixproduits(PrixproduitCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Prixproduits by criteria: {}", criteria);
        Page<Prixproduit> page = prixproduitQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /prixproduits/count} : count all the prixproduits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/prixproduits/count")
    public ResponseEntity<Long> countPrixproduits(PrixproduitCriteria criteria) {
        log.debug("REST request to count Prixproduits by criteria: {}", criteria);
        return ResponseEntity.ok().body(prixproduitQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /prixproduits/:id} : get the "id" prixproduit.
     *
     * @param id the id of the prixproduit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the prixproduit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/prixproduits/{id}")
    public ResponseEntity<Prixproduit> getPrixproduit(@PathVariable Long id) {
        log.debug("REST request to get Prixproduit : {}", id);
        Optional<Prixproduit> prixproduit = prixproduitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(prixproduit);
    }

    /**
     * {@code DELETE  /prixproduits/:id} : delete the "id" prixproduit.
     *
     * @param id the id of the prixproduit to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/prixproduits/{id}")
    public ResponseEntity<Void> deletePrixproduit(@PathVariable Long id) {
        log.debug("REST request to delete Prixproduit : {}", id);
        prixproduitService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
