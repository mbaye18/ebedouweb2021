package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ImageProduit;
import com.mycompany.myapp.repository.ImageProduitRepository;
import com.mycompany.myapp.service.ImageProduitService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ImageProduit}.
 */
@RestController
@RequestMapping("/api")
public class ImageProduitResource {

    private final Logger log = LoggerFactory.getLogger(ImageProduitResource.class);

    private static final String ENTITY_NAME = "imageProduit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImageProduitService imageProduitService;

    private final ImageProduitRepository imageProduitRepository;

    public ImageProduitResource(ImageProduitService imageProduitService, ImageProduitRepository imageProduitRepository) {
        this.imageProduitService = imageProduitService;
        this.imageProduitRepository = imageProduitRepository;
    }

    /**
     * {@code POST  /image-produits} : Create a new imageProduit.
     *
     * @param imageProduit the imageProduit to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new imageProduit, or with status {@code 400 (Bad Request)} if the imageProduit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/image-produits")
    public ResponseEntity<ImageProduit> createImageProduit(@Valid @RequestBody ImageProduit imageProduit) throws URISyntaxException {
        log.debug("REST request to save ImageProduit : {}", imageProduit);
        if (imageProduit.getId() != null) {
            throw new BadRequestAlertException("A new imageProduit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImageProduit result = imageProduitService.save(imageProduit);
        return ResponseEntity
            .created(new URI("/api/image-produits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /image-produits/:id} : Updates an existing imageProduit.
     *
     * @param id the id of the imageProduit to save.
     * @param imageProduit the imageProduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageProduit,
     * or with status {@code 400 (Bad Request)} if the imageProduit is not valid,
     * or with status {@code 500 (Internal Server Error)} if the imageProduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/image-produits/{id}")
    public ResponseEntity<ImageProduit> updateImageProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ImageProduit imageProduit
    ) throws URISyntaxException {
        log.debug("REST request to update ImageProduit : {}, {}", id, imageProduit);
        if (imageProduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, imageProduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!imageProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ImageProduit result = imageProduitService.save(imageProduit);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, imageProduit.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /image-produits/:id} : Partial updates given fields of an existing imageProduit, field will ignore if it is null
     *
     * @param id the id of the imageProduit to save.
     * @param imageProduit the imageProduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageProduit,
     * or with status {@code 400 (Bad Request)} if the imageProduit is not valid,
     * or with status {@code 404 (Not Found)} if the imageProduit is not found,
     * or with status {@code 500 (Internal Server Error)} if the imageProduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/image-produits/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ImageProduit> partialUpdateImageProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ImageProduit imageProduit
    ) throws URISyntaxException {
        log.debug("REST request to partial update ImageProduit partially : {}, {}", id, imageProduit);
        if (imageProduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, imageProduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!imageProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ImageProduit> result = imageProduitService.partialUpdate(imageProduit);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, imageProduit.getId().toString())
        );
    }

    /**
     * {@code GET  /image-produits} : get all the imageProduits.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of imageProduits in body.
     */
    @GetMapping("/image-produits")
    public ResponseEntity<List<ImageProduit>> getAllImageProduits(Pageable pageable) {
        log.debug("REST request to get a page of ImageProduits");
        Page<ImageProduit> page = imageProduitService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /image-produits/:id} : get the "id" imageProduit.
     *
     * @param id the id of the imageProduit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the imageProduit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/image-produits/{id}")
    public ResponseEntity<ImageProduit> getImageProduit(@PathVariable Long id) {
        log.debug("REST request to get ImageProduit : {}", id);
        Optional<ImageProduit> imageProduit = imageProduitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(imageProduit);
    }

    /**
     * {@code DELETE  /image-produits/:id} : delete the "id" imageProduit.
     *
     * @param id the id of the imageProduit to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/image-produits/{id}")
    public ResponseEntity<Void> deleteImageProduit(@PathVariable Long id) {
        log.debug("REST request to delete ImageProduit : {}", id);
        imageProduitService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
