package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Compagne;
import com.mycompany.myapp.repository.CompagneRepository;
import com.mycompany.myapp.service.CompagneQueryService;
import com.mycompany.myapp.service.CompagneService;
import com.mycompany.myapp.service.criteria.CompagneCriteria;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Compagne}.
 */
@RestController
@RequestMapping("/api")
public class CompagneResource {

    private final Logger log = LoggerFactory.getLogger(CompagneResource.class);

    private static final String ENTITY_NAME = "compagne";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompagneService compagneService;

    private final CompagneRepository compagneRepository;

    private final CompagneQueryService compagneQueryService;

    public CompagneResource(
        CompagneService compagneService,
        CompagneRepository compagneRepository,
        CompagneQueryService compagneQueryService
    ) {
        this.compagneService = compagneService;
        this.compagneRepository = compagneRepository;
        this.compagneQueryService = compagneQueryService;
    }

    /**
     * {@code POST  /compagnes} : Create a new compagne.
     *
     * @param compagne the compagne to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new compagne, or with status {@code 400 (Bad Request)} if the compagne has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/compagnes")
    public ResponseEntity<Compagne> createCompagne(@Valid @RequestBody Compagne compagne) throws URISyntaxException {
        log.debug("REST request to save Compagne : {}", compagne);
        if (compagne.getId() != null) {
            throw new BadRequestAlertException("A new compagne cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Compagne result = compagneService.save(compagne);
        return ResponseEntity
            .created(new URI("/api/compagnes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /compagnes/:id} : Updates an existing compagne.
     *
     * @param id the id of the compagne to save.
     * @param compagne the compagne to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compagne,
     * or with status {@code 400 (Bad Request)} if the compagne is not valid,
     * or with status {@code 500 (Internal Server Error)} if the compagne couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/compagnes/{id}")
    public ResponseEntity<Compagne> updateCompagne(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Compagne compagne
    ) throws URISyntaxException {
        log.debug("REST request to update Compagne : {}, {}", id, compagne);
        if (compagne.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, compagne.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!compagneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Compagne result = compagneService.save(compagne);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, compagne.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /compagnes/:id} : Partial updates given fields of an existing compagne, field will ignore if it is null
     *
     * @param id the id of the compagne to save.
     * @param compagne the compagne to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compagne,
     * or with status {@code 400 (Bad Request)} if the compagne is not valid,
     * or with status {@code 404 (Not Found)} if the compagne is not found,
     * or with status {@code 500 (Internal Server Error)} if the compagne couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/compagnes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Compagne> partialUpdateCompagne(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Compagne compagne
    ) throws URISyntaxException {
        log.debug("REST request to partial update Compagne partially : {}, {}", id, compagne);
        if (compagne.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, compagne.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!compagneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Compagne> result = compagneService.partialUpdate(compagne);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, compagne.getId().toString())
        );
    }

    /**
     * {@code GET  /compagnes} : get all the compagnes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of compagnes in body.
     */
    @GetMapping("/compagnes")
    public ResponseEntity<List<Compagne>> getAllCompagnes(CompagneCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Compagnes by criteria: {}", criteria);
        Page<Compagne> page = compagneQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /compagnes/count} : count all the compagnes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/compagnes/count")
    public ResponseEntity<Long> countCompagnes(CompagneCriteria criteria) {
        log.debug("REST request to count Compagnes by criteria: {}", criteria);
        return ResponseEntity.ok().body(compagneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /compagnes/:id} : get the "id" compagne.
     *
     * @param id the id of the compagne to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the compagne, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/compagnes/{id}")
    public ResponseEntity<Compagne> getCompagne(@PathVariable Long id) {
        log.debug("REST request to get Compagne : {}", id);
        Optional<Compagne> compagne = compagneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(compagne);
    }

    /**
     * {@code DELETE  /compagnes/:id} : delete the "id" compagne.
     *
     * @param id the id of the compagne to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/compagnes/{id}")
    public ResponseEntity<Void> deleteCompagne(@PathVariable Long id) {
        log.debug("REST request to delete Compagne : {}", id);
        compagneService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
