package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.QuantiteProduit;
import com.mycompany.myapp.repository.QuantiteProduitRepository;
import com.mycompany.myapp.service.QuantiteProduitService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.QuantiteProduit}.
 */
@RestController
@RequestMapping("/api")
public class QuantiteProduitResource {

    private final Logger log = LoggerFactory.getLogger(QuantiteProduitResource.class);

    private static final String ENTITY_NAME = "quantiteProduit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuantiteProduitService quantiteProduitService;

    private final QuantiteProduitRepository quantiteProduitRepository;

    public QuantiteProduitResource(QuantiteProduitService quantiteProduitService, QuantiteProduitRepository quantiteProduitRepository) {
        this.quantiteProduitService = quantiteProduitService;
        this.quantiteProduitRepository = quantiteProduitRepository;
    }

    /**
     * {@code POST  /quantite-produits} : Create a new quantiteProduit.
     *
     * @param quantiteProduit the quantiteProduit to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quantiteProduit, or with status {@code 400 (Bad Request)} if the quantiteProduit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quantite-produits")
    public ResponseEntity<QuantiteProduit> createQuantiteProduit(@RequestBody QuantiteProduit quantiteProduit) throws URISyntaxException {
        log.debug("REST request to save QuantiteProduit : {}", quantiteProduit);
        if (quantiteProduit.getId() != null) {
            throw new BadRequestAlertException("A new quantiteProduit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        quantiteProduit.setDateAjout(Instant.now());
        QuantiteProduit result = quantiteProduitService.save(quantiteProduit);
        return ResponseEntity
            .created(new URI("/api/quantite-produits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /quantite-produits/:id} : Updates an existing quantiteProduit.
     *
     * @param id the id of the quantiteProduit to save.
     * @param quantiteProduit the quantiteProduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quantiteProduit,
     * or with status {@code 400 (Bad Request)} if the quantiteProduit is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quantiteProduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quantite-produits/{id}")
    public ResponseEntity<QuantiteProduit> updateQuantiteProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody QuantiteProduit quantiteProduit
    ) throws URISyntaxException {
        log.debug("REST request to update QuantiteProduit : {}, {}", id, quantiteProduit);
        if (quantiteProduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quantiteProduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!quantiteProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        quantiteProduit.dateModification(Instant.now());
        QuantiteProduit result = quantiteProduitService.save(quantiteProduit);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quantiteProduit.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /quantite-produits/:id} : Partial updates given fields of an existing quantiteProduit, field will ignore if it is null
     *
     * @param id the id of the quantiteProduit to save.
     * @param quantiteProduit the quantiteProduit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quantiteProduit,
     * or with status {@code 400 (Bad Request)} if the quantiteProduit is not valid,
     * or with status {@code 404 (Not Found)} if the quantiteProduit is not found,
     * or with status {@code 500 (Internal Server Error)} if the quantiteProduit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/quantite-produits/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<QuantiteProduit> partialUpdateQuantiteProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody QuantiteProduit quantiteProduit
    ) throws URISyntaxException {
        log.debug("REST request to partial update QuantiteProduit partially : {}, {}", id, quantiteProduit);
        if (quantiteProduit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, quantiteProduit.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!quantiteProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<QuantiteProduit> result = quantiteProduitService.partialUpdate(quantiteProduit);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, quantiteProduit.getId().toString())
        );
    }

    /**
     * {@code GET  /quantite-produits} : get all the quantiteProduits.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quantiteProduits in body.
     */
    @GetMapping("/quantite-produits")
    public ResponseEntity<List<QuantiteProduit>> getAllQuantiteProduits(Pageable pageable) {
        log.debug("REST request to get a page of QuantiteProduits");
        Page<QuantiteProduit> page = quantiteProduitService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /quantite-produits/:id} : get the "id" quantiteProduit.
     *
     * @param id the id of the quantiteProduit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quantiteProduit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quantite-produits/{id}")
    public ResponseEntity<QuantiteProduit> getQuantiteProduit(@PathVariable Long id) {
        log.debug("REST request to get QuantiteProduit : {}", id);
        Optional<QuantiteProduit> quantiteProduit = quantiteProduitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quantiteProduit);
    }

    /**
     * {@code DELETE  /quantite-produits/:id} : delete the "id" quantiteProduit.
     *
     * @param id the id of the quantiteProduit to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quantite-produits/{id}")
    public ResponseEntity<Void> deleteQuantiteProduit(@PathVariable Long id) {
        log.debug("REST request to delete QuantiteProduit : {}", id);
        quantiteProduitService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
