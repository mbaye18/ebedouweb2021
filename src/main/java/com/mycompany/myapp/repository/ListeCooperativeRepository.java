package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ListeCooperative;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ListeCooperative entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListeCooperativeRepository extends JpaRepository<ListeCooperative, Long>, JpaSpecificationExecutor<ListeCooperative> {}
