package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ImageProduit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ImageProduit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageProduitRepository extends JpaRepository<ImageProduit, Long> {}
