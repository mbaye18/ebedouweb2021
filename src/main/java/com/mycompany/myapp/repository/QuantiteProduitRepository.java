package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.QuantiteProduit;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the QuantiteProduit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuantiteProduitRepository extends JpaRepository<QuantiteProduit, Long> {
    @Query("select quantiteProduit from QuantiteProduit quantiteProduit where quantiteProduit.user.login = ?#{principal.username}")
    List<QuantiteProduit> findByUserIsCurrentUser();
}
