package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Prixproduit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Prixproduit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrixproduitRepository extends JpaRepository<Prixproduit, Long>, JpaSpecificationExecutor<Prixproduit> {}
