package com.mycompany.myapp.repository;

import java.util.Optional;

import com.mycompany.myapp.domain.Infoplus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Infoplus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InfoplusRepository extends JpaRepository<Infoplus, Long> {

    Optional<Infoplus> findByUserLogin(String id);}
