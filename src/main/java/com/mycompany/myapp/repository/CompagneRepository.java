package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Compagne;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Compagne entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompagneRepository extends JpaRepository<Compagne, Long>, JpaSpecificationExecutor<Compagne> {}
