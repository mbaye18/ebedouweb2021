package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Demande;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Demande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandeRepository extends JpaRepository<Demande, Long> {
    @Query("select demande from Demande demande where demande.user.login = ?#{principal.username}")
    List<Demande> findByUserIsCurrentUser();
}
