package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Nationnalite;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Nationnalite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NationnaliteRepository extends JpaRepository<Nationnalite, Long> {}
