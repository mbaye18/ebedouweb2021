package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Nationnalite;
import com.mycompany.myapp.repository.NationnaliteRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Nationnalite}.
 */
@Service
@Transactional
public class NationnaliteService {

    private final Logger log = LoggerFactory.getLogger(NationnaliteService.class);

    private final NationnaliteRepository nationnaliteRepository;

    public NationnaliteService(NationnaliteRepository nationnaliteRepository) {
        this.nationnaliteRepository = nationnaliteRepository;
    }

    /**
     * Save a nationnalite.
     *
     * @param nationnalite the entity to save.
     * @return the persisted entity.
     */
    public Nationnalite save(Nationnalite nationnalite) {
        log.debug("Request to save Nationnalite : {}", nationnalite);
        return nationnaliteRepository.save(nationnalite);
    }

    /**
     * Partially update a nationnalite.
     *
     * @param nationnalite the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Nationnalite> partialUpdate(Nationnalite nationnalite) {
        log.debug("Request to partially update Nationnalite : {}", nationnalite);

        return nationnaliteRepository
            .findById(nationnalite.getId())
            .map(existingNationnalite -> {
                if (nationnalite.getLibelle() != null) {
                    existingNationnalite.setLibelle(nationnalite.getLibelle());
                }

                return existingNationnalite;
            })
            .map(nationnaliteRepository::save);
    }

    /**
     * Get all the nationnalites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Nationnalite> findAll(Pageable pageable) {
        log.debug("Request to get all Nationnalites");
        return nationnaliteRepository.findAll(pageable);
    }

    /**
     * Get one nationnalite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Nationnalite> findOne(Long id) {
        log.debug("Request to get Nationnalite : {}", id);
        return nationnaliteRepository.findById(id);
    }

    /**
     * Delete the nationnalite by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Nationnalite : {}", id);
        nationnaliteRepository.deleteById(id);
    }
}
