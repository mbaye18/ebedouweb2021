package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Compagne;
import com.mycompany.myapp.repository.CompagneRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Compagne}.
 */
@Service
@Transactional
public class CompagneService {

    private final Logger log = LoggerFactory.getLogger(CompagneService.class);

    private final CompagneRepository compagneRepository;

    public CompagneService(CompagneRepository compagneRepository) {
        this.compagneRepository = compagneRepository;
    }

    /**
     * Save a compagne.
     *
     * @param compagne the entity to save.
     * @return the persisted entity.
     */
    public Compagne save(Compagne compagne) {
        log.debug("Request to save Compagne : {}", compagne);
        return compagneRepository.save(compagne);
    }

    /**
     * Partially update a compagne.
     *
     * @param compagne the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Compagne> partialUpdate(Compagne compagne) {
        log.debug("Request to partially update Compagne : {}", compagne);

        return compagneRepository
            .findById(compagne.getId())
            .map(existingCompagne -> {
                if (compagne.getLibelle() != null) {
                    existingCompagne.setLibelle(compagne.getLibelle());
                }
                if (compagne.getDateCreation() != null) {
                    existingCompagne.setDateCreation(compagne.getDateCreation());
                }
                if (compagne.getEtat() != null) {
                    existingCompagne.setEtat(compagne.getEtat());
                }

                return existingCompagne;
            })
            .map(compagneRepository::save);
    }

    /**
     * Get all the compagnes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Compagne> findAll(Pageable pageable) {
        log.debug("Request to get all Compagnes");
        return compagneRepository.findAll(pageable);
    }

    /**
     * Get one compagne by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Compagne> findOne(Long id) {
        log.debug("Request to get Compagne : {}", id);
        return compagneRepository.findById(id);
    }

    /**
     * Delete the compagne by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Compagne : {}", id);
        compagneRepository.deleteById(id);
    }
}
