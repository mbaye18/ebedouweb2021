package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Infoplus;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.InfoplusRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Infoplus}.
 */
@Service
@Transactional
public class InfoplusService {

    private final Logger log = LoggerFactory.getLogger(InfoplusService.class);

    private final InfoplusRepository infoplusRepository;

    public InfoplusService(InfoplusRepository infoplusRepository) {
        this.infoplusRepository = infoplusRepository;
    }

    /**
     * Save a infoplus.
     *
     * @param infoplus the entity to save.
     * @return the persisted entity.
     */
    public Infoplus save(Infoplus infoplus) {
        log.debug("Request to save Infoplus : {}", infoplus);
        return infoplusRepository.save(infoplus);
    }

    /**
     * Partially update a infoplus.
     *
     * @param infoplus the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Infoplus> partialUpdate(Infoplus infoplus) {
        log.debug("Request to partially update Infoplus : {}", infoplus);

        return infoplusRepository
            .findById(infoplus.getId())
            .map(existingInfoplus -> {
                if (infoplus.getNumeroTelephone() != null) {
                    existingInfoplus.setNumeroTelephone(infoplus.getNumeroTelephone());
                }
                if (infoplus.getPieceIdentite() != null) {
                    existingInfoplus.setPieceIdentite(infoplus.getPieceIdentite());
                }
                if (infoplus.getPieceIdentiteContentType() != null) {
                    existingInfoplus.setPieceIdentiteContentType(infoplus.getPieceIdentiteContentType());
                }
                if (infoplus.getCopieAgrement() != null) {
                    existingInfoplus.setCopieAgrement(infoplus.getCopieAgrement());
                }
                if (infoplus.getCopieAgrementContentType() != null) {
                    existingInfoplus.setCopieAgrementContentType(infoplus.getCopieAgrementContentType());
                }
                if (infoplus.getDateNaissance() != null) {
                    existingInfoplus.setDateNaissance(infoplus.getDateNaissance());
                }
                if (infoplus.getLieuNaissance() != null) {
                    existingInfoplus.setLieuNaissance(infoplus.getLieuNaissance());
                }
                if (infoplus.getGenre() != null) {
                    existingInfoplus.setGenre(infoplus.getGenre());
                }

                return existingInfoplus;
            })
            .map(infoplusRepository::save);
    }

    /**
     * Get all the infopluses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Infoplus> findAll(Pageable pageable) {
        log.debug("Request to get all Infopluses");
        return infoplusRepository.findAll(pageable);
    }

    /**
     * Get one infoplus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Infoplus> findOne(String id) {
        log.debug("Request to get Infoplus : {}", id);
        return infoplusRepository.findByUserLogin(id);
    }

    /**
     * Delete the infoplus by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Infoplus : {}", id);
        infoplusRepository.deleteById(id);
    }

    public char[] findByUser(User user) {
        return null;
    }
}
