package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ConfigFile;
import com.mycompany.myapp.domain.Infoplus;
import com.mycompany.myapp.domain.SmsObject;
import com.mycompany.myapp.domain.User;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import tech.jhipster.config.JHipsterProperties;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(
        JHipsterProperties jHipsterProperties,
        JavaMailSender javaMailSender,
        MessageSource messageSource,
        SpringTemplateEngine templateEngine
    ) {
        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug(
            "Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart,
            isHtml,
            to,
            subject,
            content
        );

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }

    @Async
    public void sendSms(Infoplus infoplus) {
        SmsObject sms = new SmsObject();
        /* 
		sms.setDOMAIN(ConfigFile.getDOMAIN());
		sms.setPORT(ConfigFile.getPORT());
		sms.setPATH_SMS(ConfigFile.getPATH_SMS());
		sms.setPATH_BALANCE(ConfigFile.getPATH_BALANCE());
		sms.set_user_login(ConfigFile.get_user_login());
		sms.set_api_key(ConfigFile.get_api_key());
		sms.set_sms_text("Bonjour Mr/Mme "+infoplus.getUser().getFirstName()+" "+infoplus.getUser().getLastName()+" après vérification des informations, votre demande d'inscription sur la plateforme e.BeDou a été validé.Vous pouvez y accéder via le lien ci-dessous en utilisant le login et le mot de passe que vous aviez utilisé lors de votre inscription."+" Lien du site : ");
        ArrayList<String> numeros =  new ArrayList<>();
        numeros.add(infoplus.getNumeroTelephone());
		sms.set_sms_recipients(numeros);
		sms.set_recipients_first_names(new ArrayList<>(ConfigFile.get_recipients_first_names()));
		sms.set_recipients_last_names(new ArrayList<>(ConfigFile.get_recipients_last_names()));
		sms.set_sms_fields_1(new ArrayList<>(ConfigFile.get_sms_fields_1()));
		sms.set_sms_fields_2(new ArrayList<>(ConfigFile.get_sms_fields_2()));
		sms.set_sms_fields_3(new ArrayList<>(ConfigFile.get_sms_fields_3()));

		sms.set_sms_mode(ConfigFile.getInstantane());	// INSTANTANE or change to DIFFERE for delay sms-mode sending

		sms.set_sms_type(ConfigFile.getQualitePro());	// QUALITE_STANDARD or change to QUALITE_PRO

		//Change to correct value of DAY_OF_MONTH, MONTH, HOUR_OF_DAY etc'
		sms.set_sms_d(ConfigFile.get_sms_d());	//	DAY_OF_MONTH
		sms.set_sms_m(ConfigFile.get_sms_m());	//	MONTH
		sms.set_sms_h(ConfigFile.get_sms_h());	//	HOUR_OF_DAY;
		sms.set_sms_i(ConfigFile.get_sms_i());	//	MINUTE;
		sms.set_sms_y(ConfigFile.get_sms_y());	//	YEAR;

		sms.set_sms_sender(ConfigFile.get_sms_sender());
		sms.set_request_mode(ConfigFile.getReel());
        sms.set_sms_ticket(ConfigFile.get_sms_ticket());
        System.out.println(infoplus);
        try {
            sms.sendSms();
        } catch (Exception e) {
            //TODO: handle exception

        } */
    }
}
