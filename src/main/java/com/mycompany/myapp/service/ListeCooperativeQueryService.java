package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.domain.ListeCooperative;
import com.mycompany.myapp.repository.ListeCooperativeRepository;
import com.mycompany.myapp.service.criteria.ListeCooperativeCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ListeCooperative} entities in the database.
 * The main input is a {@link ListeCooperativeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ListeCooperative} or a {@link Page} of {@link ListeCooperative} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ListeCooperativeQueryService extends QueryService<ListeCooperative> {

    private final Logger log = LoggerFactory.getLogger(ListeCooperativeQueryService.class);

    private final ListeCooperativeRepository listeCooperativeRepository;

    public ListeCooperativeQueryService(ListeCooperativeRepository listeCooperativeRepository) {
        this.listeCooperativeRepository = listeCooperativeRepository;
    }

    /**
     * Return a {@link List} of {@link ListeCooperative} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ListeCooperative> findByCriteria(ListeCooperativeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ListeCooperative> specification = createSpecification(criteria);
        return listeCooperativeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ListeCooperative} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ListeCooperative> findByCriteria(ListeCooperativeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ListeCooperative> specification = createSpecification(criteria);
        return listeCooperativeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ListeCooperativeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ListeCooperative> specification = createSpecification(criteria);
        return listeCooperativeRepository.count(specification);
    }

    /**
     * Function to convert {@link ListeCooperativeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ListeCooperative> createSpecification(ListeCooperativeCriteria criteria) {
        Specification<ListeCooperative> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ListeCooperative_.id));
            }
            if (criteria.getMembreId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMembreId(),
                            root -> root.join(ListeCooperative_.membre, JoinType.LEFT).get(Infoplus_.id)
                        )
                    );
            }
            if (criteria.getCooperativeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCooperativeId(),
                            root -> root.join(ListeCooperative_.cooperative, JoinType.LEFT).get(Cooperative_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
