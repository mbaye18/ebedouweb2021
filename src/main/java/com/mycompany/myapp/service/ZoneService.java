package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.repository.ZoneRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Zone}.
 */
@Service
@Transactional
public class ZoneService {

    private final Logger log = LoggerFactory.getLogger(ZoneService.class);

    private final ZoneRepository zoneRepository;

    public ZoneService(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    /**
     * Save a zone.
     *
     * @param zone the entity to save.
     * @return the persisted entity.
     */
    public Zone save(Zone zone) {
        log.debug("Request to save Zone : {}", zone);
        return zoneRepository.save(zone);
    }

    /**
     * Partially update a zone.
     *
     * @param zone the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Zone> partialUpdate(Zone zone) {
        log.debug("Request to partially update Zone : {}", zone);

        return zoneRepository
            .findById(zone.getId())
            .map(existingZone -> {
                if (zone.getLibelle() != null) {
                    existingZone.setLibelle(zone.getLibelle());
                }

                return existingZone;
            })
            .map(zoneRepository::save);
    }

    /**
     * Get all the zones.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Zone> findAll(Pageable pageable) {
        log.debug("Request to get all Zones");
        return zoneRepository.findAll(pageable);
    }

    /**
     * Get one zone by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Zone> findOne(Long id) {
        log.debug("Request to get Zone : {}", id);
        return zoneRepository.findById(id);
    }

    /**
     * Delete the zone by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Zone : {}", id);
        zoneRepository.deleteById(id);
    }
}
