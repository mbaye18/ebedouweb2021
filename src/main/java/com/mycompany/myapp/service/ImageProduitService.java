package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ImageProduit;
import com.mycompany.myapp.repository.ImageProduitRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ImageProduit}.
 */
@Service
@Transactional
public class ImageProduitService {

    private final Logger log = LoggerFactory.getLogger(ImageProduitService.class);

    private final ImageProduitRepository imageProduitRepository;

    public ImageProduitService(ImageProduitRepository imageProduitRepository) {
        this.imageProduitRepository = imageProduitRepository;
    }

    /**
     * Save a imageProduit.
     *
     * @param imageProduit the entity to save.
     * @return the persisted entity.
     */
    public ImageProduit save(ImageProduit imageProduit) {
        log.debug("Request to save ImageProduit : {}", imageProduit);
        return imageProduitRepository.save(imageProduit);
    }

    /**
     * Partially update a imageProduit.
     *
     * @param imageProduit the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ImageProduit> partialUpdate(ImageProduit imageProduit) {
        log.debug("Request to partially update ImageProduit : {}", imageProduit);

        return imageProduitRepository
            .findById(imageProduit.getId())
            .map(existingImageProduit -> {
                if (imageProduit.getImage() != null) {
                    existingImageProduit.setImage(imageProduit.getImage());
                }
                if (imageProduit.getImageContentType() != null) {
                    existingImageProduit.setImageContentType(imageProduit.getImageContentType());
                }
                if (imageProduit.getDate() != null) {
                    existingImageProduit.setDate(imageProduit.getDate());
                }

                return existingImageProduit;
            })
            .map(imageProduitRepository::save);
    }

    /**
     * Get all the imageProduits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageProduit> findAll(Pageable pageable) {
        log.debug("Request to get all ImageProduits");
        return imageProduitRepository.findAll(pageable);
    }

    /**
     * Get one imageProduit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ImageProduit> findOne(Long id) {
        log.debug("Request to get ImageProduit : {}", id);
        return imageProduitRepository.findById(id);
    }

    /**
     * Delete the imageProduit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ImageProduit : {}", id);
        imageProduitRepository.deleteById(id);
    }
}
