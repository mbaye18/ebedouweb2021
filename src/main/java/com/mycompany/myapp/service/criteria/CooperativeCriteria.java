package com.mycompany.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;

import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.InstantFilter;

import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.Cooperative} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.CooperativeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cooperatives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CooperativeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter libelle;

    private InstantFilter dateCreation;

    private InstantFilter dateModification;

    private BooleanFilter etat;

    private LongFilter gerantId;

    private Boolean distinct;

    public CooperativeCriteria() {}

    public CooperativeCriteria(CooperativeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.libelle = other.libelle == null ? null : other.libelle.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.etat = other.etat == null ? null : other.etat.copy();
        this.gerantId = other.gerantId == null ? null : other.gerantId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CooperativeCriteria copy() {
        return new CooperativeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLibelle() {
        return libelle;
    }

    public StringFilter libelle() {
        if (libelle == null) {
            libelle = new StringFilter();
        }
        return libelle;
    }

    public void setLibelle(StringFilter libelle) {
        this.libelle = libelle;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public InstantFilter dateCreation() {
        if (dateCreation == null) {
            dateCreation = new InstantFilter();
        }
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public InstantFilter dateModification() {
        if (dateModification == null) {
            dateModification = new InstantFilter();
        }
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public BooleanFilter getEtat() {
        return etat;
    }

    public BooleanFilter etat() {
        if (etat == null) {
            etat = new BooleanFilter();
        }
        return etat;
    }

    public void setEtat(BooleanFilter etat) {
        this.etat = etat;
    }

    public LongFilter getGerantId() {
        return gerantId;
    }

    public LongFilter gerantId() {
        if (gerantId == null) {
            gerantId = new LongFilter();
        }
        return gerantId;
    }

    public void setGerantId(LongFilter gerantId) {
        this.gerantId = gerantId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CooperativeCriteria that = (CooperativeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(libelle, that.libelle) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(etat, that.etat) &&
            Objects.equals(gerantId, that.gerantId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, dateCreation, dateModification, etat, gerantId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooperativeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (libelle != null ? "libelle=" + libelle + ", " : "") +
            (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
            (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
            (etat != null ? "etat=" + etat + ", " : "") +
            (gerantId != null ? "gerantId=" + gerantId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
