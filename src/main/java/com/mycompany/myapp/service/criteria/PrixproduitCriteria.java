package com.mycompany.myapp.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.Prixproduit} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.PrixproduitResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /prixproduits?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PrixproduitCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter prix;

    private InstantFilter dateAjout;

    private InstantFilter dateModification;

    private LongFilter produitId;

    private LongFilter compagneId;

    private Boolean distinct;

    public PrixproduitCriteria() {}

    public PrixproduitCriteria(PrixproduitCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.prix = other.prix == null ? null : other.prix.copy();
        this.dateAjout = other.dateAjout == null ? null : other.dateAjout.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.produitId = other.produitId == null ? null : other.produitId.copy();
        this.compagneId = other.compagneId == null ? null : other.compagneId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PrixproduitCriteria copy() {
        return new PrixproduitCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getPrix() {
        return prix;
    }

    public DoubleFilter prix() {
        if (prix == null) {
            prix = new DoubleFilter();
        }
        return prix;
    }

    public void setPrix(DoubleFilter prix) {
        this.prix = prix;
    }

    public InstantFilter getDateAjout() {
        return dateAjout;
    }

    public InstantFilter dateAjout() {
        if (dateAjout == null) {
            dateAjout = new InstantFilter();
        }
        return dateAjout;
    }

    public void setDateAjout(InstantFilter dateAjout) {
        this.dateAjout = dateAjout;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public InstantFilter dateModification() {
        if (dateModification == null) {
            dateModification = new InstantFilter();
        }
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public LongFilter getProduitId() {
        return produitId;
    }

    public LongFilter produitId() {
        if (produitId == null) {
            produitId = new LongFilter();
        }
        return produitId;
    }

    public void setProduitId(LongFilter produitId) {
        this.produitId = produitId;
    }

    public LongFilter getCompagneId() {
        return compagneId;
    }

    public LongFilter compagneId() {
        if (compagneId == null) {
            compagneId = new LongFilter();
        }
        return compagneId;
    }

    public void setCompagneId(LongFilter compagneId) {
        this.compagneId = compagneId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PrixproduitCriteria that = (PrixproduitCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(prix, that.prix) &&
            Objects.equals(dateAjout, that.dateAjout) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(produitId, that.produitId) &&
            Objects.equals(compagneId, that.compagneId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prix, dateAjout, dateModification, produitId, compagneId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrixproduitCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (prix != null ? "prix=" + prix + ", " : "") +
            (dateAjout != null ? "dateAjout=" + dateAjout + ", " : "") +
            (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
            (produitId != null ? "produitId=" + produitId + ", " : "") +
            (compagneId != null ? "compagneId=" + compagneId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
