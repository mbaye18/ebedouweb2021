package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.QuantiteProduit;
import com.mycompany.myapp.repository.QuantiteProduitRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link QuantiteProduit}.
 */
@Service
@Transactional
public class QuantiteProduitService {

    private final Logger log = LoggerFactory.getLogger(QuantiteProduitService.class);

    private final QuantiteProduitRepository quantiteProduitRepository;

    public QuantiteProduitService(QuantiteProduitRepository quantiteProduitRepository) {
        this.quantiteProduitRepository = quantiteProduitRepository;
    }

    /**
     * Save a quantiteProduit.
     *
     * @param quantiteProduit the entity to save.
     * @return the persisted entity.
     */
    public QuantiteProduit save(QuantiteProduit quantiteProduit) {
        log.debug("Request to save QuantiteProduit : {}", quantiteProduit);
        return quantiteProduitRepository.save(quantiteProduit);
    }

    /**
     * Partially update a quantiteProduit.
     *
     * @param quantiteProduit the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<QuantiteProduit> partialUpdate(QuantiteProduit quantiteProduit) {
        log.debug("Request to partially update QuantiteProduit : {}", quantiteProduit);

        return quantiteProduitRepository
            .findById(quantiteProduit.getId())
            .map(existingQuantiteProduit -> {
                if (quantiteProduit.getQuantite() != null) {
                    existingQuantiteProduit.setQuantite(quantiteProduit.getQuantite());
                }
                if (quantiteProduit.getDateAjout() != null) {
                    existingQuantiteProduit.setDateAjout(quantiteProduit.getDateAjout());
                }
                if (quantiteProduit.getDateModification() != null) {
                    existingQuantiteProduit.setDateModification(quantiteProduit.getDateModification());
                }
                if (quantiteProduit.getDescription() != null) {
                    existingQuantiteProduit.setDescription(quantiteProduit.getDescription());
                }
                if (quantiteProduit.getQuantiteRestant() != null) {
                    existingQuantiteProduit.setQuantiteRestant(quantiteProduit.getQuantiteRestant());
                }

                return existingQuantiteProduit;
            })
            .map(quantiteProduitRepository::save);
    }

    /**
     * Get all the quantiteProduits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<QuantiteProduit> findAll(Pageable pageable) {
        log.debug("Request to get all QuantiteProduits");
        return quantiteProduitRepository.findAll(pageable);
    }

    /**
     * Get one quantiteProduit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<QuantiteProduit> findOne(Long id) {
        log.debug("Request to get QuantiteProduit : {}", id);
        return quantiteProduitRepository.findById(id);
    }

    /**
     * Delete the quantiteProduit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete QuantiteProduit : {}", id);
        quantiteProduitRepository.deleteById(id);
    }
}
