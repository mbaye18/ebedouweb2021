package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.domain.Prixproduit;
import com.mycompany.myapp.repository.PrixproduitRepository;
import com.mycompany.myapp.service.criteria.PrixproduitCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Prixproduit} entities in the database.
 * The main input is a {@link PrixproduitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Prixproduit} or a {@link Page} of {@link Prixproduit} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PrixproduitQueryService extends QueryService<Prixproduit> {

    private final Logger log = LoggerFactory.getLogger(PrixproduitQueryService.class);

    private final PrixproduitRepository prixproduitRepository;

    public PrixproduitQueryService(PrixproduitRepository prixproduitRepository) {
        this.prixproduitRepository = prixproduitRepository;
    }

    /**
     * Return a {@link List} of {@link Prixproduit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Prixproduit> findByCriteria(PrixproduitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Prixproduit> specification = createSpecification(criteria);
        return prixproduitRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Prixproduit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Prixproduit> findByCriteria(PrixproduitCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Prixproduit> specification = createSpecification(criteria);
        return prixproduitRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PrixproduitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Prixproduit> specification = createSpecification(criteria);
        return prixproduitRepository.count(specification);
    }

    /**
     * Function to convert {@link PrixproduitCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Prixproduit> createSpecification(PrixproduitCriteria criteria) {
        Specification<Prixproduit> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Prixproduit_.id));
            }
            if (criteria.getPrix() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrix(), Prixproduit_.prix));
            }
            if (criteria.getDateAjout() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateAjout(), Prixproduit_.dateAjout));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Prixproduit_.dateModification));
            }
            if (criteria.getProduitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getProduitId(), root -> root.join(Prixproduit_.produit, JoinType.LEFT).get(Produit_.id))
                    );
            }
            if (criteria.getCompagneId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCompagneId(),
                            root -> root.join(Prixproduit_.compagne, JoinType.LEFT).get(Compagne_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
