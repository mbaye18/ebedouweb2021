package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Demande;
import com.mycompany.myapp.repository.DemandeRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Demande}.
 */
@Service
@Transactional
public class DemandeService {

    private final Logger log = LoggerFactory.getLogger(DemandeService.class);

    private final DemandeRepository demandeRepository;

    public DemandeService(DemandeRepository demandeRepository) {
        this.demandeRepository = demandeRepository;
    }

    /**
     * Save a demande.
     *
     * @param demande the entity to save.
     * @return the persisted entity.
     */
    public Demande save(Demande demande) {
        log.debug("Request to save Demande : {}", demande);
        return demandeRepository.save(demande);
    }

    /**
     * Partially update a demande.
     *
     * @param demande the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Demande> partialUpdate(Demande demande) {
        log.debug("Request to partially update Demande : {}", demande);

        return demandeRepository
            .findById(demande.getId())
            .map(existingDemande -> {
                if (demande.getDate() != null) {
                    existingDemande.setDate(demande.getDate());
                }
                if (demande.getEtat() != null) {
                    existingDemande.setEtat(demande.getEtat());
                }

                return existingDemande;
            })
            .map(demandeRepository::save);
    }

    /**
     * Get all the demandes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Demande> findAll(Pageable pageable) {
        log.debug("Request to get all Demandes");
        return demandeRepository.findAll(pageable);
    }

    /**
     * Get one demande by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Demande> findOne(Long id) {
        log.debug("Request to get Demande : {}", id);
        return demandeRepository.findById(id);
    }

    /**
     * Delete the demande by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Demande : {}", id);
        demandeRepository.deleteById(id);
    }
}
