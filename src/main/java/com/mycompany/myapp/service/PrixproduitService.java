package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Prixproduit;
import com.mycompany.myapp.repository.PrixproduitRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Prixproduit}.
 */
@Service
@Transactional
public class PrixproduitService {

    private final Logger log = LoggerFactory.getLogger(PrixproduitService.class);

    private final PrixproduitRepository prixproduitRepository;

    public PrixproduitService(PrixproduitRepository prixproduitRepository) {
        this.prixproduitRepository = prixproduitRepository;
    }

    /**
     * Save a prixproduit.
     *
     * @param prixproduit the entity to save.
     * @return the persisted entity.
     */
    public Prixproduit save(Prixproduit prixproduit) {
        log.debug("Request to save Prixproduit : {}", prixproduit);
        return prixproduitRepository.save(prixproduit);
    }

    /**
     * Partially update a prixproduit.
     *
     * @param prixproduit the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Prixproduit> partialUpdate(Prixproduit prixproduit) {
        log.debug("Request to partially update Prixproduit : {}", prixproduit);

        return prixproduitRepository
            .findById(prixproduit.getId())
            .map(existingPrixproduit -> {
                if (prixproduit.getPrix() != null) {
                    existingPrixproduit.setPrix(prixproduit.getPrix());
                }
                if (prixproduit.getDateAjout() != null) {
                    existingPrixproduit.setDateAjout(prixproduit.getDateAjout());
                }
                if (prixproduit.getDateModification() != null) {
                    existingPrixproduit.setDateModification(prixproduit.getDateModification());
                }

                return existingPrixproduit;
            })
            .map(prixproduitRepository::save);
    }

    /**
     * Get all the prixproduits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Prixproduit> findAll(Pageable pageable) {
        log.debug("Request to get all Prixproduits");
        return prixproduitRepository.findAll(pageable);
    }

    /**
     * Get one prixproduit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Prixproduit> findOne(Long id) {
        log.debug("Request to get Prixproduit : {}", id);
        return prixproduitRepository.findById(id);
    }

    /**
     * Delete the prixproduit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Prixproduit : {}", id);
        prixproduitRepository.deleteById(id);
    }
}
