package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ListeCooperative;
import com.mycompany.myapp.repository.ListeCooperativeRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ListeCooperative}.
 */
@Service
@Transactional
public class ListeCooperativeService {

    private final Logger log = LoggerFactory.getLogger(ListeCooperativeService.class);

    private final ListeCooperativeRepository listeCooperativeRepository;

    public ListeCooperativeService(ListeCooperativeRepository listeCooperativeRepository) {
        this.listeCooperativeRepository = listeCooperativeRepository;
    }

    /**
     * Save a listeCooperative.
     *
     * @param listeCooperative the entity to save.
     * @return the persisted entity.
     */
    public ListeCooperative save(ListeCooperative listeCooperative) {
        log.debug("Request to save ListeCooperative : {}", listeCooperative);
        return listeCooperativeRepository.save(listeCooperative);
    }

    /**
     * Partially update a listeCooperative.
     *
     * @param listeCooperative the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ListeCooperative> partialUpdate(ListeCooperative listeCooperative) {
        log.debug("Request to partially update ListeCooperative : {}", listeCooperative);

        return listeCooperativeRepository
            .findById(listeCooperative.getId())
            .map(existingListeCooperative -> {
                return existingListeCooperative;
            })
            .map(listeCooperativeRepository::save);
    }

    /**
     * Get all the listeCooperatives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ListeCooperative> findAll(Pageable pageable) {
        log.debug("Request to get all ListeCooperatives");
        return listeCooperativeRepository.findAll(pageable);
    }

    /**
     * Get one listeCooperative by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ListeCooperative> findOne(Long id) {
        log.debug("Request to get ListeCooperative : {}", id);
        return listeCooperativeRepository.findById(id);
    }

    /**
     * Delete the listeCooperative by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ListeCooperative : {}", id);
        listeCooperativeRepository.deleteById(id);
    }
}
