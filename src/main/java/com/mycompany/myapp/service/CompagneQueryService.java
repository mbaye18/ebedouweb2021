package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.domain.Compagne;
import com.mycompany.myapp.repository.CompagneRepository;
import com.mycompany.myapp.service.criteria.CompagneCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Compagne} entities in the database.
 * The main input is a {@link CompagneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Compagne} or a {@link Page} of {@link Compagne} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompagneQueryService extends QueryService<Compagne> {

    private final Logger log = LoggerFactory.getLogger(CompagneQueryService.class);

    private final CompagneRepository compagneRepository;

    public CompagneQueryService(CompagneRepository compagneRepository) {
        this.compagneRepository = compagneRepository;
    }

    /**
     * Return a {@link List} of {@link Compagne} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Compagne> findByCriteria(CompagneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Compagne> specification = createSpecification(criteria);
        return compagneRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Compagne} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Compagne> findByCriteria(CompagneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Compagne> specification = createSpecification(criteria);
        return compagneRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompagneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Compagne> specification = createSpecification(criteria);
        return compagneRepository.count(specification);
    }

    /**
     * Function to convert {@link CompagneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Compagne> createSpecification(CompagneCriteria criteria) {
        Specification<Compagne> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Compagne_.id));
            }
            if (criteria.getLibelle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLibelle(), Compagne_.libelle));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Compagne_.dateCreation));
            }
            if (criteria.getEtat() != null) {
                specification = specification.and(buildSpecification(criteria.getEtat(), Compagne_.etat));
            }
        }
        return specification;
    }
}
