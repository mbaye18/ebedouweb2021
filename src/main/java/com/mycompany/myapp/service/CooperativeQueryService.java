package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*; // for static metamodels
import com.mycompany.myapp.domain.Cooperative;
import com.mycompany.myapp.repository.CooperativeRepository;

import com.mycompany.myapp.service.criteria.CooperativeCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Cooperative} entities in the database.
 * The main input is a {@link CooperativeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Cooperative} or a {@link Page} of {@link Cooperative} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CooperativeQueryService extends QueryService<Cooperative> {

    private final Logger log = LoggerFactory.getLogger(CooperativeQueryService.class);

    private final CooperativeRepository cooperativeRepository;

   

    public CooperativeQueryService(CooperativeRepository cooperativeRepository) {
        this.cooperativeRepository = cooperativeRepository;
      
    }

    /**
     * Return a {@link List} of {@link Cooperative} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Cooperative> findByCriteria(CooperativeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cooperative> specification = createSpecification(criteria);
        return cooperativeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Cooperative} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Cooperative> findByCriteria(CooperativeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cooperative> specification = createSpecification(criteria);
        return cooperativeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CooperativeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cooperative> specification = createSpecification(criteria);
        return cooperativeRepository.count(specification);
    }

    /**
     * Function to convert {@link CooperativeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Cooperative> createSpecification(CooperativeCriteria criteria) {
        Specification<Cooperative> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Cooperative_.id));
            }
            if (criteria.getLibelle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLibelle(), Cooperative_.libelle));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Cooperative_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Cooperative_.dateModification));
            }
            if (criteria.getEtat() != null) {
                specification = specification.and(buildSpecification(criteria.getEtat(), Cooperative_.etat));
            }
            if (criteria.getGerantId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getGerantId(), root -> root.join(Cooperative_.gerant, JoinType.LEFT).get(Infoplus_.id))
                    );
            }
        }
        return specification;
    }
}
