package com.mycompany.myapp.service;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mycompany.myapp.domain.Cooperative;
import com.mycompany.myapp.repository.CooperativeRepository;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Cooperative}.
 */
@Service
@Transactional
public class CooperativeService {

    private final Logger log = LoggerFactory.getLogger(CooperativeService.class);

    private final CooperativeRepository cooperativeRepository;

  

    public CooperativeService(CooperativeRepository cooperativeRepository) {
        this.cooperativeRepository = cooperativeRepository;
       
    }

    /**
     * Save a cooperative.
     *
     * @param cooperative the entity to save.
     * @return the persisted entity.
     */
    public Cooperative save(Cooperative cooperative) {
        log.debug("Request to save Cooperative : {}", cooperative);
        Cooperative result = cooperativeRepository.save(cooperative);
       
        return result;
    }

    /**
     * Partially update a cooperative.
     *
     * @param cooperative the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Cooperative> partialUpdate(Cooperative cooperative) {
        log.debug("Request to partially update Cooperative : {}", cooperative);

        return cooperativeRepository
            .findById(cooperative.getId())
            .map(existingCooperative -> {
                if (cooperative.getLibelle() != null) {
                    existingCooperative.setLibelle(cooperative.getLibelle());
                }
                if (cooperative.getDateCreation() != null) {
                    existingCooperative.setDateCreation(cooperative.getDateCreation());
                }
                if (cooperative.getDateModification() != null) {
                    existingCooperative.setDateModification(cooperative.getDateModification());
                }
                if (cooperative.getEtat() != null) {
                    existingCooperative.setEtat(cooperative.getEtat());
                }

                return existingCooperative;
            })
            .map(cooperativeRepository::save)
            .map(savedCooperative -> {
        
                return savedCooperative;
            });
    }

    /**
     * Get all the cooperatives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Cooperative> findAll(Pageable pageable) {
        log.debug("Request to get all Cooperatives");
        return cooperativeRepository.findAll(pageable);
    }

    /**
     * Get one cooperative by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Cooperative> findOne(Long id) {
        log.debug("Request to get Cooperative : {}", id);
        return cooperativeRepository.findById(id);
    }

    /**
     * Delete the cooperative by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Cooperative : {}", id);
        cooperativeRepository.deleteById(id);
       
    }

    /**
     * Search for the cooperative corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Cooperative> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Cooperatives for query {}", query);
        return null;
    }
}
