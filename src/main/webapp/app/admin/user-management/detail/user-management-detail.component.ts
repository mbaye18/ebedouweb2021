import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataUtils } from 'app/core/util/data-util.service';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { InfoplusService } from 'app/entities/infoplus/service/infoplus.service';

import { User } from '../user-management.model';

@Component({
  selector: 'jhi-user-mgmt-detail',
  templateUrl: './user-management-detail.component.html',
})
export class UserManagementDetailComponent implements OnInit {
  user: User | null = null;
  infoplus: IInfoplus | null=null;

  constructor(public dataUtils:DataUtils,private route: ActivatedRoute,private infoplusService:InfoplusService ) {}

  ngOnInit(): void {
    this.route.data.subscribe(({ user }) => {
      this.user = user;
      this.infoplusService.find(user.login).subscribe((res)=> {
        this.infoplus=res.body
      })
    });
  }
  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }
  pre():void{
    window.history.back()
  }
  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }
}
