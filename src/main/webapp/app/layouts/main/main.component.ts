import { Component, OnInit, RendererFactory2, Renderer2, ChangeDetectorRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import * as dayjs from 'dayjs';

import { AccountService } from 'app/core/auth/account.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { LoginService } from 'app/login/login.service';
import { SessionStorageService } from 'ngx-webstorage';
import { LANGUAGES } from 'app/config/language.constants';

@Component({
  selector: 'jhi-main',
  templateUrl: './main.component.html',
  styleUrls: ['../navbar/navbar.component.scss'],
})
export class MainComponent implements OnInit {
  private renderer: Renderer2;
    mobileQuery!: MediaQueryList;
    mobileQueryListener: () => void;
   etat! : boolean;
   languages = LANGUAGES;
   account!: any;
   shownoteparametre=false;
   showutilisateur=false;
  constructor(
    private accountService: AccountService,
    private titleService: Title,
    private router: Router,
    private translateService: TranslateService,
    rootRenderer: RendererFactory2,
    private sessionStorageService: SessionStorageService,
    private loginService: LoginService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    this.renderer = rootRenderer.createRenderer(document.querySelector('html'), null);
  }

  ngOnInit(): void {
    // try to log in automatically
    this.accountService.identity().subscribe();
    this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.updateTitle();
      }
    });
    this.etat=true
    this.translateService.onLangChange.subscribe((langChangeEvent: LangChangeEvent) => {
      this.updateTitle();
      dayjs.locale(langChangeEvent.lang);
      this.renderer.setAttribute(document.querySelector('html'), 'lang', langChangeEvent.lang);
    });
  }
  login(): void {
    this.router.navigate(['/login']);
  }
  onclick(actife:string) :void{
    this.router.navigate([`/admin/user-management/`+actife]);
  }
  changeLanguage(languageKey: string): void {
    this.sessionStorageService.store('locale', languageKey);
    this.translateService.use(languageKey);
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }
  
  logout(): void {

    this.loginService.logout();
    this.router.navigate(['']);
  }
  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot): string {
    const title: string = routeSnapshot.data['pageTitle'] ?? '';
    if (routeSnapshot.firstChild) {
      return this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  private updateTitle(): void {
    let pageTitle = this.getPageTitle(this.router.routerState.snapshot.root);
    if (!pageTitle) {
      pageTitle = 'global.title';
    }
    this.translateService.get(pageTitle).subscribe(title => this.titleService.setTitle(title));
  }
}
