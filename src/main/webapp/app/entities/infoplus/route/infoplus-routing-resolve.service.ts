import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IInfoplus, Infoplus } from '../infoplus.model';
import { InfoplusService } from '../service/infoplus.service';

@Injectable({ providedIn: 'root' })
export class InfoplusRoutingResolveService implements Resolve<IInfoplus> {
  constructor(protected service: InfoplusService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IInfoplus> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((infoplus: HttpResponse<Infoplus>) => {
          if (infoplus.body) {
            return of(infoplus.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Infoplus());
  }
}
