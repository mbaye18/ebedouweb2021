import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { InfoplusComponent } from '../list/infoplus.component';
import { InfoplusDetailComponent } from '../detail/infoplus-detail.component';
import { InfoplusUpdateComponent } from '../update/infoplus-update.component';
import { InfoplusRoutingResolveService } from './infoplus-routing-resolve.service';

const infoplusRoute: Routes = [
  {
    path: '',
    component: InfoplusComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: InfoplusDetailComponent,
    resolve: {
      infoplus: InfoplusRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: InfoplusUpdateComponent,
    resolve: {
      infoplus: InfoplusRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: InfoplusUpdateComponent,
    resolve: {
      infoplus: InfoplusRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(infoplusRoute)],
  exports: [RouterModule],
})
export class InfoplusRoutingModule {}
