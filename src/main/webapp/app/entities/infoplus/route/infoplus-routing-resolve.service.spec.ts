jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IInfoplus, Infoplus } from '../infoplus.model';
import { InfoplusService } from '../service/infoplus.service';

import { InfoplusRoutingResolveService } from './infoplus-routing-resolve.service';

describe('Infoplus routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: InfoplusRoutingResolveService;
  let service: InfoplusService;
  let resultInfoplus: IInfoplus | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(InfoplusRoutingResolveService);
    service = TestBed.inject(InfoplusService);
    resultInfoplus = undefined;
  });

  describe('resolve', () => {
    it('should return IInfoplus returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultInfoplus = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultInfoplus).toEqual({ id: 123 });
    });

    it('should return new IInfoplus if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultInfoplus = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultInfoplus).toEqual(new Infoplus());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Infoplus })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultInfoplus = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultInfoplus).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
