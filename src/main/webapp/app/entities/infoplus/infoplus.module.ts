import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { InfoplusComponent } from './list/infoplus.component';
import { InfoplusDetailComponent } from './detail/infoplus-detail.component';
import { InfoplusUpdateComponent } from './update/infoplus-update.component';
import { InfoplusDeleteDialogComponent } from './delete/infoplus-delete-dialog.component';
import { InfoplusRoutingModule } from './route/infoplus-routing.module';

@NgModule({
  imports: [SharedModule, InfoplusRoutingModule],
  declarations: [InfoplusComponent, InfoplusDetailComponent, InfoplusUpdateComponent, InfoplusDeleteDialogComponent],
  entryComponents: [InfoplusDeleteDialogComponent],
})
export class InfoplusModule {}
