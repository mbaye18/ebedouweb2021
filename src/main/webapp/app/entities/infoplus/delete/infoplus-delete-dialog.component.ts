import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IInfoplus } from '../infoplus.model';
import { InfoplusService } from '../service/infoplus.service';

@Component({
  templateUrl: './infoplus-delete-dialog.component.html',
})
export class InfoplusDeleteDialogComponent {
  infoplus?: IInfoplus;

  constructor(protected infoplusService: InfoplusService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.infoplusService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
