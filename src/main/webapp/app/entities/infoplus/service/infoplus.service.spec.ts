import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IInfoplus, Infoplus } from '../infoplus.model';

import { InfoplusService } from './infoplus.service';

describe('Infoplus Service', () => {
  let service: InfoplusService;
  let httpMock: HttpTestingController;
  let elemDefault: IInfoplus;
  let expectedResult: IInfoplus | IInfoplus[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(InfoplusService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      numeroTelephone: 'AAAAAAA',
      pieceIdentiteContentType: 'image/png',
      pieceIdentite: 'AAAAAAA',
      copieAgrementContentType: 'image/png',
      copieAgrement: 'AAAAAAA',
      dateNaissance: currentDate,
      lieuNaissance: 'AAAAAAA',
      genre: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateNaissance: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Infoplus', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateNaissance: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateNaissance: currentDate,
        },
        returnedFromService
      );

      service.create(new Infoplus()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Infoplus', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          numeroTelephone: 'BBBBBB',
          pieceIdentite: 'BBBBBB',
          copieAgrement: 'BBBBBB',
          dateNaissance: currentDate.format(DATE_FORMAT),
          lieuNaissance: 'BBBBBB',
          genre: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateNaissance: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Infoplus', () => {
      const patchObject = Object.assign(
        {
          numeroTelephone: 'BBBBBB',
          copieAgrement: 'BBBBBB',
          dateNaissance: currentDate.format(DATE_FORMAT),
          lieuNaissance: 'BBBBBB',
          genre: 'BBBBBB',
        },
        new Infoplus()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateNaissance: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Infoplus', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          numeroTelephone: 'BBBBBB',
          pieceIdentite: 'BBBBBB',
          copieAgrement: 'BBBBBB',
          dateNaissance: currentDate.format(DATE_FORMAT),
          lieuNaissance: 'BBBBBB',
          genre: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateNaissance: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Infoplus', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addInfoplusToCollectionIfMissing', () => {
      it('should add a Infoplus to an empty array', () => {
        const infoplus: IInfoplus = { id: 123 };
        expectedResult = service.addInfoplusToCollectionIfMissing([], infoplus);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(infoplus);
      });

      it('should not add a Infoplus to an array that contains it', () => {
        const infoplus: IInfoplus = { id: 123 };
        const infoplusCollection: IInfoplus[] = [
          {
            ...infoplus,
          },
          { id: 456 },
        ];
        expectedResult = service.addInfoplusToCollectionIfMissing(infoplusCollection, infoplus);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Infoplus to an array that doesn't contain it", () => {
        const infoplus: IInfoplus = { id: 123 };
        const infoplusCollection: IInfoplus[] = [{ id: 456 }];
        expectedResult = service.addInfoplusToCollectionIfMissing(infoplusCollection, infoplus);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(infoplus);
      });

      it('should add only unique Infoplus to an array', () => {
        const infoplusArray: IInfoplus[] = [{ id: 123 }, { id: 456 }, { id: 97752 }];
        const infoplusCollection: IInfoplus[] = [{ id: 123 }];
        expectedResult = service.addInfoplusToCollectionIfMissing(infoplusCollection, ...infoplusArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const infoplus: IInfoplus = { id: 123 };
        const infoplus2: IInfoplus = { id: 456 };
        expectedResult = service.addInfoplusToCollectionIfMissing([], infoplus, infoplus2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(infoplus);
        expect(expectedResult).toContain(infoplus2);
      });

      it('should accept null and undefined values', () => {
        const infoplus: IInfoplus = { id: 123 };
        expectedResult = service.addInfoplusToCollectionIfMissing([], null, infoplus, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(infoplus);
      });

      it('should return initial array if no Infoplus is added', () => {
        const infoplusCollection: IInfoplus[] = [{ id: 123 }];
        expectedResult = service.addInfoplusToCollectionIfMissing(infoplusCollection, undefined, null);
        expect(expectedResult).toEqual(infoplusCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
