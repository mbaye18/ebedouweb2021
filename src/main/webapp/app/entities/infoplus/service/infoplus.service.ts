import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IInfoplus, getInfoplusIdentifier } from '../infoplus.model';

export type EntityResponseType = HttpResponse<IInfoplus>;
export type EntityArrayResponseType = HttpResponse<IInfoplus[]>;

@Injectable({ providedIn: 'root' })
export class InfoplusService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/infopluses');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(infoplus: IInfoplus): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(infoplus);
    return this.http
      .post<IInfoplus>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(infoplus: IInfoplus): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(infoplus);
    return this.http
      .put<IInfoplus>(`${this.resourceUrl}/${getInfoplusIdentifier(infoplus) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(infoplus: IInfoplus): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(infoplus);
    return this.http
      .patch<IInfoplus>(`${this.resourceUrl}/${getInfoplusIdentifier(infoplus) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IInfoplus>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IInfoplus[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addInfoplusToCollectionIfMissing(infoplusCollection: IInfoplus[], ...infoplusesToCheck: (IInfoplus | null | undefined)[]): IInfoplus[] {
    const infopluses: IInfoplus[] = infoplusesToCheck.filter(isPresent);
    if (infopluses.length > 0) {
      const infoplusCollectionIdentifiers = infoplusCollection.map(infoplusItem => getInfoplusIdentifier(infoplusItem)!);
      const infoplusesToAdd = infopluses.filter(infoplusItem => {
        const infoplusIdentifier = getInfoplusIdentifier(infoplusItem);
        if (infoplusIdentifier == null || infoplusCollectionIdentifiers.includes(infoplusIdentifier)) {
          return false;
        }
        infoplusCollectionIdentifiers.push(infoplusIdentifier);
        return true;
      });
      return [...infoplusesToAdd, ...infoplusCollection];
    }
    return infoplusCollection;
  }

  protected convertDateFromClient(infoplus: IInfoplus): IInfoplus {
    return Object.assign({}, infoplus, {
      dateNaissance: infoplus.dateNaissance?.isValid() ? infoplus.dateNaissance.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateNaissance = res.body.dateNaissance ? dayjs(res.body.dateNaissance) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((infoplus: IInfoplus) => {
        infoplus.dateNaissance = infoplus.dateNaissance ? dayjs(infoplus.dateNaissance) : undefined;
      });
    }
    return res;
  }
}
