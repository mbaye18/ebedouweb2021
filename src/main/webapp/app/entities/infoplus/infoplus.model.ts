import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { INationnalite } from 'app/entities/nationnalite/nationnalite.model';
import { IZoneExploitation } from 'app/entities/zone-exploitation/zone-exploitation.model';

export interface IInfoplus {
  id?: number;
  numeroTelephone?: string;
  pieceIdentiteContentType?: string | null;
  pieceIdentite?: string | null;
  copieAgrementContentType?: string | null;
  copieAgrement?: string | null;
  dateNaissance?: dayjs.Dayjs;
  lieuNaissance?: string;
  genre?: string;
  user?: IUser;
  nationnalite?: INationnalite;
  zoneExploitation?: IZoneExploitation | null;
}

export class Infoplus implements IInfoplus {
  constructor(
    public id?: number,
    public numeroTelephone?: string,
    public pieceIdentiteContentType?: string | null,
    public pieceIdentite?: string | null,
    public copieAgrementContentType?: string | null,
    public copieAgrement?: string | null,
    public dateNaissance?: dayjs.Dayjs,
    public lieuNaissance?: string,
    public genre?: string,
    public user?: IUser,
    public nationnalite?: INationnalite,
    public zoneExploitation?: IZoneExploitation | null
  ) {}
}

export function getInfoplusIdentifier(infoplus: IInfoplus): number | undefined {
  return infoplus.id;
}
