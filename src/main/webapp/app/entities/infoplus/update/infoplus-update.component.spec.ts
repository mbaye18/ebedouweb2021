jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { InfoplusService } from '../service/infoplus.service';
import { IInfoplus, Infoplus } from '../infoplus.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { INationnalite } from 'app/entities/nationnalite/nationnalite.model';
import { NationnaliteService } from 'app/entities/nationnalite/service/nationnalite.service';
import { IZoneExploitation } from 'app/entities/zone-exploitation/zone-exploitation.model';
import { ZoneExploitationService } from 'app/entities/zone-exploitation/service/zone-exploitation.service';

import { InfoplusUpdateComponent } from './infoplus-update.component';

describe('Infoplus Management Update Component', () => {
  let comp: InfoplusUpdateComponent;
  let fixture: ComponentFixture<InfoplusUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let infoplusService: InfoplusService;
  let userService: UserService;
  let nationnaliteService: NationnaliteService;
  let zoneExploitationService: ZoneExploitationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [InfoplusUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(InfoplusUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(InfoplusUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    infoplusService = TestBed.inject(InfoplusService);
    userService = TestBed.inject(UserService);
    nationnaliteService = TestBed.inject(NationnaliteService);
    zoneExploitationService = TestBed.inject(ZoneExploitationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call User query and add missing value', () => {
      const infoplus: IInfoplus = { id: 456 };
      const user: IUser = { id: 4092 };
      infoplus.user = user;

      const userCollection: IUser[] = [{ id: 65414 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [user];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Nationnalite query and add missing value', () => {
      const infoplus: IInfoplus = { id: 456 };
      const nationnalite: INationnalite = { id: 2361 };
      infoplus.nationnalite = nationnalite;

      const nationnaliteCollection: INationnalite[] = [{ id: 87583 }];
      jest.spyOn(nationnaliteService, 'query').mockReturnValue(of(new HttpResponse({ body: nationnaliteCollection })));
      const additionalNationnalites = [nationnalite];
      const expectedCollection: INationnalite[] = [...additionalNationnalites, ...nationnaliteCollection];
      jest.spyOn(nationnaliteService, 'addNationnaliteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      expect(nationnaliteService.query).toHaveBeenCalled();
      expect(nationnaliteService.addNationnaliteToCollectionIfMissing).toHaveBeenCalledWith(
        nationnaliteCollection,
        ...additionalNationnalites
      );
      expect(comp.nationnalitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call ZoneExploitation query and add missing value', () => {
      const infoplus: IInfoplus = { id: 456 };
      const zoneExploitation: IZoneExploitation = { id: 18304 };
      infoplus.zoneExploitation = zoneExploitation;

      const zoneExploitationCollection: IZoneExploitation[] = [{ id: 89835 }];
      jest.spyOn(zoneExploitationService, 'query').mockReturnValue(of(new HttpResponse({ body: zoneExploitationCollection })));
      const additionalZoneExploitations = [zoneExploitation];
      const expectedCollection: IZoneExploitation[] = [...additionalZoneExploitations, ...zoneExploitationCollection];
      jest.spyOn(zoneExploitationService, 'addZoneExploitationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      expect(zoneExploitationService.query).toHaveBeenCalled();
      expect(zoneExploitationService.addZoneExploitationToCollectionIfMissing).toHaveBeenCalledWith(
        zoneExploitationCollection,
        ...additionalZoneExploitations
      );
      expect(comp.zoneExploitationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const infoplus: IInfoplus = { id: 456 };
      const user: IUser = { id: 70759 };
      infoplus.user = user;
      const nationnalite: INationnalite = { id: 46933 };
      infoplus.nationnalite = nationnalite;
      const zoneExploitation: IZoneExploitation = { id: 61974 };
      infoplus.zoneExploitation = zoneExploitation;

      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(infoplus));
      expect(comp.usersSharedCollection).toContain(user);
      expect(comp.nationnalitesSharedCollection).toContain(nationnalite);
      expect(comp.zoneExploitationsSharedCollection).toContain(zoneExploitation);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infoplus>>();
      const infoplus = { id: 123 };
      jest.spyOn(infoplusService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: infoplus }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(infoplusService.update).toHaveBeenCalledWith(infoplus);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infoplus>>();
      const infoplus = new Infoplus();
      jest.spyOn(infoplusService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: infoplus }));
      saveSubject.complete();

      // THEN
      expect(infoplusService.create).toHaveBeenCalledWith(infoplus);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Infoplus>>();
      const infoplus = { id: 123 };
      jest.spyOn(infoplusService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ infoplus });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(infoplusService.update).toHaveBeenCalledWith(infoplus);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackUserById', () => {
      it('Should return tracked User primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackNationnaliteById', () => {
      it('Should return tracked Nationnalite primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackNationnaliteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackZoneExploitationById', () => {
      it('Should return tracked ZoneExploitation primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackZoneExploitationById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
