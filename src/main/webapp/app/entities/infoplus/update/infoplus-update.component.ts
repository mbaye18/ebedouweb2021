import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IInfoplus, Infoplus } from '../infoplus.model';
import { InfoplusService } from '../service/infoplus.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { INationnalite } from 'app/entities/nationnalite/nationnalite.model';
import { NationnaliteService } from 'app/entities/nationnalite/service/nationnalite.service';
import { IZoneExploitation } from 'app/entities/zone-exploitation/zone-exploitation.model';
import { ZoneExploitationService } from 'app/entities/zone-exploitation/service/zone-exploitation.service';

@Component({
  selector: 'jhi-infoplus-update',
  templateUrl: './infoplus-update.component.html',
})
export class InfoplusUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];
  nationnalitesSharedCollection: INationnalite[] = [];
  zoneExploitationsSharedCollection: IZoneExploitation[] = [];

  editForm = this.fb.group({
    id: [],
    numeroTelephone: [null, [Validators.required]],
    pieceIdentite: [],
    pieceIdentiteContentType: [],
    copieAgrement: [],
    copieAgrementContentType: [],
    dateNaissance: [null, [Validators.required]],
    lieuNaissance: [null, [Validators.required]],
    genre: [null, [Validators.required]],
    user: [null, Validators.required],
    nationnalite: [null, Validators.required],
    zoneExploitation: [],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected infoplusService: InfoplusService,
    protected userService: UserService,
    protected nationnaliteService: NationnaliteService,
    protected zoneExploitationService: ZoneExploitationService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ infoplus }) => {
      this.updateForm(infoplus);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('ebedouApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const infoplus = this.createFromForm();
    if (infoplus.id !== undefined) {
      this.subscribeToSaveResponse(this.infoplusService.update(infoplus));
    } else {
      this.subscribeToSaveResponse(this.infoplusService.create(infoplus));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackNationnaliteById(index: number, item: INationnalite): number {
    return item.id!;
  }

  trackZoneExploitationById(index: number, item: IZoneExploitation): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInfoplus>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(infoplus: IInfoplus): void {
    this.editForm.patchValue({
      id: infoplus.id,
      numeroTelephone: infoplus.numeroTelephone,
      pieceIdentite: infoplus.pieceIdentite,
      pieceIdentiteContentType: infoplus.pieceIdentiteContentType,
      copieAgrement: infoplus.copieAgrement,
      copieAgrementContentType: infoplus.copieAgrementContentType,
      dateNaissance: infoplus.dateNaissance,
      lieuNaissance: infoplus.lieuNaissance,
      genre: infoplus.genre,
      user: infoplus.user,
      nationnalite: infoplus.nationnalite,
      zoneExploitation: infoplus.zoneExploitation,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, infoplus.user);
    this.nationnalitesSharedCollection = this.nationnaliteService.addNationnaliteToCollectionIfMissing(
      this.nationnalitesSharedCollection,
      infoplus.nationnalite
    );
    this.zoneExploitationsSharedCollection = this.zoneExploitationService.addZoneExploitationToCollectionIfMissing(
      this.zoneExploitationsSharedCollection,
      infoplus.zoneExploitation
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.nationnaliteService
      .query()
      .pipe(map((res: HttpResponse<INationnalite[]>) => res.body ?? []))
      .pipe(
        map((nationnalites: INationnalite[]) =>
          this.nationnaliteService.addNationnaliteToCollectionIfMissing(nationnalites, this.editForm.get('nationnalite')!.value)
        )
      )
      .subscribe((nationnalites: INationnalite[]) => (this.nationnalitesSharedCollection = nationnalites));

    this.zoneExploitationService
      .query()
      .pipe(map((res: HttpResponse<IZoneExploitation[]>) => res.body ?? []))
      .pipe(
        map((zoneExploitations: IZoneExploitation[]) =>
          this.zoneExploitationService.addZoneExploitationToCollectionIfMissing(
            zoneExploitations,
            this.editForm.get('zoneExploitation')!.value
          )
        )
      )
      .subscribe((zoneExploitations: IZoneExploitation[]) => (this.zoneExploitationsSharedCollection = zoneExploitations));
  }

  protected createFromForm(): IInfoplus {
    return {
      ...new Infoplus(),
      id: this.editForm.get(['id'])!.value,
      numeroTelephone: this.editForm.get(['numeroTelephone'])!.value,
      pieceIdentiteContentType: this.editForm.get(['pieceIdentiteContentType'])!.value,
      pieceIdentite: this.editForm.get(['pieceIdentite'])!.value,
      copieAgrementContentType: this.editForm.get(['copieAgrementContentType'])!.value,
      copieAgrement: this.editForm.get(['copieAgrement'])!.value,
      dateNaissance: this.editForm.get(['dateNaissance'])!.value,
      lieuNaissance: this.editForm.get(['lieuNaissance'])!.value,
      genre: this.editForm.get(['genre'])!.value,
      user: this.editForm.get(['user'])!.value,
      nationnalite: this.editForm.get(['nationnalite'])!.value,
      zoneExploitation: this.editForm.get(['zoneExploitation'])!.value,
    };
  }
}
