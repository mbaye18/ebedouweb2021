import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuantiteProduit } from '../quantite-produit.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-quantite-produit-detail',
  templateUrl: './quantite-produit-detail.component.html',
})
export class QuantiteProduitDetailComponent implements OnInit {
  quantiteProduit: IQuantiteProduit | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ quantiteProduit }) => {
      this.quantiteProduit = quantiteProduit;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
