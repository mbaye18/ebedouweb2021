import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IQuantiteProduit } from '../quantite-produit.model';
import { QuantiteProduitService } from '../service/quantite-produit.service';

@Component({
  templateUrl: './quantite-produit-delete-dialog.component.html',
})
export class QuantiteProduitDeleteDialogComponent {
  quantiteProduit?: IQuantiteProduit;

  constructor(protected quantiteProduitService: QuantiteProduitService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.quantiteProduitService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
