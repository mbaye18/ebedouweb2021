import * as dayjs from 'dayjs';
import { IPrixproduit } from 'app/entities/prixproduit/prixproduit.model';
import { IUser } from 'app/entities/user/user.model';

export interface IQuantiteProduit {
  id?: number;
  quantite?: number;
  dateAjout?: dayjs.Dayjs;
  dateModification?: dayjs.Dayjs | null;
  description?: string | null;
  quantiteRestant?: number | null;
  prixproduit?: IPrixproduit;
  user?: IUser;
}

export class QuantiteProduit implements IQuantiteProduit {
  constructor(
    public id?: number,
    public quantite?: number,
    public dateAjout?: dayjs.Dayjs,
    public dateModification?: dayjs.Dayjs | null,
    public description?: string | null,
    public quantiteRestant?: number | null,
    public prixproduit?: IPrixproduit,
    public user?: IUser
  ) {}
}

export function getQuantiteProduitIdentifier(quantiteProduit: IQuantiteProduit): number | undefined {
  return quantiteProduit.id;
}
