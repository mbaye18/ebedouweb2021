import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IQuantiteProduit, QuantiteProduit } from '../quantite-produit.model';

import { QuantiteProduitService } from './quantite-produit.service';

describe('QuantiteProduit Service', () => {
  let service: QuantiteProduitService;
  let httpMock: HttpTestingController;
  let elemDefault: IQuantiteProduit;
  let expectedResult: IQuantiteProduit | IQuantiteProduit[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(QuantiteProduitService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      quantite: 0,
      dateAjout: currentDate,
      dateModification: currentDate,
      description: 'AAAAAAA',
      quantiteRestant: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a QuantiteProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.create(new QuantiteProduit()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a QuantiteProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          quantite: 1,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
          description: 'BBBBBB',
          quantiteRestant: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a QuantiteProduit', () => {
      const patchObject = Object.assign(
        {
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        new QuantiteProduit()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of QuantiteProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          quantite: 1,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
          description: 'BBBBBB',
          quantiteRestant: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a QuantiteProduit', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addQuantiteProduitToCollectionIfMissing', () => {
      it('should add a QuantiteProduit to an empty array', () => {
        const quantiteProduit: IQuantiteProduit = { id: 123 };
        expectedResult = service.addQuantiteProduitToCollectionIfMissing([], quantiteProduit);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(quantiteProduit);
      });

      it('should not add a QuantiteProduit to an array that contains it', () => {
        const quantiteProduit: IQuantiteProduit = { id: 123 };
        const quantiteProduitCollection: IQuantiteProduit[] = [
          {
            ...quantiteProduit,
          },
          { id: 456 },
        ];
        expectedResult = service.addQuantiteProduitToCollectionIfMissing(quantiteProduitCollection, quantiteProduit);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a QuantiteProduit to an array that doesn't contain it", () => {
        const quantiteProduit: IQuantiteProduit = { id: 123 };
        const quantiteProduitCollection: IQuantiteProduit[] = [{ id: 456 }];
        expectedResult = service.addQuantiteProduitToCollectionIfMissing(quantiteProduitCollection, quantiteProduit);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(quantiteProduit);
      });

      it('should add only unique QuantiteProduit to an array', () => {
        const quantiteProduitArray: IQuantiteProduit[] = [{ id: 123 }, { id: 456 }, { id: 84018 }];
        const quantiteProduitCollection: IQuantiteProduit[] = [{ id: 123 }];
        expectedResult = service.addQuantiteProduitToCollectionIfMissing(quantiteProduitCollection, ...quantiteProduitArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const quantiteProduit: IQuantiteProduit = { id: 123 };
        const quantiteProduit2: IQuantiteProduit = { id: 456 };
        expectedResult = service.addQuantiteProduitToCollectionIfMissing([], quantiteProduit, quantiteProduit2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(quantiteProduit);
        expect(expectedResult).toContain(quantiteProduit2);
      });

      it('should accept null and undefined values', () => {
        const quantiteProduit: IQuantiteProduit = { id: 123 };
        expectedResult = service.addQuantiteProduitToCollectionIfMissing([], null, quantiteProduit, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(quantiteProduit);
      });

      it('should return initial array if no QuantiteProduit is added', () => {
        const quantiteProduitCollection: IQuantiteProduit[] = [{ id: 123 }];
        expectedResult = service.addQuantiteProduitToCollectionIfMissing(quantiteProduitCollection, undefined, null);
        expect(expectedResult).toEqual(quantiteProduitCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
