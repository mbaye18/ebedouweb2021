import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IQuantiteProduit, getQuantiteProduitIdentifier } from '../quantite-produit.model';

export type EntityResponseType = HttpResponse<IQuantiteProduit>;
export type EntityArrayResponseType = HttpResponse<IQuantiteProduit[]>;

@Injectable({ providedIn: 'root' })
export class QuantiteProduitService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/quantite-produits');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(quantiteProduit: IQuantiteProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(quantiteProduit);
    return this.http
      .post<IQuantiteProduit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(quantiteProduit: IQuantiteProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(quantiteProduit);
    return this.http
      .put<IQuantiteProduit>(`${this.resourceUrl}/${getQuantiteProduitIdentifier(quantiteProduit) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(quantiteProduit: IQuantiteProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(quantiteProduit);
    return this.http
      .patch<IQuantiteProduit>(`${this.resourceUrl}/${getQuantiteProduitIdentifier(quantiteProduit) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IQuantiteProduit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IQuantiteProduit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addQuantiteProduitToCollectionIfMissing(
    quantiteProduitCollection: IQuantiteProduit[],
    ...quantiteProduitsToCheck: (IQuantiteProduit | null | undefined)[]
  ): IQuantiteProduit[] {
    const quantiteProduits: IQuantiteProduit[] = quantiteProduitsToCheck.filter(isPresent);
    if (quantiteProduits.length > 0) {
      const quantiteProduitCollectionIdentifiers = quantiteProduitCollection.map(
        quantiteProduitItem => getQuantiteProduitIdentifier(quantiteProduitItem)!
      );
      const quantiteProduitsToAdd = quantiteProduits.filter(quantiteProduitItem => {
        const quantiteProduitIdentifier = getQuantiteProduitIdentifier(quantiteProduitItem);
        if (quantiteProduitIdentifier == null || quantiteProduitCollectionIdentifiers.includes(quantiteProduitIdentifier)) {
          return false;
        }
        quantiteProduitCollectionIdentifiers.push(quantiteProduitIdentifier);
        return true;
      });
      return [...quantiteProduitsToAdd, ...quantiteProduitCollection];
    }
    return quantiteProduitCollection;
  }

  protected convertDateFromClient(quantiteProduit: IQuantiteProduit): IQuantiteProduit {
    return Object.assign({}, quantiteProduit, {
      dateAjout: quantiteProduit.dateAjout?.isValid() ? quantiteProduit.dateAjout.toJSON() : undefined,
      dateModification: quantiteProduit.dateModification?.isValid() ? quantiteProduit.dateModification.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateAjout = res.body.dateAjout ? dayjs(res.body.dateAjout) : undefined;
      res.body.dateModification = res.body.dateModification ? dayjs(res.body.dateModification) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((quantiteProduit: IQuantiteProduit) => {
        quantiteProduit.dateAjout = quantiteProduit.dateAjout ? dayjs(quantiteProduit.dateAjout) : undefined;
        quantiteProduit.dateModification = quantiteProduit.dateModification ? dayjs(quantiteProduit.dateModification) : undefined;
      });
    }
    return res;
  }
}
