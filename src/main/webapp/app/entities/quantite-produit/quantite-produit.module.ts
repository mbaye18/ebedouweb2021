import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { QuantiteProduitComponent } from './list/quantite-produit.component';
import { QuantiteProduitDetailComponent } from './detail/quantite-produit-detail.component';
import { QuantiteProduitUpdateComponent } from './update/quantite-produit-update.component';
import { QuantiteProduitDeleteDialogComponent } from './delete/quantite-produit-delete-dialog.component';
import { QuantiteProduitRoutingModule } from './route/quantite-produit-routing.module';

@NgModule({
  imports: [SharedModule, QuantiteProduitRoutingModule],
  declarations: [
    QuantiteProduitComponent,
    QuantiteProduitDetailComponent,
    QuantiteProduitUpdateComponent,
    QuantiteProduitDeleteDialogComponent,
  ],
  entryComponents: [QuantiteProduitDeleteDialogComponent, QuantiteProduitUpdateComponent],
})
export class QuantiteProduitModule {}
