import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IQuantiteProduit, QuantiteProduit } from '../quantite-produit.model';
import { QuantiteProduitService } from '../service/quantite-produit.service';

@Injectable({ providedIn: 'root' })
export class QuantiteProduitRoutingResolveService implements Resolve<IQuantiteProduit> {
  constructor(protected service: QuantiteProduitService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IQuantiteProduit> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((quantiteProduit: HttpResponse<QuantiteProduit>) => {
          if (quantiteProduit.body) {
            return of(quantiteProduit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new QuantiteProduit());
  }
}
