jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IQuantiteProduit, QuantiteProduit } from '../quantite-produit.model';
import { QuantiteProduitService } from '../service/quantite-produit.service';

import { QuantiteProduitRoutingResolveService } from './quantite-produit-routing-resolve.service';

describe('QuantiteProduit routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: QuantiteProduitRoutingResolveService;
  let service: QuantiteProduitService;
  let resultQuantiteProduit: IQuantiteProduit | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(QuantiteProduitRoutingResolveService);
    service = TestBed.inject(QuantiteProduitService);
    resultQuantiteProduit = undefined;
  });

  describe('resolve', () => {
    it('should return IQuantiteProduit returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultQuantiteProduit = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultQuantiteProduit).toEqual({ id: 123 });
    });

    it('should return new IQuantiteProduit if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultQuantiteProduit = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultQuantiteProduit).toEqual(new QuantiteProduit());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as QuantiteProduit })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultQuantiteProduit = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultQuantiteProduit).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
