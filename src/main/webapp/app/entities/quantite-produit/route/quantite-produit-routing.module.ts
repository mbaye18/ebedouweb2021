import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { QuantiteProduitComponent } from '../list/quantite-produit.component';
import { QuantiteProduitDetailComponent } from '../detail/quantite-produit-detail.component';
import { QuantiteProduitUpdateComponent } from '../update/quantite-produit-update.component';
import { QuantiteProduitRoutingResolveService } from './quantite-produit-routing-resolve.service';

const quantiteProduitRoute: Routes = [
  {
    path: '',
    component: QuantiteProduitComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: QuantiteProduitDetailComponent,
    resolve: {
      quantiteProduit: QuantiteProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: QuantiteProduitUpdateComponent,
    resolve: {
      quantiteProduit: QuantiteProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: QuantiteProduitUpdateComponent,
    resolve: {
      quantiteProduit: QuantiteProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(quantiteProduitRoute)],
  exports: [RouterModule],
})
export class QuantiteProduitRoutingModule {}
