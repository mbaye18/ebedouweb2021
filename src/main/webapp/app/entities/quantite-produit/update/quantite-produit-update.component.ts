import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IQuantiteProduit, QuantiteProduit } from '../quantite-produit.model';
import { QuantiteProduitService } from '../service/quantite-produit.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IPrixproduit } from 'app/entities/prixproduit/prixproduit.model';
import { PrixproduitService } from 'app/entities/prixproduit/service/prixproduit.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { takeUntil } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'jhi-quantite-produit-update',
  templateUrl: './quantite-produit-update.component.html',
})
export class QuantiteProduitUpdateComponent implements OnInit {
  isSaving = false;

  prixproduitsSharedCollection: IPrixproduit[] = [];
  usersSharedCollection: IUser[] = [];
  account: Account | null = null;

  private readonly destroy$ = new Subject<void>();
  editForm = this.fb.group({
    id: [],
    quantite: [null, [Validators.required]],
    description: [],
    prixproduit: [null, Validators.required],
  });
  quantiteProduit: QuantiteProduit | null = null;
  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected quantiteProduitService: QuantiteProduitService,
    protected prixproduitService: PrixproduitService,
    protected accountService: AccountService,
    protected activeModal: NgbActiveModal,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.quantiteProduit!.id === undefined) {
      this.updateForm(this.quantiteProduit!);
    }
    this.accountService
      .getAuthenticationState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(account => (this.account = account));

    this.loadRelationshipsOptions();
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('ebedouApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  previousState(): void {
    this.activeModal.close(this.quantiteProduit);
  }

  save(): void {
    this.isSaving = true;
    const quantiteProduit = this.createFromForm();
    if (quantiteProduit.id !== undefined) {
      this.subscribeToSaveResponse(this.quantiteProduitService.update(quantiteProduit));
    } else {
      this.subscribeToSaveResponse(this.quantiteProduitService.create(quantiteProduit));
    }
  }

  trackPrixproduitById(index: number, item: IPrixproduit): number {
    return item.id!;
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuantiteProduit>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(quantiteProduit: IQuantiteProduit): void {
    this.editForm.patchValue({
      id: quantiteProduit.id,
      quantite: quantiteProduit.quantite,
      description: quantiteProduit.description,
      prixproduit: quantiteProduit.prixproduit,
    });

    this.prixproduitsSharedCollection = this.prixproduitService.addPrixproduitToCollectionIfMissing(
      this.prixproduitsSharedCollection,
      quantiteProduit.prixproduit
    );
  }
  protected loadRelationshipsOptions(): void {
    this.prixproduitService
      .query()
      .pipe(map((res: HttpResponse<IPrixproduit[]>) => res.body ?? []))
      .pipe(
        map((prixproduits: IPrixproduit[]) =>
          this.prixproduitService.addPrixproduitToCollectionIfMissing(prixproduits, this.editForm.get('prixproduit')!.value)
        )
      )
      .subscribe((prixproduits: IPrixproduit[]) => (this.prixproduitsSharedCollection = prixproduits));
  }

  protected createFromForm(): IQuantiteProduit {
    return {
      ...new QuantiteProduit(),
      id: this.editForm.get(['id'])!.value,
      quantite: this.editForm.get(['quantite'])!.value,
      dateAjout: this.quantiteProduit!.dateAjout ? this.quantiteProduit?.dateAjout : undefined,
      dateModification: this.quantiteProduit!.dateModification ? this.quantiteProduit?.dateModification : undefined,
      description: this.editForm.get(['description'])!.value,
      quantiteRestant: this.quantiteProduit?.quantiteRestant,
      prixproduit: this.editForm.get(['prixproduit'])!.value,
      user: this.account!,
    };
  }
}
