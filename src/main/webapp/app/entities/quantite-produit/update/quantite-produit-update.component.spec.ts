jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { QuantiteProduitService } from '../service/quantite-produit.service';
import { IQuantiteProduit, QuantiteProduit } from '../quantite-produit.model';
import { IPrixproduit } from 'app/entities/prixproduit/prixproduit.model';
import { PrixproduitService } from 'app/entities/prixproduit/service/prixproduit.service';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { QuantiteProduitUpdateComponent } from './quantite-produit-update.component';

describe('QuantiteProduit Management Update Component', () => {
  let comp: QuantiteProduitUpdateComponent;
  let fixture: ComponentFixture<QuantiteProduitUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let quantiteProduitService: QuantiteProduitService;
  let prixproduitService: PrixproduitService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [QuantiteProduitUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(QuantiteProduitUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(QuantiteProduitUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    quantiteProduitService = TestBed.inject(QuantiteProduitService);
    prixproduitService = TestBed.inject(PrixproduitService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Prixproduit query and add missing value', () => {
      const quantiteProduit: IQuantiteProduit = { id: 456 };
      const prixproduit: IPrixproduit = { id: 67615 };
      quantiteProduit.prixproduit = prixproduit;

      const prixproduitCollection: IPrixproduit[] = [{ id: 81015 }];
      jest.spyOn(prixproduitService, 'query').mockReturnValue(of(new HttpResponse({ body: prixproduitCollection })));
      const additionalPrixproduits = [prixproduit];
      const expectedCollection: IPrixproduit[] = [...additionalPrixproduits, ...prixproduitCollection];
      jest.spyOn(prixproduitService, 'addPrixproduitToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      expect(prixproduitService.query).toHaveBeenCalled();
      expect(prixproduitService.addPrixproduitToCollectionIfMissing).toHaveBeenCalledWith(prixproduitCollection, ...additionalPrixproduits);
      expect(comp.prixproduitsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call User query and add missing value', () => {
      const quantiteProduit: IQuantiteProduit = { id: 456 };
      const user: IUser = { id: 7101 };
      quantiteProduit.user = user;

      const userCollection: IUser[] = [{ id: 84776 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [user];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const quantiteProduit: IQuantiteProduit = { id: 456 };
      const prixproduit: IPrixproduit = { id: 29067 };
      quantiteProduit.prixproduit = prixproduit;
      const user: IUser = { id: 69006 };
      quantiteProduit.user = user;

      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(quantiteProduit));
      expect(comp.prixproduitsSharedCollection).toContain(prixproduit);
      expect(comp.usersSharedCollection).toContain(user);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<QuantiteProduit>>();
      const quantiteProduit = { id: 123 };
      jest.spyOn(quantiteProduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: quantiteProduit }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(quantiteProduitService.update).toHaveBeenCalledWith(quantiteProduit);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<QuantiteProduit>>();
      const quantiteProduit = new QuantiteProduit();
      jest.spyOn(quantiteProduitService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: quantiteProduit }));
      saveSubject.complete();

      // THEN
      expect(quantiteProduitService.create).toHaveBeenCalledWith(quantiteProduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<QuantiteProduit>>();
      const quantiteProduit = { id: 123 };
      jest.spyOn(quantiteProduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ quantiteProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(quantiteProduitService.update).toHaveBeenCalledWith(quantiteProduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPrixproduitById', () => {
      it('Should return tracked Prixproduit primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPrixproduitById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackUserById', () => {
      it('Should return tracked User primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
