import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INationnalite, getNationnaliteIdentifier } from '../nationnalite.model';

export type EntityResponseType = HttpResponse<INationnalite>;
export type EntityArrayResponseType = HttpResponse<INationnalite[]>;

@Injectable({ providedIn: 'root' })
export class NationnaliteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nationnalites');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(nationnalite: INationnalite): Observable<EntityResponseType> {
    return this.http.post<INationnalite>(this.resourceUrl, nationnalite, { observe: 'response' });
  }

  update(nationnalite: INationnalite): Observable<EntityResponseType> {
    return this.http.put<INationnalite>(`${this.resourceUrl}/${getNationnaliteIdentifier(nationnalite) as number}`, nationnalite, {
      observe: 'response',
    });
  }

  partialUpdate(nationnalite: INationnalite): Observable<EntityResponseType> {
    return this.http.patch<INationnalite>(`${this.resourceUrl}/${getNationnaliteIdentifier(nationnalite) as number}`, nationnalite, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INationnalite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INationnalite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addNationnaliteToCollectionIfMissing(
    nationnaliteCollection: INationnalite[],
    ...nationnalitesToCheck: (INationnalite | null | undefined)[]
  ): INationnalite[] {
    const nationnalites: INationnalite[] = nationnalitesToCheck.filter(isPresent);
    if (nationnalites.length > 0) {
      const nationnaliteCollectionIdentifiers = nationnaliteCollection.map(
        nationnaliteItem => getNationnaliteIdentifier(nationnaliteItem)!
      );
      const nationnalitesToAdd = nationnalites.filter(nationnaliteItem => {
        const nationnaliteIdentifier = getNationnaliteIdentifier(nationnaliteItem);
        if (nationnaliteIdentifier == null || nationnaliteCollectionIdentifiers.includes(nationnaliteIdentifier)) {
          return false;
        }
        nationnaliteCollectionIdentifiers.push(nationnaliteIdentifier);
        return true;
      });
      return [...nationnalitesToAdd, ...nationnaliteCollection];
    }
    return nationnaliteCollection;
  }
}
