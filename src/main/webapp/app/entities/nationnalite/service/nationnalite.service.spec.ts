import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INationnalite, Nationnalite } from '../nationnalite.model';

import { NationnaliteService } from './nationnalite.service';

describe('Nationnalite Service', () => {
  let service: NationnaliteService;
  let httpMock: HttpTestingController;
  let elemDefault: INationnalite;
  let expectedResult: INationnalite | INationnalite[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NationnaliteService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      libelle: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Nationnalite', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Nationnalite()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Nationnalite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Nationnalite', () => {
      const patchObject = Object.assign({}, new Nationnalite());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Nationnalite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Nationnalite', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addNationnaliteToCollectionIfMissing', () => {
      it('should add a Nationnalite to an empty array', () => {
        const nationnalite: INationnalite = { id: 123 };
        expectedResult = service.addNationnaliteToCollectionIfMissing([], nationnalite);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(nationnalite);
      });

      it('should not add a Nationnalite to an array that contains it', () => {
        const nationnalite: INationnalite = { id: 123 };
        const nationnaliteCollection: INationnalite[] = [
          {
            ...nationnalite,
          },
          { id: 456 },
        ];
        expectedResult = service.addNationnaliteToCollectionIfMissing(nationnaliteCollection, nationnalite);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Nationnalite to an array that doesn't contain it", () => {
        const nationnalite: INationnalite = { id: 123 };
        const nationnaliteCollection: INationnalite[] = [{ id: 456 }];
        expectedResult = service.addNationnaliteToCollectionIfMissing(nationnaliteCollection, nationnalite);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(nationnalite);
      });

      it('should add only unique Nationnalite to an array', () => {
        const nationnaliteArray: INationnalite[] = [{ id: 123 }, { id: 456 }, { id: 61012 }];
        const nationnaliteCollection: INationnalite[] = [{ id: 123 }];
        expectedResult = service.addNationnaliteToCollectionIfMissing(nationnaliteCollection, ...nationnaliteArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const nationnalite: INationnalite = { id: 123 };
        const nationnalite2: INationnalite = { id: 456 };
        expectedResult = service.addNationnaliteToCollectionIfMissing([], nationnalite, nationnalite2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(nationnalite);
        expect(expectedResult).toContain(nationnalite2);
      });

      it('should accept null and undefined values', () => {
        const nationnalite: INationnalite = { id: 123 };
        expectedResult = service.addNationnaliteToCollectionIfMissing([], null, nationnalite, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(nationnalite);
      });

      it('should return initial array if no Nationnalite is added', () => {
        const nationnaliteCollection: INationnalite[] = [{ id: 123 }];
        expectedResult = service.addNationnaliteToCollectionIfMissing(nationnaliteCollection, undefined, null);
        expect(expectedResult).toEqual(nationnaliteCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
