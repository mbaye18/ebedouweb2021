import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NationnaliteComponent } from './list/nationnalite.component';
import { NationnaliteDetailComponent } from './detail/nationnalite-detail.component';
import { NationnaliteUpdateComponent } from './update/nationnalite-update.component';
import { NationnaliteDeleteDialogComponent } from './delete/nationnalite-delete-dialog.component';
import { NationnaliteRoutingModule } from './route/nationnalite-routing.module';

@NgModule({
  imports: [SharedModule, NationnaliteRoutingModule],
  declarations: [NationnaliteComponent, NationnaliteDetailComponent, NationnaliteUpdateComponent, NationnaliteDeleteDialogComponent],
  entryComponents: [NationnaliteDeleteDialogComponent],
})
export class NationnaliteModule {}
