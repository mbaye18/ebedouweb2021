import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INationnalite } from '../nationnalite.model';
import { NationnaliteService } from '../service/nationnalite.service';

@Component({
  templateUrl: './nationnalite-delete-dialog.component.html',
})
export class NationnaliteDeleteDialogComponent {
  nationnalite?: INationnalite;

  constructor(protected nationnaliteService: NationnaliteService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.nationnaliteService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
