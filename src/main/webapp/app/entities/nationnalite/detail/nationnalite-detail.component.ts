import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INationnalite } from '../nationnalite.model';

@Component({
  selector: 'jhi-nationnalite-detail',
  templateUrl: './nationnalite-detail.component.html',
})
export class NationnaliteDetailComponent implements OnInit {
  nationnalite: INationnalite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nationnalite }) => {
      this.nationnalite = nationnalite;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
