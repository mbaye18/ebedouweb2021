import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NationnaliteDetailComponent } from './nationnalite-detail.component';

describe('Nationnalite Management Detail Component', () => {
  let comp: NationnaliteDetailComponent;
  let fixture: ComponentFixture<NationnaliteDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NationnaliteDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ nationnalite: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NationnaliteDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NationnaliteDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load nationnalite on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.nationnalite).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
