import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { INationnalite, Nationnalite } from '../nationnalite.model';
import { NationnaliteService } from '../service/nationnalite.service';

@Component({
  selector: 'jhi-nationnalite-update',
  templateUrl: './nationnalite-update.component.html',
})
export class NationnaliteUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
  });

  constructor(protected nationnaliteService: NationnaliteService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nationnalite }) => {
      this.updateForm(nationnalite);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const nationnalite = this.createFromForm();
    if (nationnalite.id !== undefined) {
      this.subscribeToSaveResponse(this.nationnaliteService.update(nationnalite));
    } else {
      this.subscribeToSaveResponse(this.nationnaliteService.create(nationnalite));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INationnalite>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(nationnalite: INationnalite): void {
    this.editForm.patchValue({
      id: nationnalite.id,
      libelle: nationnalite.libelle,
    });
  }

  protected createFromForm(): INationnalite {
    return {
      ...new Nationnalite(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }
}
