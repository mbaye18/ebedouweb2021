jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { NationnaliteService } from '../service/nationnalite.service';
import { INationnalite, Nationnalite } from '../nationnalite.model';

import { NationnaliteUpdateComponent } from './nationnalite-update.component';

describe('Nationnalite Management Update Component', () => {
  let comp: NationnaliteUpdateComponent;
  let fixture: ComponentFixture<NationnaliteUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let nationnaliteService: NationnaliteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [NationnaliteUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(NationnaliteUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NationnaliteUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    nationnaliteService = TestBed.inject(NationnaliteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const nationnalite: INationnalite = { id: 456 };

      activatedRoute.data = of({ nationnalite });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(nationnalite));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Nationnalite>>();
      const nationnalite = { id: 123 };
      jest.spyOn(nationnaliteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ nationnalite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: nationnalite }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(nationnaliteService.update).toHaveBeenCalledWith(nationnalite);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Nationnalite>>();
      const nationnalite = new Nationnalite();
      jest.spyOn(nationnaliteService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ nationnalite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: nationnalite }));
      saveSubject.complete();

      // THEN
      expect(nationnaliteService.create).toHaveBeenCalledWith(nationnalite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Nationnalite>>();
      const nationnalite = { id: 123 };
      jest.spyOn(nationnaliteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ nationnalite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(nationnaliteService.update).toHaveBeenCalledWith(nationnalite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
