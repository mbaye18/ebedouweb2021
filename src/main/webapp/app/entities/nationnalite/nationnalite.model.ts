export interface INationnalite {
  id?: number;
  libelle?: string;
}

export class Nationnalite implements INationnalite {
  constructor(public id?: number, public libelle?: string) {}
}

export function getNationnaliteIdentifier(nationnalite: INationnalite): number | undefined {
  return nationnalite.id;
}
