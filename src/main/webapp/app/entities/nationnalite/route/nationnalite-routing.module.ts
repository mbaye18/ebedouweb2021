import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NationnaliteComponent } from '../list/nationnalite.component';
import { NationnaliteDetailComponent } from '../detail/nationnalite-detail.component';
import { NationnaliteUpdateComponent } from '../update/nationnalite-update.component';
import { NationnaliteRoutingResolveService } from './nationnalite-routing-resolve.service';

const nationnaliteRoute: Routes = [
  {
    path: '',
    component: NationnaliteComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NationnaliteDetailComponent,
    resolve: {
      nationnalite: NationnaliteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NationnaliteUpdateComponent,
    resolve: {
      nationnalite: NationnaliteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NationnaliteUpdateComponent,
    resolve: {
      nationnalite: NationnaliteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(nationnaliteRoute)],
  exports: [RouterModule],
})
export class NationnaliteRoutingModule {}
