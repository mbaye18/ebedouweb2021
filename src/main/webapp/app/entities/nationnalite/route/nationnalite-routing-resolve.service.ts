import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INationnalite, Nationnalite } from '../nationnalite.model';
import { NationnaliteService } from '../service/nationnalite.service';

@Injectable({ providedIn: 'root' })
export class NationnaliteRoutingResolveService implements Resolve<INationnalite> {
  constructor(protected service: NationnaliteService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INationnalite> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((nationnalite: HttpResponse<Nationnalite>) => {
          if (nationnalite.body) {
            return of(nationnalite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Nationnalite());
  }
}
