export interface IDepartement {
  id?: number;
  imageContentType?: string | null;
  image?: string | null;
}

export class Departement implements IDepartement {
  constructor(public id?: number, public imageContentType?: string | null, public image?: string | null) {}
}

export function getDepartementIdentifier(departement: IDepartement): number | undefined {
  return departement.id;
}
