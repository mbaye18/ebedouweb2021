import * as dayjs from 'dayjs';

export interface IProduit {
  id?: number;
  libelle?: string;
  dateAjout?: dayjs.Dayjs;
  dateModification?: dayjs.Dayjs;
}

export class Produit implements IProduit {
  constructor(public id?: number, public libelle?: string, public dateAjout?: dayjs.Dayjs, public dateModification?: dayjs.Dayjs) {}
}

export function getProduitIdentifier(produit: IProduit): number | undefined {
  return produit.id;
}
