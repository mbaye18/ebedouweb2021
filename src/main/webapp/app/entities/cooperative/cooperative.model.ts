import * as dayjs from 'dayjs';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';

export interface ICooperative {
  id?: number;
  libelle?: string;
  dateCreation?: dayjs.Dayjs;
  dateModification?: dayjs.Dayjs;
  etat?: boolean;
  gerant?: IInfoplus;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public libelle?: string,
    public dateCreation?: dayjs.Dayjs,
    public dateModification?: dayjs.Dayjs,
    public etat?: boolean,
    public gerant?: IInfoplus
  ) {
    this.etat = this.etat ?? false;
  }
}

export function getCooperativeIdentifier(cooperative: ICooperative): number | undefined {
  return cooperative.id;
}
