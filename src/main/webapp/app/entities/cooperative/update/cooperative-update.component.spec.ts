jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CooperativeService } from '../service/cooperative.service';
import { ICooperative, Cooperative } from '../cooperative.model';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { InfoplusService } from 'app/entities/infoplus/service/infoplus.service';

import { CooperativeUpdateComponent } from './cooperative-update.component';

describe('Cooperative Management Update Component', () => {
  let comp: CooperativeUpdateComponent;
  let fixture: ComponentFixture<CooperativeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let cooperativeService: CooperativeService;
  let infoplusService: InfoplusService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CooperativeUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CooperativeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CooperativeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    cooperativeService = TestBed.inject(CooperativeService);
    infoplusService = TestBed.inject(InfoplusService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Infoplus query and add missing value', () => {
      const cooperative: ICooperative = { id: 456 };
      const gerant: IInfoplus = { id: 6253 };
      cooperative.gerant = gerant;

      const infoplusCollection: IInfoplus[] = [{ id: 67473 }];
      jest.spyOn(infoplusService, 'query').mockReturnValue(of(new HttpResponse({ body: infoplusCollection })));
      const additionalInfopluses = [gerant];
      const expectedCollection: IInfoplus[] = [...additionalInfopluses, ...infoplusCollection];
      jest.spyOn(infoplusService, 'addInfoplusToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ cooperative });
      comp.ngOnInit();

      expect(infoplusService.query).toHaveBeenCalled();
      expect(infoplusService.addInfoplusToCollectionIfMissing).toHaveBeenCalledWith(infoplusCollection, ...additionalInfopluses);
      expect(comp.infoplusesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const cooperative: ICooperative = { id: 456 };
      const gerant: IInfoplus = { id: 5183 };
      cooperative.gerant = gerant;

      activatedRoute.data = of({ cooperative });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(cooperative));
      expect(comp.infoplusesSharedCollection).toContain(gerant);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Cooperative>>();
      const cooperative = { id: 123 };
      jest.spyOn(cooperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cooperative }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(cooperativeService.update).toHaveBeenCalledWith(cooperative);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Cooperative>>();
      const cooperative = new Cooperative();
      jest.spyOn(cooperativeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cooperative }));
      saveSubject.complete();

      // THEN
      expect(cooperativeService.create).toHaveBeenCalledWith(cooperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Cooperative>>();
      const cooperative = { id: 123 };
      jest.spyOn(cooperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(cooperativeService.update).toHaveBeenCalledWith(cooperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackInfoplusById', () => {
      it('Should return tracked Infoplus primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackInfoplusById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
