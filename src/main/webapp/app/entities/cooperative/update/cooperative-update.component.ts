import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICooperative, Cooperative } from '../cooperative.model';
import { CooperativeService } from '../service/cooperative.service';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { InfoplusService } from 'app/entities/infoplus/service/infoplus.service';

@Component({
  selector: 'jhi-cooperative-update',
  templateUrl: './cooperative-update.component.html',
})
export class CooperativeUpdateComponent implements OnInit {
  isSaving = false;

  infoplusesSharedCollection: IInfoplus[] = [];

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
    dateCreation: [null, [Validators.required]],
    dateModification: [null, [Validators.required]],
    etat: [null, [Validators.required]],
    gerant: [null, Validators.required],
  });

  constructor(
    protected cooperativeService: CooperativeService,
    protected infoplusService: InfoplusService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cooperative }) => {
      if (cooperative.id === undefined) {
        const today = dayjs().startOf('day');
        cooperative.dateCreation = today;
        cooperative.dateModification = today;
      }

      this.updateForm(cooperative);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cooperative = this.createFromForm();
    if (cooperative.id !== undefined) {
      this.subscribeToSaveResponse(this.cooperativeService.update(cooperative));
    } else {
      this.subscribeToSaveResponse(this.cooperativeService.create(cooperative));
    }
  }

  trackInfoplusById(index: number, item: IInfoplus): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICooperative>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cooperative: ICooperative): void {
    this.editForm.patchValue({
      id: cooperative.id,
      libelle: cooperative.libelle,
      dateCreation: cooperative.dateCreation ? cooperative.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: cooperative.dateModification ? cooperative.dateModification.format(DATE_TIME_FORMAT) : null,
      etat: cooperative.etat,
      gerant: cooperative.gerant,
    });

    this.infoplusesSharedCollection = this.infoplusService.addInfoplusToCollectionIfMissing(
      this.infoplusesSharedCollection,
      cooperative.gerant
    );
  }

  protected loadRelationshipsOptions(): void {
    this.infoplusService
      .query()
      .pipe(map((res: HttpResponse<IInfoplus[]>) => res.body ?? []))
      .pipe(
        map((infopluses: IInfoplus[]) =>
          this.infoplusService.addInfoplusToCollectionIfMissing(infopluses, this.editForm.get('gerant')!.value)
        )
      )
      .subscribe((infopluses: IInfoplus[]) => (this.infoplusesSharedCollection = infopluses));
  }

  protected createFromForm(): ICooperative {
    return {
      ...new Cooperative(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? dayjs(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? dayjs(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      etat: this.editForm.get(['etat'])!.value,
      gerant: this.editForm.get(['gerant'])!.value,
    };
  }
}
