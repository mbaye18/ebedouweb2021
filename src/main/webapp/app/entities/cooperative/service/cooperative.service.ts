import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICooperative, getCooperativeIdentifier } from '../cooperative.model';

export type EntityResponseType = HttpResponse<ICooperative>;
export type EntityArrayResponseType = HttpResponse<ICooperative[]>;

@Injectable({ providedIn: 'root' })
export class CooperativeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/cooperatives');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(cooperative: ICooperative): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cooperative);
    return this.http
      .post<ICooperative>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(cooperative: ICooperative): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cooperative);
    return this.http
      .put<ICooperative>(`${this.resourceUrl}/${getCooperativeIdentifier(cooperative) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(cooperative: ICooperative): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cooperative);
    return this.http
      .patch<ICooperative>(`${this.resourceUrl}/${getCooperativeIdentifier(cooperative) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICooperative>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICooperative[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCooperativeToCollectionIfMissing(
    cooperativeCollection: ICooperative[],
    ...cooperativesToCheck: (ICooperative | null | undefined)[]
  ): ICooperative[] {
    const cooperatives: ICooperative[] = cooperativesToCheck.filter(isPresent);
    if (cooperatives.length > 0) {
      const cooperativeCollectionIdentifiers = cooperativeCollection.map(cooperativeItem => getCooperativeIdentifier(cooperativeItem)!);
      const cooperativesToAdd = cooperatives.filter(cooperativeItem => {
        const cooperativeIdentifier = getCooperativeIdentifier(cooperativeItem);
        if (cooperativeIdentifier == null || cooperativeCollectionIdentifiers.includes(cooperativeIdentifier)) {
          return false;
        }
        cooperativeCollectionIdentifiers.push(cooperativeIdentifier);
        return true;
      });
      return [...cooperativesToAdd, ...cooperativeCollection];
    }
    return cooperativeCollection;
  }

  protected convertDateFromClient(cooperative: ICooperative): ICooperative {
    return Object.assign({}, cooperative, {
      dateCreation: cooperative.dateCreation?.isValid() ? cooperative.dateCreation.toJSON() : undefined,
      dateModification: cooperative.dateModification?.isValid() ? cooperative.dateModification.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? dayjs(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? dayjs(res.body.dateModification) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((cooperative: ICooperative) => {
        cooperative.dateCreation = cooperative.dateCreation ? dayjs(cooperative.dateCreation) : undefined;
        cooperative.dateModification = cooperative.dateModification ? dayjs(cooperative.dateModification) : undefined;
      });
    }
    return res;
  }
}
