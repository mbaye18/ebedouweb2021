export interface IZone {
  id?: number;
  libelle?: string;
}

export class Zone implements IZone {
  constructor(public id?: number, public libelle?: string) {}
}

export function getZoneIdentifier(zone: IZone): number | undefined {
  return zone.id;
}
