import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ICompagne, Compagne } from '../compagne.model';

import { CompagneService } from './compagne.service';

describe('Compagne Service', () => {
  let service: CompagneService;
  let httpMock: HttpTestingController;
  let elemDefault: ICompagne;
  let expectedResult: ICompagne | ICompagne[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CompagneService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      libelle: 'AAAAAAA',
      dateCreation: currentDate,
      etat: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateCreation: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Compagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateCreation: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateCreation: currentDate,
        },
        returnedFromService
      );

      service.create(new Compagne()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Compagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
          dateCreation: currentDate.format(DATE_FORMAT),
          etat: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateCreation: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Compagne', () => {
      const patchObject = Object.assign(
        {
          dateCreation: currentDate.format(DATE_FORMAT),
        },
        new Compagne()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateCreation: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Compagne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
          dateCreation: currentDate.format(DATE_FORMAT),
          etat: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateCreation: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Compagne', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCompagneToCollectionIfMissing', () => {
      it('should add a Compagne to an empty array', () => {
        const compagne: ICompagne = { id: 123 };
        expectedResult = service.addCompagneToCollectionIfMissing([], compagne);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(compagne);
      });

      it('should not add a Compagne to an array that contains it', () => {
        const compagne: ICompagne = { id: 123 };
        const compagneCollection: ICompagne[] = [
          {
            ...compagne,
          },
          { id: 456 },
        ];
        expectedResult = service.addCompagneToCollectionIfMissing(compagneCollection, compagne);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Compagne to an array that doesn't contain it", () => {
        const compagne: ICompagne = { id: 123 };
        const compagneCollection: ICompagne[] = [{ id: 456 }];
        expectedResult = service.addCompagneToCollectionIfMissing(compagneCollection, compagne);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(compagne);
      });

      it('should add only unique Compagne to an array', () => {
        const compagneArray: ICompagne[] = [{ id: 123 }, { id: 456 }, { id: 76112 }];
        const compagneCollection: ICompagne[] = [{ id: 123 }];
        expectedResult = service.addCompagneToCollectionIfMissing(compagneCollection, ...compagneArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const compagne: ICompagne = { id: 123 };
        const compagne2: ICompagne = { id: 456 };
        expectedResult = service.addCompagneToCollectionIfMissing([], compagne, compagne2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(compagne);
        expect(expectedResult).toContain(compagne2);
      });

      it('should accept null and undefined values', () => {
        const compagne: ICompagne = { id: 123 };
        expectedResult = service.addCompagneToCollectionIfMissing([], null, compagne, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(compagne);
      });

      it('should return initial array if no Compagne is added', () => {
        const compagneCollection: ICompagne[] = [{ id: 123 }];
        expectedResult = service.addCompagneToCollectionIfMissing(compagneCollection, undefined, null);
        expect(expectedResult).toEqual(compagneCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
