import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICompagne, getCompagneIdentifier } from '../compagne.model';

export type EntityResponseType = HttpResponse<ICompagne>;
export type EntityArrayResponseType = HttpResponse<ICompagne[]>;

@Injectable({ providedIn: 'root' })
export class CompagneService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/compagnes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(compagne: ICompagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(compagne);
    return this.http
      .post<ICompagne>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(compagne: ICompagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(compagne);
    return this.http
      .put<ICompagne>(`${this.resourceUrl}/${getCompagneIdentifier(compagne) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(compagne: ICompagne): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(compagne);
    return this.http
      .patch<ICompagne>(`${this.resourceUrl}/${getCompagneIdentifier(compagne) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICompagne>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICompagne[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCompagneToCollectionIfMissing(compagneCollection: ICompagne[], ...compagnesToCheck: (ICompagne | null | undefined)[]): ICompagne[] {
    const compagnes: ICompagne[] = compagnesToCheck.filter(isPresent);
    if (compagnes.length > 0) {
      const compagneCollectionIdentifiers = compagneCollection.map(compagneItem => getCompagneIdentifier(compagneItem)!);
      const compagnesToAdd = compagnes.filter(compagneItem => {
        const compagneIdentifier = getCompagneIdentifier(compagneItem);
        if (compagneIdentifier == null || compagneCollectionIdentifiers.includes(compagneIdentifier)) {
          return false;
        }
        compagneCollectionIdentifiers.push(compagneIdentifier);
        return true;
      });
      return [...compagnesToAdd, ...compagneCollection];
    }
    return compagneCollection;
  }

  protected convertDateFromClient(compagne: ICompagne): ICompagne {
    return Object.assign({}, compagne, {
      dateCreation: compagne.dateCreation?.isValid() ? compagne.dateCreation.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? dayjs(res.body.dateCreation) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((compagne: ICompagne) => {
        compagne.dateCreation = compagne.dateCreation ? dayjs(compagne.dateCreation) : undefined;
      });
    }
    return res;
  }
}
