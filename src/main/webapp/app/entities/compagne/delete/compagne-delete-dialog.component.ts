import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompagne } from '../compagne.model';
import { CompagneService } from '../service/compagne.service';

@Component({
  templateUrl: './compagne-delete-dialog.component.html',
})
export class CompagneDeleteDialogComponent {
  compagne?: ICompagne;

  constructor(protected compagneService: CompagneService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.compagneService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
