import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CompagneComponent } from '../list/compagne.component';
import { CompagneDetailComponent } from '../detail/compagne-detail.component';
import { CompagneUpdateComponent } from '../update/compagne-update.component';
import { CompagneRoutingResolveService } from './compagne-routing-resolve.service';

const compagneRoute: Routes = [
  {
    path: '',
    component: CompagneComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CompagneDetailComponent,
    resolve: {
      compagne: CompagneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CompagneUpdateComponent,
    resolve: {
      compagne: CompagneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CompagneUpdateComponent,
    resolve: {
      compagne: CompagneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(compagneRoute)],
  exports: [RouterModule],
})
export class CompagneRoutingModule {}
