import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICompagne, Compagne } from '../compagne.model';
import { CompagneService } from '../service/compagne.service';

@Injectable({ providedIn: 'root' })
export class CompagneRoutingResolveService implements Resolve<ICompagne> {
  constructor(protected service: CompagneService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICompagne> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((compagne: HttpResponse<Compagne>) => {
          if (compagne.body) {
            return of(compagne.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Compagne());
  }
}
