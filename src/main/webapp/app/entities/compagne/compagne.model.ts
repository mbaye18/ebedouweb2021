import * as dayjs from 'dayjs';

export interface ICompagne {
  id?: number;
  libelle?: string;
  dateCreation?: dayjs.Dayjs;
  etat?: boolean;
}

export class Compagne implements ICompagne {
  constructor(public id?: number, public libelle?: string, public dateCreation?: dayjs.Dayjs, public etat?: boolean) {
    this.etat = this.etat ?? false;
  }
}

export function getCompagneIdentifier(compagne: ICompagne): number | undefined {
  return compagne.id;
}
