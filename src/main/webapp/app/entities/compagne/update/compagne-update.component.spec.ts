jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CompagneService } from '../service/compagne.service';
import { ICompagne, Compagne } from '../compagne.model';

import { CompagneUpdateComponent } from './compagne-update.component';

describe('Compagne Management Update Component', () => {
  let comp: CompagneUpdateComponent;
  let fixture: ComponentFixture<CompagneUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let compagneService: CompagneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CompagneUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CompagneUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompagneUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    compagneService = TestBed.inject(CompagneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const compagne: ICompagne = { id: 456 };

      activatedRoute.data = of({ compagne });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(compagne));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Compagne>>();
      const compagne = { id: 123 };
      jest.spyOn(compagneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: compagne }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(compagneService.update).toHaveBeenCalledWith(compagne);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Compagne>>();
      const compagne = new Compagne();
      jest.spyOn(compagneService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: compagne }));
      saveSubject.complete();

      // THEN
      expect(compagneService.create).toHaveBeenCalledWith(compagne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Compagne>>();
      const compagne = { id: 123 };
      jest.spyOn(compagneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compagne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(compagneService.update).toHaveBeenCalledWith(compagne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
