import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICompagne, Compagne } from '../compagne.model';
import { CompagneService } from '../service/compagne.service';

@Component({
  selector: 'jhi-compagne-update',
  templateUrl: './compagne-update.component.html',
})
export class CompagneUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
    dateCreation: [null, [Validators.required]],
    etat: [null, [Validators.required]],
  });

  constructor(protected compagneService: CompagneService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compagne }) => {
      this.updateForm(compagne);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const compagne = this.createFromForm();
    if (compagne.id !== undefined) {
      this.subscribeToSaveResponse(this.compagneService.update(compagne));
    } else {
      this.subscribeToSaveResponse(this.compagneService.create(compagne));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompagne>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(compagne: ICompagne): void {
    this.editForm.patchValue({
      id: compagne.id,
      libelle: compagne.libelle,
      dateCreation: compagne.dateCreation,
      etat: compagne.etat,
    });
  }

  protected createFromForm(): ICompagne {
    return {
      ...new Compagne(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value,
      etat: this.editForm.get(['etat'])!.value,
    };
  }
}
