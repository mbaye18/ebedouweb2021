import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CompagneComponent } from './list/compagne.component';
import { CompagneDetailComponent } from './detail/compagne-detail.component';
import { CompagneUpdateComponent } from './update/compagne-update.component';
import { CompagneDeleteDialogComponent } from './delete/compagne-delete-dialog.component';
import { CompagneRoutingModule } from './route/compagne-routing.module';

@NgModule({
  imports: [SharedModule, CompagneRoutingModule],
  declarations: [CompagneComponent, CompagneDetailComponent, CompagneUpdateComponent, CompagneDeleteDialogComponent],
  entryComponents: [CompagneDeleteDialogComponent],
})
export class CompagneModule {}
