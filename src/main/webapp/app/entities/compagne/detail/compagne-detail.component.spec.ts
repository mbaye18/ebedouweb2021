import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CompagneDetailComponent } from './compagne-detail.component';

describe('Compagne Management Detail Component', () => {
  let comp: CompagneDetailComponent;
  let fixture: ComponentFixture<CompagneDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompagneDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ compagne: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CompagneDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CompagneDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load compagne on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.compagne).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
