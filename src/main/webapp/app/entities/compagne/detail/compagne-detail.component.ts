import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompagne } from '../compagne.model';

@Component({
  selector: 'jhi-compagne-detail',
  templateUrl: './compagne-detail.component.html',
})
export class CompagneDetailComponent implements OnInit {
  compagne: ICompagne | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compagne }) => {
      this.compagne = compagne;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
