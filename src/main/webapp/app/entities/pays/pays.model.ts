export interface IPays {
  id?: number;
  libelle?: string;
}

export class Pays implements IPays {
  constructor(public id?: number, public libelle?: string) {}
}

export function getPaysIdentifier(pays: IPays): number | undefined {
  return pays.id;
}
