import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'infoplus',
        data: { pageTitle: 'ebedouApp.infoplus.home.title' },
        loadChildren: () => import('./infoplus/infoplus.module').then(m => m.InfoplusModule),
      },
      {
        path: 'nationnalite',
        data: { pageTitle: 'ebedouApp.nationnalite.home.title' },
        loadChildren: () => import('./nationnalite/nationnalite.module').then(m => m.NationnaliteModule),
      },
      {
        path: 'pays',
        data: { pageTitle: 'ebedouApp.pays.home.title' },
        loadChildren: () => import('./pays/pays.module').then(m => m.PaysModule),
      },
      {
        path: 'zone-exploitation',
        data: { pageTitle: 'ebedouApp.zoneExploitation.home.title' },
        loadChildren: () => import('./zone-exploitation/zone-exploitation.module').then(m => m.ZoneExploitationModule),
      },
      {
        path: 'cooperative',
        data: { pageTitle: 'ebedouApp.cooperative.home.title' },
        loadChildren: () => import('./cooperative/cooperative.module').then(m => m.CooperativeModule),
      },
      {
        path: 'campagne',
        data: { pageTitle: 'ebedouApp.compagne.home.title' },
        loadChildren: () => import('./compagne/compagne.module').then(m => m.CompagneModule),
      },
      {
        path: 'produit',
        data: { pageTitle: 'ebedouApp.produit.home.title' },
        loadChildren: () => import('./produit/produit.module').then(m => m.ProduitModule),
      },
      {
        path: 'prixproduit',
        data: { pageTitle: 'ebedouApp.prixproduit.home.title' },
        loadChildren: () => import('./prixproduit/prixproduit.module').then(m => m.PrixproduitModule),
      },
      {
        path: 'liste-cooperative',
        data: { pageTitle: 'ebedouApp.listeCooperative.home.title' },
        loadChildren: () => import('./liste-cooperative/liste-cooperative.module').then(m => m.ListeCooperativeModule),
      },
      {
        path: 'departement',
        data: { pageTitle: 'ebedouApp.departement.home.title' },
        loadChildren: () => import('./departement/departement.module').then(m => m.DepartementModule),
      },
      {
        path: 'demande',
        data: { pageTitle: 'ebedouApp.demande.home.title' },
        loadChildren: () => import('./demande/demande.module').then(m => m.DemandeModule),
      },
      {
        path: 'mesProduit',
        data: { pageTitle: 'ebedouApp.demande.home.title' },
        loadChildren: () => import('./quantite-produit/quantite-produit.module').then(m => m.QuantiteProduitModule),
      },
      {
        path: 'wallet',
        data: { pageTitle: 'ebedouApp.wallet.home.title' },
        loadChildren: () => import('./wallet/wallet.module').then(m => m.WalletModule),
      },
      {
        path: 'quantite-produit',
        data: { pageTitle: 'ebedouApp.quantiteProduit.home.title' },
        loadChildren: () => import('./quantite-produit/quantite-produit.module').then(m => m.QuantiteProduitModule),
      },
      {
        path: 'compagne',
        data: { pageTitle: 'ebedouApp.compagne.home.title' },
        loadChildren: () => import('./compagne/compagne.module').then(m => m.CompagneModule),
      },
      {
        path: 'zone',
        data: { pageTitle: 'ebedouApp.zone.home.title' },
        loadChildren: () => import('./zone/zone.module').then(m => m.ZoneModule),
      },
      {
        path: 'image-produit',
        data: { pageTitle: 'ebedouApp.imageProduit.home.title' },
        loadChildren: () => import('./image-produit/image-produit.module').then(m => m.ImageProduitModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
