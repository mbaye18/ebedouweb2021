import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PrixproduitComponent } from './list/prixproduit.component';
import { PrixproduitDetailComponent } from './detail/prixproduit-detail.component';
import { PrixproduitUpdateComponent } from './update/prixproduit-update.component';
import { PrixproduitDeleteDialogComponent } from './delete/prixproduit-delete-dialog.component';
import { PrixproduitRoutingModule } from './route/prixproduit-routing.module';

@NgModule({
  imports: [SharedModule, PrixproduitRoutingModule],
  declarations: [PrixproduitComponent, PrixproduitDetailComponent, PrixproduitUpdateComponent, PrixproduitDeleteDialogComponent],
  entryComponents: [PrixproduitDeleteDialogComponent],
})
export class PrixproduitModule {}
