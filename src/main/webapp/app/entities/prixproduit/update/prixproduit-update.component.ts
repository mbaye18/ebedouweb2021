import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPrixproduit, Prixproduit } from '../prixproduit.model';
import { PrixproduitService } from '../service/prixproduit.service';
import { IProduit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';
import { ICompagne } from 'app/entities/compagne/compagne.model';
import { CompagneService } from 'app/entities/compagne/service/compagne.service';

@Component({
  selector: 'jhi-prixproduit-update',
  templateUrl: './prixproduit-update.component.html',
})
export class PrixproduitUpdateComponent implements OnInit {
  isSaving = false;

  produitsCollection: IProduit[] = [];
  compagnesSharedCollection: ICompagne[] = [];

  editForm = this.fb.group({
    id: [],
    prix: [null, [Validators.required]],
    dateAjout: [null, [Validators.required]],
    dateModification: [null, [Validators.required]],
    produit: [null, Validators.required],
    compagne: [null, Validators.required],
  });

  constructor(
    protected prixproduitService: PrixproduitService,
    protected produitService: ProduitService,
    protected compagneService: CompagneService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prixproduit }) => {
      if (prixproduit.id === undefined) {
        const today = dayjs().startOf('day');
        prixproduit.dateAjout = today;
        prixproduit.dateModification = today;
      }

      this.updateForm(prixproduit);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const prixproduit = this.createFromForm();
    if (prixproduit.id !== undefined) {
      this.subscribeToSaveResponse(this.prixproduitService.update(prixproduit));
    } else {
      this.subscribeToSaveResponse(this.prixproduitService.create(prixproduit));
    }
  }

  trackProduitById(index: number, item: IProduit): number {
    return item.id!;
  }

  trackCompagneById(index: number, item: ICompagne): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPrixproduit>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(prixproduit: IPrixproduit): void {
    this.editForm.patchValue({
      id: prixproduit.id,
      prix: prixproduit.prix,
      dateAjout: prixproduit.dateAjout ? prixproduit.dateAjout.format(DATE_TIME_FORMAT) : null,
      dateModification: prixproduit.dateModification ? prixproduit.dateModification.format(DATE_TIME_FORMAT) : null,
      produit: prixproduit.produit,
      compagne: prixproduit.compagne,
    });

    this.produitsCollection = this.produitService.addProduitToCollectionIfMissing(this.produitsCollection, prixproduit.produit);
    this.compagnesSharedCollection = this.compagneService.addCompagneToCollectionIfMissing(
      this.compagnesSharedCollection,
      prixproduit.compagne
    );
  }

  protected loadRelationshipsOptions(): void {
    this.produitService
      .query({ 'prixproduitId.specified': 'false' })
      .pipe(map((res: HttpResponse<IProduit[]>) => res.body ?? []))
      .pipe(
        map((produits: IProduit[]) => this.produitService.addProduitToCollectionIfMissing(produits, this.editForm.get('produit')!.value))
      )
      .subscribe((produits: IProduit[]) => (this.produitsCollection = produits));

    this.compagneService
      .query()
      .pipe(map((res: HttpResponse<ICompagne[]>) => res.body ?? []))
      .pipe(
        map((compagnes: ICompagne[]) =>
          this.compagneService.addCompagneToCollectionIfMissing(compagnes, this.editForm.get('compagne')!.value)
        )
      )
      .subscribe((compagnes: ICompagne[]) => (this.compagnesSharedCollection = compagnes));
  }

  protected createFromForm(): IPrixproduit {
    return {
      ...new Prixproduit(),
      id: this.editForm.get(['id'])!.value,
      prix: this.editForm.get(['prix'])!.value,
      dateAjout: this.editForm.get(['dateAjout'])!.value ? dayjs(this.editForm.get(['dateAjout'])!.value, DATE_TIME_FORMAT) : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? dayjs(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      produit: this.editForm.get(['produit'])!.value,
      compagne: this.editForm.get(['compagne'])!.value,
    };
  }
}
