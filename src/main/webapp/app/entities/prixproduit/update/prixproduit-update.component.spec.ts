jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PrixproduitService } from '../service/prixproduit.service';
import { IPrixproduit, Prixproduit } from '../prixproduit.model';
import { IProduit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';
import { ICompagne } from 'app/entities/compagne/compagne.model';
import { CompagneService } from 'app/entities/compagne/service/compagne.service';

import { PrixproduitUpdateComponent } from './prixproduit-update.component';

describe('Prixproduit Management Update Component', () => {
  let comp: PrixproduitUpdateComponent;
  let fixture: ComponentFixture<PrixproduitUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let prixproduitService: PrixproduitService;
  let produitService: ProduitService;
  let compagneService: CompagneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PrixproduitUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PrixproduitUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PrixproduitUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    prixproduitService = TestBed.inject(PrixproduitService);
    produitService = TestBed.inject(ProduitService);
    compagneService = TestBed.inject(CompagneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call produit query and add missing value', () => {
      const prixproduit: IPrixproduit = { id: 456 };
      const produit: IProduit = { id: 62680 };
      prixproduit.produit = produit;

      const produitCollection: IProduit[] = [{ id: 15131 }];
      jest.spyOn(produitService, 'query').mockReturnValue(of(new HttpResponse({ body: produitCollection })));
      const expectedCollection: IProduit[] = [produit, ...produitCollection];
      jest.spyOn(produitService, 'addProduitToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      expect(produitService.query).toHaveBeenCalled();
      expect(produitService.addProduitToCollectionIfMissing).toHaveBeenCalledWith(produitCollection, produit);
      expect(comp.produitsCollection).toEqual(expectedCollection);
    });

    it('Should call Compagne query and add missing value', () => {
      const prixproduit: IPrixproduit = { id: 456 };
      const compagne: ICompagne = { id: 84294 };
      prixproduit.compagne = compagne;

      const compagneCollection: ICompagne[] = [{ id: 26967 }];
      jest.spyOn(compagneService, 'query').mockReturnValue(of(new HttpResponse({ body: compagneCollection })));
      const additionalCompagnes = [compagne];
      const expectedCollection: ICompagne[] = [...additionalCompagnes, ...compagneCollection];
      jest.spyOn(compagneService, 'addCompagneToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      expect(compagneService.query).toHaveBeenCalled();
      expect(compagneService.addCompagneToCollectionIfMissing).toHaveBeenCalledWith(compagneCollection, ...additionalCompagnes);
      expect(comp.compagnesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const prixproduit: IPrixproduit = { id: 456 };
      const produit: IProduit = { id: 75196 };
      prixproduit.produit = produit;
      const compagne: ICompagne = { id: 73433 };
      prixproduit.compagne = compagne;

      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(prixproduit));
      expect(comp.produitsCollection).toContain(produit);
      expect(comp.compagnesSharedCollection).toContain(compagne);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prixproduit>>();
      const prixproduit = { id: 123 };
      jest.spyOn(prixproduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: prixproduit }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(prixproduitService.update).toHaveBeenCalledWith(prixproduit);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prixproduit>>();
      const prixproduit = new Prixproduit();
      jest.spyOn(prixproduitService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: prixproduit }));
      saveSubject.complete();

      // THEN
      expect(prixproduitService.create).toHaveBeenCalledWith(prixproduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Prixproduit>>();
      const prixproduit = { id: 123 };
      jest.spyOn(prixproduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ prixproduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(prixproduitService.update).toHaveBeenCalledWith(prixproduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackProduitById', () => {
      it('Should return tracked Produit primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProduitById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCompagneById', () => {
      it('Should return tracked Compagne primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCompagneById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
