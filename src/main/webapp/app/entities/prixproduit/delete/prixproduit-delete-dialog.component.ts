import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPrixproduit } from '../prixproduit.model';
import { PrixproduitService } from '../service/prixproduit.service';

@Component({
  templateUrl: './prixproduit-delete-dialog.component.html',
})
export class PrixproduitDeleteDialogComponent {
  prixproduit?: IPrixproduit;

  constructor(protected prixproduitService: PrixproduitService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.prixproduitService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
