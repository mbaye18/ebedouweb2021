import * as dayjs from 'dayjs';
import { IProduit } from 'app/entities/produit/produit.model';
import { ICompagne } from 'app/entities/compagne/compagne.model';

export interface IPrixproduit {
  id?: number;
  prix?: number;
  dateAjout?: dayjs.Dayjs;
  dateModification?: dayjs.Dayjs;
  produit?: IProduit;
  compagne?: ICompagne;
}

export class Prixproduit implements IPrixproduit {
  constructor(
    public id?: number,
    public prix?: number,
    public dateAjout?: dayjs.Dayjs,
    public dateModification?: dayjs.Dayjs,
    public produit?: IProduit,
    public compagne?: ICompagne
  ) {}
}

export function getPrixproduitIdentifier(prixproduit: IPrixproduit): number | undefined {
  return prixproduit.id;
}
