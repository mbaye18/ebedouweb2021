import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPrixproduit, Prixproduit } from '../prixproduit.model';
import { PrixproduitService } from '../service/prixproduit.service';

@Injectable({ providedIn: 'root' })
export class PrixproduitRoutingResolveService implements Resolve<IPrixproduit> {
  constructor(protected service: PrixproduitService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPrixproduit> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((prixproduit: HttpResponse<Prixproduit>) => {
          if (prixproduit.body) {
            return of(prixproduit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Prixproduit());
  }
}
