import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PrixproduitComponent } from '../list/prixproduit.component';
import { PrixproduitDetailComponent } from '../detail/prixproduit-detail.component';
import { PrixproduitUpdateComponent } from '../update/prixproduit-update.component';
import { PrixproduitRoutingResolveService } from './prixproduit-routing-resolve.service';

const prixproduitRoute: Routes = [
  {
    path: '',
    component: PrixproduitComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PrixproduitDetailComponent,
    resolve: {
      prixproduit: PrixproduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PrixproduitUpdateComponent,
    resolve: {
      prixproduit: PrixproduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PrixproduitUpdateComponent,
    resolve: {
      prixproduit: PrixproduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(prixproduitRoute)],
  exports: [RouterModule],
})
export class PrixproduitRoutingModule {}
