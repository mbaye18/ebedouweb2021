jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPrixproduit, Prixproduit } from '../prixproduit.model';
import { PrixproduitService } from '../service/prixproduit.service';

import { PrixproduitRoutingResolveService } from './prixproduit-routing-resolve.service';

describe('Prixproduit routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PrixproduitRoutingResolveService;
  let service: PrixproduitService;
  let resultPrixproduit: IPrixproduit | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PrixproduitRoutingResolveService);
    service = TestBed.inject(PrixproduitService);
    resultPrixproduit = undefined;
  });

  describe('resolve', () => {
    it('should return IPrixproduit returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrixproduit = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPrixproduit).toEqual({ id: 123 });
    });

    it('should return new IPrixproduit if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrixproduit = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPrixproduit).toEqual(new Prixproduit());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Prixproduit })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPrixproduit = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPrixproduit).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
