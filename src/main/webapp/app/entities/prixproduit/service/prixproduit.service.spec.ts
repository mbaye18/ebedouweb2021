import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPrixproduit, Prixproduit } from '../prixproduit.model';

import { PrixproduitService } from './prixproduit.service';

describe('Prixproduit Service', () => {
  let service: PrixproduitService;
  let httpMock: HttpTestingController;
  let elemDefault: IPrixproduit;
  let expectedResult: IPrixproduit | IPrixproduit[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PrixproduitService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      prix: 0,
      dateAjout: currentDate,
      dateModification: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Prixproduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.create(new Prixproduit()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Prixproduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          prix: 1,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Prixproduit', () => {
      const patchObject = Object.assign(
        {
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        new Prixproduit()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Prixproduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          prix: 1,
          dateAjout: currentDate.format(DATE_TIME_FORMAT),
          dateModification: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateAjout: currentDate,
          dateModification: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Prixproduit', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPrixproduitToCollectionIfMissing', () => {
      it('should add a Prixproduit to an empty array', () => {
        const prixproduit: IPrixproduit = { id: 123 };
        expectedResult = service.addPrixproduitToCollectionIfMissing([], prixproduit);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(prixproduit);
      });

      it('should not add a Prixproduit to an array that contains it', () => {
        const prixproduit: IPrixproduit = { id: 123 };
        const prixproduitCollection: IPrixproduit[] = [
          {
            ...prixproduit,
          },
          { id: 456 },
        ];
        expectedResult = service.addPrixproduitToCollectionIfMissing(prixproduitCollection, prixproduit);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Prixproduit to an array that doesn't contain it", () => {
        const prixproduit: IPrixproduit = { id: 123 };
        const prixproduitCollection: IPrixproduit[] = [{ id: 456 }];
        expectedResult = service.addPrixproduitToCollectionIfMissing(prixproduitCollection, prixproduit);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(prixproduit);
      });

      it('should add only unique Prixproduit to an array', () => {
        const prixproduitArray: IPrixproduit[] = [{ id: 123 }, { id: 456 }, { id: 2043 }];
        const prixproduitCollection: IPrixproduit[] = [{ id: 123 }];
        expectedResult = service.addPrixproduitToCollectionIfMissing(prixproduitCollection, ...prixproduitArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const prixproduit: IPrixproduit = { id: 123 };
        const prixproduit2: IPrixproduit = { id: 456 };
        expectedResult = service.addPrixproduitToCollectionIfMissing([], prixproduit, prixproduit2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(prixproduit);
        expect(expectedResult).toContain(prixproduit2);
      });

      it('should accept null and undefined values', () => {
        const prixproduit: IPrixproduit = { id: 123 };
        expectedResult = service.addPrixproduitToCollectionIfMissing([], null, prixproduit, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(prixproduit);
      });

      it('should return initial array if no Prixproduit is added', () => {
        const prixproduitCollection: IPrixproduit[] = [{ id: 123 }];
        expectedResult = service.addPrixproduitToCollectionIfMissing(prixproduitCollection, undefined, null);
        expect(expectedResult).toEqual(prixproduitCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
