import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPrixproduit, getPrixproduitIdentifier } from '../prixproduit.model';

export type EntityResponseType = HttpResponse<IPrixproduit>;
export type EntityArrayResponseType = HttpResponse<IPrixproduit[]>;

@Injectable({ providedIn: 'root' })
export class PrixproduitService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/prixproduits');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(prixproduit: IPrixproduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(prixproduit);
    return this.http
      .post<IPrixproduit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(prixproduit: IPrixproduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(prixproduit);
    return this.http
      .put<IPrixproduit>(`${this.resourceUrl}/${getPrixproduitIdentifier(prixproduit) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(prixproduit: IPrixproduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(prixproduit);
    return this.http
      .patch<IPrixproduit>(`${this.resourceUrl}/${getPrixproduitIdentifier(prixproduit) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPrixproduit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPrixproduit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPrixproduitToCollectionIfMissing(
    prixproduitCollection: IPrixproduit[],
    ...prixproduitsToCheck: (IPrixproduit | null | undefined)[]
  ): IPrixproduit[] {
    const prixproduits: IPrixproduit[] = prixproduitsToCheck.filter(isPresent);
    if (prixproduits.length > 0) {
      const prixproduitCollectionIdentifiers = prixproduitCollection.map(prixproduitItem => getPrixproduitIdentifier(prixproduitItem)!);
      const prixproduitsToAdd = prixproduits.filter(prixproduitItem => {
        const prixproduitIdentifier = getPrixproduitIdentifier(prixproduitItem);
        if (prixproduitIdentifier == null || prixproduitCollectionIdentifiers.includes(prixproduitIdentifier)) {
          return false;
        }
        prixproduitCollectionIdentifiers.push(prixproduitIdentifier);
        return true;
      });
      return [...prixproduitsToAdd, ...prixproduitCollection];
    }
    return prixproduitCollection;
  }

  protected convertDateFromClient(prixproduit: IPrixproduit): IPrixproduit {
    return Object.assign({}, prixproduit, {
      dateAjout: prixproduit.dateAjout?.isValid() ? prixproduit.dateAjout.toJSON() : undefined,
      dateModification: prixproduit.dateModification?.isValid() ? prixproduit.dateModification.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateAjout = res.body.dateAjout ? dayjs(res.body.dateAjout) : undefined;
      res.body.dateModification = res.body.dateModification ? dayjs(res.body.dateModification) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((prixproduit: IPrixproduit) => {
        prixproduit.dateAjout = prixproduit.dateAjout ? dayjs(prixproduit.dateAjout) : undefined;
        prixproduit.dateModification = prixproduit.dateModification ? dayjs(prixproduit.dateModification) : undefined;
      });
    }
    return res;
  }
}
