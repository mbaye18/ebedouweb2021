import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PrixproduitDetailComponent } from './prixproduit-detail.component';

describe('Prixproduit Management Detail Component', () => {
  let comp: PrixproduitDetailComponent;
  let fixture: ComponentFixture<PrixproduitDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PrixproduitDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ prixproduit: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PrixproduitDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PrixproduitDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load prixproduit on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.prixproduit).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
