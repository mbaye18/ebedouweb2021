import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPrixproduit } from '../prixproduit.model';

@Component({
  selector: 'jhi-prixproduit-detail',
  templateUrl: './prixproduit-detail.component.html',
})
export class PrixproduitDetailComponent implements OnInit {
  prixproduit: IPrixproduit | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prixproduit }) => {
      this.prixproduit = prixproduit;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
