import { IUser } from 'app/entities/user/user.model';

export interface IWallet {
  id?: number;
  user?: IUser;
}

export class Wallet implements IWallet {
  constructor(public id?: number, public user?: IUser) {}
}

export function getWalletIdentifier(wallet: IWallet): number | undefined {
  return wallet.id;
}
