import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IListeCooperative, ListeCooperative } from '../liste-cooperative.model';
import { ListeCooperativeService } from '../service/liste-cooperative.service';

@Injectable({ providedIn: 'root' })
export class ListeCooperativeRoutingResolveService implements Resolve<IListeCooperative> {
  constructor(protected service: ListeCooperativeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IListeCooperative> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((listeCooperative: HttpResponse<ListeCooperative>) => {
          if (listeCooperative.body) {
            return of(listeCooperative.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ListeCooperative());
  }
}
