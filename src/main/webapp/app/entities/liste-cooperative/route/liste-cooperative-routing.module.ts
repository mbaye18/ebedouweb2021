import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ListeCooperativeComponent } from '../list/liste-cooperative.component';
import { ListeCooperativeDetailComponent } from '../detail/liste-cooperative-detail.component';
import { ListeCooperativeUpdateComponent } from '../update/liste-cooperative-update.component';
import { ListeCooperativeRoutingResolveService } from './liste-cooperative-routing-resolve.service';

const listeCooperativeRoute: Routes = [
  {
    path: '',
    component: ListeCooperativeComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ListeCooperativeDetailComponent,
    resolve: {
      listeCooperative: ListeCooperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ListeCooperativeUpdateComponent,
    resolve: {
      listeCooperative: ListeCooperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ListeCooperativeUpdateComponent,
    resolve: {
      listeCooperative: ListeCooperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(listeCooperativeRoute)],
  exports: [RouterModule],
})
export class ListeCooperativeRoutingModule {}
