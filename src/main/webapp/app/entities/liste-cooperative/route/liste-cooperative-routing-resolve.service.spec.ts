jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IListeCooperative, ListeCooperative } from '../liste-cooperative.model';
import { ListeCooperativeService } from '../service/liste-cooperative.service';

import { ListeCooperativeRoutingResolveService } from './liste-cooperative-routing-resolve.service';

describe('ListeCooperative routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ListeCooperativeRoutingResolveService;
  let service: ListeCooperativeService;
  let resultListeCooperative: IListeCooperative | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(ListeCooperativeRoutingResolveService);
    service = TestBed.inject(ListeCooperativeService);
    resultListeCooperative = undefined;
  });

  describe('resolve', () => {
    it('should return IListeCooperative returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultListeCooperative = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultListeCooperative).toEqual({ id: 123 });
    });

    it('should return new IListeCooperative if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultListeCooperative = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultListeCooperative).toEqual(new ListeCooperative());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as ListeCooperative })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultListeCooperative = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultListeCooperative).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
