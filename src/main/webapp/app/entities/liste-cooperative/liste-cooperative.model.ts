import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';

export interface IListeCooperative {
  id?: number;
  membre?: IInfoplus;
  cooperative?: ICooperative;
}

export class ListeCooperative implements IListeCooperative {
  constructor(public id?: number, public membre?: IInfoplus, public cooperative?: ICooperative) {}
}

export function getListeCooperativeIdentifier(listeCooperative: IListeCooperative): number | undefined {
  return listeCooperative.id;
}
