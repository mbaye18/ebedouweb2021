import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IListeCooperative } from '../liste-cooperative.model';
import { ListeCooperativeService } from '../service/liste-cooperative.service';

@Component({
  templateUrl: './liste-cooperative-delete-dialog.component.html',
})
export class ListeCooperativeDeleteDialogComponent {
  listeCooperative?: IListeCooperative;

  constructor(protected listeCooperativeService: ListeCooperativeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.listeCooperativeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
