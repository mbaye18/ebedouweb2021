jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ListeCooperativeService } from '../service/liste-cooperative.service';
import { IListeCooperative, ListeCooperative } from '../liste-cooperative.model';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { InfoplusService } from 'app/entities/infoplus/service/infoplus.service';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';

import { ListeCooperativeUpdateComponent } from './liste-cooperative-update.component';

describe('ListeCooperative Management Update Component', () => {
  let comp: ListeCooperativeUpdateComponent;
  let fixture: ComponentFixture<ListeCooperativeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let listeCooperativeService: ListeCooperativeService;
  let infoplusService: InfoplusService;
  let cooperativeService: CooperativeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ListeCooperativeUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ListeCooperativeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ListeCooperativeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    listeCooperativeService = TestBed.inject(ListeCooperativeService);
    infoplusService = TestBed.inject(InfoplusService);
    cooperativeService = TestBed.inject(CooperativeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call membre query and add missing value', () => {
      const listeCooperative: IListeCooperative = { id: 456 };
      const membre: IInfoplus = { id: 8752 };
      listeCooperative.membre = membre;

      const membreCollection: IInfoplus[] = [{ id: 16146 }];
      jest.spyOn(infoplusService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const expectedCollection: IInfoplus[] = [membre, ...membreCollection];
      jest.spyOn(infoplusService, 'addInfoplusToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      expect(infoplusService.query).toHaveBeenCalled();
      expect(infoplusService.addInfoplusToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, membre);
      expect(comp.membresCollection).toEqual(expectedCollection);
    });

    it('Should call Cooperative query and add missing value', () => {
      const listeCooperative: IListeCooperative = { id: 456 };
      const cooperative: ICooperative = { id: 45130 };
      listeCooperative.cooperative = cooperative;

      const cooperativeCollection: ICooperative[] = [{ id: 20190 }];
      jest.spyOn(cooperativeService, 'query').mockReturnValue(of(new HttpResponse({ body: cooperativeCollection })));
      const additionalCooperatives = [cooperative];
      const expectedCollection: ICooperative[] = [...additionalCooperatives, ...cooperativeCollection];
      jest.spyOn(cooperativeService, 'addCooperativeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      expect(cooperativeService.query).toHaveBeenCalled();
      expect(cooperativeService.addCooperativeToCollectionIfMissing).toHaveBeenCalledWith(cooperativeCollection, ...additionalCooperatives);
      expect(comp.cooperativesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const listeCooperative: IListeCooperative = { id: 456 };
      const membre: IInfoplus = { id: 75592 };
      listeCooperative.membre = membre;
      const cooperative: ICooperative = { id: 11667 };
      listeCooperative.cooperative = cooperative;

      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(listeCooperative));
      expect(comp.membresCollection).toContain(membre);
      expect(comp.cooperativesSharedCollection).toContain(cooperative);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ListeCooperative>>();
      const listeCooperative = { id: 123 };
      jest.spyOn(listeCooperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: listeCooperative }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(listeCooperativeService.update).toHaveBeenCalledWith(listeCooperative);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ListeCooperative>>();
      const listeCooperative = new ListeCooperative();
      jest.spyOn(listeCooperativeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: listeCooperative }));
      saveSubject.complete();

      // THEN
      expect(listeCooperativeService.create).toHaveBeenCalledWith(listeCooperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ListeCooperative>>();
      const listeCooperative = { id: 123 };
      jest.spyOn(listeCooperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ listeCooperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(listeCooperativeService.update).toHaveBeenCalledWith(listeCooperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackInfoplusById', () => {
      it('Should return tracked Infoplus primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackInfoplusById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCooperativeById', () => {
      it('Should return tracked Cooperative primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCooperativeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
