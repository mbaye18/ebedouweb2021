import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IListeCooperative, ListeCooperative } from '../liste-cooperative.model';
import { ListeCooperativeService } from '../service/liste-cooperative.service';
import { IInfoplus } from 'app/entities/infoplus/infoplus.model';
import { InfoplusService } from 'app/entities/infoplus/service/infoplus.service';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';

@Component({
  selector: 'jhi-liste-cooperative-update',
  templateUrl: './liste-cooperative-update.component.html',
})
export class ListeCooperativeUpdateComponent implements OnInit {
  isSaving = false;

  membresCollection: IInfoplus[] = [];
  cooperativesSharedCollection: ICooperative[] = [];

  editForm = this.fb.group({
    id: [],
    membre: [null, Validators.required],
    cooperative: [null, Validators.required],
  });

  constructor(
    protected listeCooperativeService: ListeCooperativeService,
    protected infoplusService: InfoplusService,
    protected cooperativeService: CooperativeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listeCooperative }) => {
      this.updateForm(listeCooperative);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const listeCooperative = this.createFromForm();
    if (listeCooperative.id !== undefined) {
      this.subscribeToSaveResponse(this.listeCooperativeService.update(listeCooperative));
    } else {
      this.subscribeToSaveResponse(this.listeCooperativeService.create(listeCooperative));
    }
  }

  trackInfoplusById(index: number, item: IInfoplus): number {
    return item.id!;
  }

  trackCooperativeById(index: number, item: ICooperative): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListeCooperative>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(listeCooperative: IListeCooperative): void {
    this.editForm.patchValue({
      id: listeCooperative.id,
      membre: listeCooperative.membre,
      cooperative: listeCooperative.cooperative,
    });

    this.membresCollection = this.infoplusService.addInfoplusToCollectionIfMissing(this.membresCollection, listeCooperative.membre);
    this.cooperativesSharedCollection = this.cooperativeService.addCooperativeToCollectionIfMissing(
      this.cooperativesSharedCollection,
      listeCooperative.cooperative
    );
  }

  protected loadRelationshipsOptions(): void {
    this.infoplusService
      .query({ filter: 'listecooperative-is-null' })
      .pipe(map((res: HttpResponse<IInfoplus[]>) => res.body ?? []))
      .pipe(
        map((infopluses: IInfoplus[]) =>
          this.infoplusService.addInfoplusToCollectionIfMissing(infopluses, this.editForm.get('membre')!.value)
        )
      )
      .subscribe((infopluses: IInfoplus[]) => (this.membresCollection = infopluses));

    this.cooperativeService
      .query()
      .pipe(map((res: HttpResponse<ICooperative[]>) => res.body ?? []))
      .pipe(
        map((cooperatives: ICooperative[]) =>
          this.cooperativeService.addCooperativeToCollectionIfMissing(cooperatives, this.editForm.get('cooperative')!.value)
        )
      )
      .subscribe((cooperatives: ICooperative[]) => (this.cooperativesSharedCollection = cooperatives));
  }

  protected createFromForm(): IListeCooperative {
    return {
      ...new ListeCooperative(),
      id: this.editForm.get(['id'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      cooperative: this.editForm.get(['cooperative'])!.value,
    };
  }
}
