import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ListeCooperativeComponent } from './list/liste-cooperative.component';
import { ListeCooperativeDetailComponent } from './detail/liste-cooperative-detail.component';
import { ListeCooperativeUpdateComponent } from './update/liste-cooperative-update.component';
import { ListeCooperativeDeleteDialogComponent } from './delete/liste-cooperative-delete-dialog.component';
import { ListeCooperativeRoutingModule } from './route/liste-cooperative-routing.module';

@NgModule({
  imports: [SharedModule, ListeCooperativeRoutingModule],
  declarations: [
    ListeCooperativeComponent,
    ListeCooperativeDetailComponent,
    ListeCooperativeUpdateComponent,
    ListeCooperativeDeleteDialogComponent,
  ],
  entryComponents: [ListeCooperativeDeleteDialogComponent],
})
export class ListeCooperativeModule {}
