import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IListeCooperative, ListeCooperative } from '../liste-cooperative.model';

import { ListeCooperativeService } from './liste-cooperative.service';

describe('ListeCooperative Service', () => {
  let service: ListeCooperativeService;
  let httpMock: HttpTestingController;
  let elemDefault: IListeCooperative;
  let expectedResult: IListeCooperative | IListeCooperative[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ListeCooperativeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ListeCooperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ListeCooperative()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ListeCooperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ListeCooperative', () => {
      const patchObject = Object.assign({}, new ListeCooperative());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ListeCooperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ListeCooperative', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addListeCooperativeToCollectionIfMissing', () => {
      it('should add a ListeCooperative to an empty array', () => {
        const listeCooperative: IListeCooperative = { id: 123 };
        expectedResult = service.addListeCooperativeToCollectionIfMissing([], listeCooperative);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(listeCooperative);
      });

      it('should not add a ListeCooperative to an array that contains it', () => {
        const listeCooperative: IListeCooperative = { id: 123 };
        const listeCooperativeCollection: IListeCooperative[] = [
          {
            ...listeCooperative,
          },
          { id: 456 },
        ];
        expectedResult = service.addListeCooperativeToCollectionIfMissing(listeCooperativeCollection, listeCooperative);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ListeCooperative to an array that doesn't contain it", () => {
        const listeCooperative: IListeCooperative = { id: 123 };
        const listeCooperativeCollection: IListeCooperative[] = [{ id: 456 }];
        expectedResult = service.addListeCooperativeToCollectionIfMissing(listeCooperativeCollection, listeCooperative);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(listeCooperative);
      });

      it('should add only unique ListeCooperative to an array', () => {
        const listeCooperativeArray: IListeCooperative[] = [{ id: 123 }, { id: 456 }, { id: 89176 }];
        const listeCooperativeCollection: IListeCooperative[] = [{ id: 123 }];
        expectedResult = service.addListeCooperativeToCollectionIfMissing(listeCooperativeCollection, ...listeCooperativeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const listeCooperative: IListeCooperative = { id: 123 };
        const listeCooperative2: IListeCooperative = { id: 456 };
        expectedResult = service.addListeCooperativeToCollectionIfMissing([], listeCooperative, listeCooperative2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(listeCooperative);
        expect(expectedResult).toContain(listeCooperative2);
      });

      it('should accept null and undefined values', () => {
        const listeCooperative: IListeCooperative = { id: 123 };
        expectedResult = service.addListeCooperativeToCollectionIfMissing([], null, listeCooperative, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(listeCooperative);
      });

      it('should return initial array if no ListeCooperative is added', () => {
        const listeCooperativeCollection: IListeCooperative[] = [{ id: 123 }];
        expectedResult = service.addListeCooperativeToCollectionIfMissing(listeCooperativeCollection, undefined, null);
        expect(expectedResult).toEqual(listeCooperativeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
