import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IListeCooperative, getListeCooperativeIdentifier } from '../liste-cooperative.model';

export type EntityResponseType = HttpResponse<IListeCooperative>;
export type EntityArrayResponseType = HttpResponse<IListeCooperative[]>;

@Injectable({ providedIn: 'root' })
export class ListeCooperativeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/liste-cooperatives');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(listeCooperative: IListeCooperative): Observable<EntityResponseType> {
    return this.http.post<IListeCooperative>(this.resourceUrl, listeCooperative, { observe: 'response' });
  }

  update(listeCooperative: IListeCooperative): Observable<EntityResponseType> {
    return this.http.put<IListeCooperative>(
      `${this.resourceUrl}/${getListeCooperativeIdentifier(listeCooperative) as number}`,
      listeCooperative,
      { observe: 'response' }
    );
  }

  partialUpdate(listeCooperative: IListeCooperative): Observable<EntityResponseType> {
    return this.http.patch<IListeCooperative>(
      `${this.resourceUrl}/${getListeCooperativeIdentifier(listeCooperative) as number}`,
      listeCooperative,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListeCooperative>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListeCooperative[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addListeCooperativeToCollectionIfMissing(
    listeCooperativeCollection: IListeCooperative[],
    ...listeCooperativesToCheck: (IListeCooperative | null | undefined)[]
  ): IListeCooperative[] {
    const listeCooperatives: IListeCooperative[] = listeCooperativesToCheck.filter(isPresent);
    if (listeCooperatives.length > 0) {
      const listeCooperativeCollectionIdentifiers = listeCooperativeCollection.map(
        listeCooperativeItem => getListeCooperativeIdentifier(listeCooperativeItem)!
      );
      const listeCooperativesToAdd = listeCooperatives.filter(listeCooperativeItem => {
        const listeCooperativeIdentifier = getListeCooperativeIdentifier(listeCooperativeItem);
        if (listeCooperativeIdentifier == null || listeCooperativeCollectionIdentifiers.includes(listeCooperativeIdentifier)) {
          return false;
        }
        listeCooperativeCollectionIdentifiers.push(listeCooperativeIdentifier);
        return true;
      });
      return [...listeCooperativesToAdd, ...listeCooperativeCollection];
    }
    return listeCooperativeCollection;
  }
}
