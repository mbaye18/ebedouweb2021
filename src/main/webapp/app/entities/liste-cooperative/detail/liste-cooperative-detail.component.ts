import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListeCooperative } from '../liste-cooperative.model';

@Component({
  selector: 'jhi-liste-cooperative-detail',
  templateUrl: './liste-cooperative-detail.component.html',
})
export class ListeCooperativeDetailComponent implements OnInit {
  listeCooperative: IListeCooperative | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listeCooperative }) => {
      this.listeCooperative = listeCooperative;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
