import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ListeCooperativeDetailComponent } from './liste-cooperative-detail.component';

describe('ListeCooperative Management Detail Component', () => {
  let comp: ListeCooperativeDetailComponent;
  let fixture: ComponentFixture<ListeCooperativeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListeCooperativeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ listeCooperative: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ListeCooperativeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ListeCooperativeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load listeCooperative on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.listeCooperative).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
