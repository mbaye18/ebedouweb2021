import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';

export interface IDemande {
  id?: number;
  date?: dayjs.Dayjs;
  etat?: string;
  user?: IUser;
  cooperative?: ICooperative;
}

export class Demande implements IDemande {
  constructor(
    public id?: number,
    public date?: dayjs.Dayjs,
    public etat?: string,
    public user?: IUser,
    public cooperative?: ICooperative
  ) {}
}

export function getDemandeIdentifier(demande: IDemande): number | undefined {
  return demande.id;
}
