import * as dayjs from 'dayjs';

export interface IImageProduit {
  id?: number;
  imageContentType?: string;
  image?: string;
  date?: dayjs.Dayjs;
}

export class ImageProduit implements IImageProduit {
  constructor(public id?: number, public imageContentType?: string, public image?: string, public date?: dayjs.Dayjs) {}
}

export function getImageProduitIdentifier(imageProduit: IImageProduit): number | undefined {
  return imageProduit.id;
}
