import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IImageProduit, ImageProduit } from '../image-produit.model';
import { ImageProduitService } from '../service/image-produit.service';

@Injectable({ providedIn: 'root' })
export class ImageProduitRoutingResolveService implements Resolve<IImageProduit> {
  constructor(protected service: ImageProduitService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IImageProduit> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((imageProduit: HttpResponse<ImageProduit>) => {
          if (imageProduit.body) {
            return of(imageProduit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ImageProduit());
  }
}
