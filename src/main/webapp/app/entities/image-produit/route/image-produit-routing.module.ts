import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ImageProduitComponent } from '../list/image-produit.component';
import { ImageProduitDetailComponent } from '../detail/image-produit-detail.component';
import { ImageProduitUpdateComponent } from '../update/image-produit-update.component';
import { ImageProduitRoutingResolveService } from './image-produit-routing-resolve.service';

const imageProduitRoute: Routes = [
  {
    path: '',
    component: ImageProduitComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ImageProduitDetailComponent,
    resolve: {
      imageProduit: ImageProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ImageProduitUpdateComponent,
    resolve: {
      imageProduit: ImageProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ImageProduitUpdateComponent,
    resolve: {
      imageProduit: ImageProduitRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(imageProduitRoute)],
  exports: [RouterModule],
})
export class ImageProduitRoutingModule {}
