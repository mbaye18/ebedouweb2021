jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ImageProduitService } from '../service/image-produit.service';
import { IImageProduit, ImageProduit } from '../image-produit.model';

import { ImageProduitUpdateComponent } from './image-produit-update.component';

describe('ImageProduit Management Update Component', () => {
  let comp: ImageProduitUpdateComponent;
  let fixture: ComponentFixture<ImageProduitUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let imageProduitService: ImageProduitService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ImageProduitUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ImageProduitUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ImageProduitUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    imageProduitService = TestBed.inject(ImageProduitService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const imageProduit: IImageProduit = { id: 456 };

      activatedRoute.data = of({ imageProduit });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(imageProduit));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ImageProduit>>();
      const imageProduit = { id: 123 };
      jest.spyOn(imageProduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imageProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: imageProduit }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(imageProduitService.update).toHaveBeenCalledWith(imageProduit);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ImageProduit>>();
      const imageProduit = new ImageProduit();
      jest.spyOn(imageProduitService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imageProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: imageProduit }));
      saveSubject.complete();

      // THEN
      expect(imageProduitService.create).toHaveBeenCalledWith(imageProduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ImageProduit>>();
      const imageProduit = { id: 123 };
      jest.spyOn(imageProduitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imageProduit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(imageProduitService.update).toHaveBeenCalledWith(imageProduit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
