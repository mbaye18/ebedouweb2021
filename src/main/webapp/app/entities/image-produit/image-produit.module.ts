import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ImageProduitComponent } from './list/image-produit.component';
import { ImageProduitDetailComponent } from './detail/image-produit-detail.component';
import { ImageProduitUpdateComponent } from './update/image-produit-update.component';
import { ImageProduitDeleteDialogComponent } from './delete/image-produit-delete-dialog.component';
import { ImageProduitRoutingModule } from './route/image-produit-routing.module';

@NgModule({
  imports: [SharedModule, ImageProduitRoutingModule],
  declarations: [ImageProduitComponent, ImageProduitDetailComponent, ImageProduitUpdateComponent, ImageProduitDeleteDialogComponent],
  entryComponents: [ImageProduitDeleteDialogComponent],
})
export class ImageProduitModule {}
