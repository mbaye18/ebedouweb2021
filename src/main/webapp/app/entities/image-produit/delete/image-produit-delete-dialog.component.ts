import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IImageProduit } from '../image-produit.model';
import { ImageProduitService } from '../service/image-produit.service';

@Component({
  templateUrl: './image-produit-delete-dialog.component.html',
})
export class ImageProduitDeleteDialogComponent {
  imageProduit?: IImageProduit;

  constructor(protected imageProduitService: ImageProduitService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.imageProduitService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
