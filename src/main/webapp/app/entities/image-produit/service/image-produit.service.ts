import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IImageProduit, getImageProduitIdentifier } from '../image-produit.model';

export type EntityResponseType = HttpResponse<IImageProduit>;
export type EntityArrayResponseType = HttpResponse<IImageProduit[]>;

@Injectable({ providedIn: 'root' })
export class ImageProduitService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/image-produits');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(imageProduit: IImageProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imageProduit);
    return this.http
      .post<IImageProduit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(imageProduit: IImageProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imageProduit);
    return this.http
      .put<IImageProduit>(`${this.resourceUrl}/${getImageProduitIdentifier(imageProduit) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(imageProduit: IImageProduit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imageProduit);
    return this.http
      .patch<IImageProduit>(`${this.resourceUrl}/${getImageProduitIdentifier(imageProduit) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IImageProduit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IImageProduit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addImageProduitToCollectionIfMissing(
    imageProduitCollection: IImageProduit[],
    ...imageProduitsToCheck: (IImageProduit | null | undefined)[]
  ): IImageProduit[] {
    const imageProduits: IImageProduit[] = imageProduitsToCheck.filter(isPresent);
    if (imageProduits.length > 0) {
      const imageProduitCollectionIdentifiers = imageProduitCollection.map(
        imageProduitItem => getImageProduitIdentifier(imageProduitItem)!
      );
      const imageProduitsToAdd = imageProduits.filter(imageProduitItem => {
        const imageProduitIdentifier = getImageProduitIdentifier(imageProduitItem);
        if (imageProduitIdentifier == null || imageProduitCollectionIdentifiers.includes(imageProduitIdentifier)) {
          return false;
        }
        imageProduitCollectionIdentifiers.push(imageProduitIdentifier);
        return true;
      });
      return [...imageProduitsToAdd, ...imageProduitCollection];
    }
    return imageProduitCollection;
  }

  protected convertDateFromClient(imageProduit: IImageProduit): IImageProduit {
    return Object.assign({}, imageProduit, {
      date: imageProduit.date?.isValid() ? imageProduit.date.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((imageProduit: IImageProduit) => {
        imageProduit.date = imageProduit.date ? dayjs(imageProduit.date) : undefined;
      });
    }
    return res;
  }
}
