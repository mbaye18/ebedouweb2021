import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IImageProduit, ImageProduit } from '../image-produit.model';

import { ImageProduitService } from './image-produit.service';

describe('ImageProduit Service', () => {
  let service: ImageProduitService;
  let httpMock: HttpTestingController;
  let elemDefault: IImageProduit;
  let expectedResult: IImageProduit | IImageProduit[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ImageProduitService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      imageContentType: 'image/png',
      image: 'AAAAAAA',
      date: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ImageProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.create(new ImageProduit()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ImageProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          image: 'BBBBBB',
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ImageProduit', () => {
      const patchObject = Object.assign(
        {
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        new ImageProduit()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ImageProduit', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          image: 'BBBBBB',
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ImageProduit', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addImageProduitToCollectionIfMissing', () => {
      it('should add a ImageProduit to an empty array', () => {
        const imageProduit: IImageProduit = { id: 123 };
        expectedResult = service.addImageProduitToCollectionIfMissing([], imageProduit);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imageProduit);
      });

      it('should not add a ImageProduit to an array that contains it', () => {
        const imageProduit: IImageProduit = { id: 123 };
        const imageProduitCollection: IImageProduit[] = [
          {
            ...imageProduit,
          },
          { id: 456 },
        ];
        expectedResult = service.addImageProduitToCollectionIfMissing(imageProduitCollection, imageProduit);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ImageProduit to an array that doesn't contain it", () => {
        const imageProduit: IImageProduit = { id: 123 };
        const imageProduitCollection: IImageProduit[] = [{ id: 456 }];
        expectedResult = service.addImageProduitToCollectionIfMissing(imageProduitCollection, imageProduit);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imageProduit);
      });

      it('should add only unique ImageProduit to an array', () => {
        const imageProduitArray: IImageProduit[] = [{ id: 123 }, { id: 456 }, { id: 92857 }];
        const imageProduitCollection: IImageProduit[] = [{ id: 123 }];
        expectedResult = service.addImageProduitToCollectionIfMissing(imageProduitCollection, ...imageProduitArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const imageProduit: IImageProduit = { id: 123 };
        const imageProduit2: IImageProduit = { id: 456 };
        expectedResult = service.addImageProduitToCollectionIfMissing([], imageProduit, imageProduit2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imageProduit);
        expect(expectedResult).toContain(imageProduit2);
      });

      it('should accept null and undefined values', () => {
        const imageProduit: IImageProduit = { id: 123 };
        expectedResult = service.addImageProduitToCollectionIfMissing([], null, imageProduit, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imageProduit);
      });

      it('should return initial array if no ImageProduit is added', () => {
        const imageProduitCollection: IImageProduit[] = [{ id: 123 }];
        expectedResult = service.addImageProduitToCollectionIfMissing(imageProduitCollection, undefined, null);
        expect(expectedResult).toEqual(imageProduitCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
