import { Component, AfterViewInit, ElementRef, ViewChild, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/config/error.constants';
import { RegisterService } from './register.service';
import { INationnalite } from 'app/entities/nationnalite/nationnalite.model';
import { IZoneExploitation } from 'app/entities/zone-exploitation/zone-exploitation.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { NationnaliteService } from 'app/entities/nationnalite/service/nationnalite.service';
import { ZoneExploitationService } from 'app/entities/zone-exploitation/service/zone-exploitation.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { map } from 'rxjs/operators';
import { Registration } from './register.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements AfterViewInit, OnInit {
  @ViewChild('login', { static: false })
  login?: ElementRef;

  doNotMatch = false;
  error = false;
  errorEmailExists = false;
  errorUserExists = false;
  success = false;
  nationnalitesSharedCollection: INationnalite[] = [];
  zoneExploitationsSharedCollection: IZoneExploitation[] = [];
  registerForm = this.fb.group({
    login: [
      '',
      [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$'),
      ],
    ],
    firstName: ['', [Validators.maxLength(50)]],
    lastName: ['', [Validators.maxLength(50)]],
    langKey: [],
    profileSelected: [null, [Validators.required]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
    numeroTelephone: [null, [Validators.required]],
    pieceIdentite: [null, [Validators.required]],
    pieceIdentiteContentType: [],
    dateNaissance: [null, [Validators.required]],
    lieuNaissance: [null, [Validators.required]],
    genre: [null, [Validators.required]],
    nationnalite: [null, Validators.required],
    zoneExploitation: [null, [Validators.required]],
  });

  constructor(
    private translateService: TranslateService,
    private registerService: RegisterService,
    private fb: FormBuilder,
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected nationnaliteService: NationnaliteService,
    protected router: Router,
    protected zoneExploitationService: ZoneExploitationService
  ) {}

  ngAfterViewInit(): void {
    if (this.login) {
      this.login.nativeElement.focus();
    }
  }
  ngOnInit(): void {
    this.loadRelationshipsOptions();
  }
  register(): void {
    this.doNotMatch = false;
    this.error = false;
    this.errorEmailExists = false;
    this.errorUserExists = false;

    const password = this.registerForm.get(['password'])!.value;
    if (password !== this.registerForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      const user = this.createFromForm();
      this.registerService.save(user).subscribe(
        () => {
          const formData: any = new FormData();
          formData.append('admin_pass', 'EB-OTF-5K4-837-0P2');
          formData.append('name', user.login);
          formData.append('email', user.email);
          formData.append('password', user.password);

          this.registerService.saveportement(formData).subscribe(res => {
            // eslint-disable-next-line no-console
            console.log('ddsxscx' + res);
          });
          Swal.fire({
            icon: 'success',
            title: 'Félicitations...',
            text:
              'Merci ' +
              user.firstName +
              ' ' +
              user.lastName +
              ' pour votre inscription sur la plateforme e.BEDOU. Votre demande a bien été prise en compte. \n Un message de validation vous sera envoyé si toutes les informations fournies sont correctes.\n Cordialement !',
            footer: '<p>Veuillez soigneusement garder votre login et mot de passe pour pouvoir vous connecter ultérieurement.</p>',
          }).then(result => {
            if (result.value) {
              this.router.navigate(['']);
            }
          });
          this.success = true;
        },
        response => this.processError(response)
      );
    }
  }
  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }
  annuller(): void {
    window.history.back();
  }
  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.registerForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('ebedouApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }
  private processError(response: HttpErrorResponse): void {
    if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
      this.errorUserExists = true;
    } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
      this.errorEmailExists = true;
    } else {
      this.error = true;
    }
  }
  protected loadRelationshipsOptions(): void {
    this.nationnaliteService
      .query()
      .pipe(map((res: HttpResponse<INationnalite[]>) => res.body ?? []))
      .pipe(
        map((nationnalites: INationnalite[]) =>
          this.nationnaliteService.addNationnaliteToCollectionIfMissing(nationnalites, this.registerForm.get('nationnalite')!.value)
        )
      )
      .subscribe((nationnalites: INationnalite[]) => (this.nationnalitesSharedCollection = nationnalites));

    this.zoneExploitationService
      .query()
      .pipe(map((res: HttpResponse<IZoneExploitation[]>) => res.body ?? []))
      .pipe(
        map((zoneExploitations: IZoneExploitation[]) =>
          this.zoneExploitationService.addZoneExploitationToCollectionIfMissing(
            zoneExploitations,
            this.registerForm.get('zoneExploitation')!.value
          )
        )
      )
      .subscribe((zoneExploitations: IZoneExploitation[]) => (this.zoneExploitationsSharedCollection = zoneExploitations));
  }

  protected createFromForm(): Registration {
    return {
      numeroTelephone: this.registerForm.get(['numeroTelephone'])!.value,
      pieceIdentiteContentType: this.registerForm.get(['pieceIdentiteContentType'])!.value,
      pieceIdentite: this.registerForm.get(['pieceIdentite'])!.value,
      login: this.registerForm.get(['login'])!.value,
      email: this.registerForm.get(['email'])!.value,
      firstName: this.registerForm.get(['firstName'])!.value,
      lastName: this.registerForm.get(['lastName'])!.value,
      password: this.registerForm.get(['password'])!.value,
      authorities: [this.registerForm.get(['profileSelected'])!.value],
      dateNaissance: this.registerForm.get(['dateNaissance'])!.value,
      lieuNaissance: this.registerForm.get(['lieuNaissance'])!.value,
      genre: this.registerForm.get(['genre'])!.value,
      nationnalite: this.registerForm.get(['nationnalite'])!.value,
      zoneExploitation: this.registerForm.get(['zoneExploitation'])!.value,
    };
  }
}
