import { INationnalite } from "app/entities/nationnalite/nationnalite.model";
import { IZoneExploitation } from "app/entities/zone-exploitation/zone-exploitation.model";
import * as dayjs from "dayjs";

export class Registration {
  constructor(public login: string, public email: string, public password: string, 
    public numeroTelephone?: string,
    public pieceIdentiteContentType?: string | null,
    public pieceIdentite?: string | null,
    public dateNaissance?: dayjs.Dayjs,
    public lieuNaissance?: string,
    public firstName?: string | null,
    public lastName?: string | null,
    public authorities?: string[],
    public genre?: string,
    public nationnalite?: INationnalite,
    
    public zoneExploitation?: IZoneExploitation | null
    ) {}
}
